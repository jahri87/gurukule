<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'third_party/expo-noti/vendor/autoload.php');

class Webservice extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('mailer');
        $this->load->library(array('customlib', 'enc_lib'));
        $this->load->library('slug', $config);
        $this->load->model(array('auth_model', 'route_model', 'student_model', 'setting_model', 'attendencetype_model', 'studentfeemaster_model', 'feediscount_model', 'teachersubject_model', 'timetable_model', 'user_model', 'examgroup_model', 'webservice_model','grade_model','librarymember_model','bookissue_model','homework_model','event_model','vehroute_model','timeline_model','module_model','paymentsetting_model', 'customfield_model', 'subjecttimetable_model', 'onlineexam_model', 'leave_model', 'chatuser_model', 'conference_model', 'syllabus_model', 'staff_model','class_model','section_model','stuattendence_model','attendencetype_model','cms_program_model','subjectgroup_model','conferencehistory_model'));
		$this->config->load("mailsms");
        $this->load->library('mailsmsconf');
        $this->config_attendance = $this->config->item('attendence');
        $config = array(
            'field' => 'slug',
            'title' => 'title',
            'table' => 'front_cms_programs',
            'id' => 'id',
        );
	}
    

    public function getApplyLeave()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $data       = array();
                    $params     = json_decode(file_get_contents('php://input'), true);
                    $student_id = $params['student_id'];
                    $student    = $this->student_model->get($student_id);

                    $result               = $this->leave_model->get($student->student_session_id);
                    $data['result_array'] = $result;

                    json_output($response['status'], $data);
                    // json_output($response['status'], $response);
                }
            }

        }
    }

    public function addLeave()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $data = $this->input->POST();

                    $this->form_validation->set_data($data);
                    $this->form_validation->set_error_delimiters('', '');
                    $this->form_validation->set_rules('from_date', 'From', 'required|trim');
                    $this->form_validation->set_rules('to_date', 'To', 'required|trim');
                    $this->form_validation->set_rules('apply_date', 'Apply Date', 'required|trim');
                    $this->form_validation->set_rules('student_id', 'Student ID', 'required|trim');
                    // $this->form_validation->set_rules('base_url', 'base_url', 'required|trim');

                    if ($this->form_validation->run() == false) {

                        $sss = array(
                            'from_date'  => form_error('from_date'),
                            'to_date'    => form_error('to_date'),
                            'apply_date' => form_error('apply_date'),
                            'student_id' => form_error('student_id'),
                            'base_url'   => form_error('base_url'),
                        );
                        $array = array('status' => '0', 'error' => $sss);
                        // echo json_encode($array);

                    } else {
                        //==================
                        $student = $this->student_model->get($this->input->post('student_id'));

                        $data = array(
                            'from_date'          => $this->input->post('from_date'),
                            'to_date'            => $this->input->post('to_date'),
                            'apply_date'         => $this->input->post('apply_date'),
                            'reason'             => $this->input->post('reason'),
                            'student_session_id' => $student->student_session_id,
                        );
                        // $base_url = $this->input->post('base_url');

                        $leave_id    = $this->leave_model->add($data);
                        $upload_path = '../uploads/student_leavedocuments/';
                        // $upload_path = $this->config->item('upload_path') . "/student_leavedocuments/";

                        if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                            $fileInfo = pathinfo($_FILES["file"]["name"]);
                            $img_name = $leave_id . '.' . $fileInfo['extension'];
                            move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path . $img_name);
                            $data = array('id' => $leave_id, 'docs' => $img_name);
                            $this->leave_model->add($data);
                        }

                        $array = array('status' => '1', 'msg' => 'Success');
                    }
                    json_output(200, $array);
                }
            }

        }
    }

    public function updateLeave()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {

                    $data = $this->input->POST();
                    $this->form_validation->set_data($data);
                    $this->form_validation->set_error_delimiters('', '');
                    $this->form_validation->set_rules('id', 'From', 'required|trim');
                    $this->form_validation->set_rules('from_date', 'From', 'required|trim');
                    $this->form_validation->set_rules('to_date', 'To', 'required|trim');
                    $this->form_validation->set_rules('apply_date', 'Apply Date', 'required|trim');

                    if ($this->form_validation->run() == false) {

                        $sss = array(
                            'id'         => form_error('id'),
                            'from_date'  => form_error('from_date'),
                            'to_date'    => form_error('to_date'),
                            'apply_date' => form_error('apply_date'),

                        );
                        $array = array('status' => '0', 'error' => $sss);
                        // echo json_encode($array);

                    } else {
                        //==================
                        $leave_id = $this->input->post('id');
                        $data     = array(
                            'id'         => $this->input->post('id'),
                            'from_date'  => $this->input->post('from_date'),
                            'to_date'    => $this->input->post('to_date'),
                            'apply_date' => $this->input->post('apply_date'),
                            'reason'     => $this->input->post('reason'),
                        );
                        $upload_path = $this->config->item('upload_path') . "/student_leavedocuments/";

                        $this->leave_model->add($data);
                        if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                            $fileInfo = pathinfo($_FILES["file"]["name"]);
                            $img_name = $leave_id . '.' . $fileInfo['extension'];
                            move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path . $img_name);
                            $data = array('id' => $leave_id, 'docs' => $img_name);
                            $this->leave_model->add($data);
                        }

                        $array = array('status' => '1', 'msg' => 'Success');
                    }
                    json_output(200, $array);
                }
            }

        }
    }

    public function deleteLeave()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params   = json_decode(file_get_contents('php://input'), true);
                    $leave_id = $params['leave_id'];
                    $this->leave_model->delete($leave_id);

                    json_output($response['status'], array('result' => 'Success'));
                    // json_output($response['status'], $response);
                }
            }

        }
    }
    public function getSchoolDetails()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {

                    $result                   = $this->setting_model->getSchoolDisplay();
                    $result->start_month_name = ucfirst($this->customlib->getMonthList($result->start_month));

                    json_output($response['status'], $result);
                    // json_output($response['status'], $response);
                }
            }

        }
    }

   public function getStudentProfile()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params    = json_decode(file_get_contents('php://input'), true);
                    $studentId = $params['student_id'];

                    $student_fields = $this->setting_model->student_fields();
                    $custom_fields  = $this->customfield_model->student_fields();
                  
                    $student_array                   = array();
                    $student_result = $this->student_model->get($studentId);
                    $student_array['student_result'] = $student_result;
                    $student_array['student_fields'] = $student_fields;

                    if(!empty($custom_fields)){
                      foreach ($custom_fields as $custom_key => $custom_value) {
                           
                      $custom_fields[$custom_key]= $student_result->{$custom_key};
                         
                        }
                    }
                    $student_array['custom_fields']  = $custom_fields;
                    json_output($response['status'], $student_array);
                }
            }
        }
    }


    public function addTask()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {

                $_POST = json_decode(file_get_contents("php://input"), true);
                $this->form_validation->set_data($_POST);
                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('event_title', 'Title', 'required|trim');
                $this->form_validation->set_rules('date', 'Date', 'required|trim');
                $this->form_validation->set_rules('user_id', 'user login id', 'required|trim');

                if ($this->form_validation->run() == false) {

                    $sss = array(
                        'event_title' => form_error('event_title'),
                        'date'        => form_error('date'),
                        'user_id'     => form_error('user_id'),
                    );
                    $array = array('status' => '0', 'error' => $sss);
                    // echo json_encode($array);

                } else {
                    //==================
                    $data = array(
                        'event_title' => $this->input->post('event_title'),
                        'start_date'  => $this->input->post('date'),
                        'end_date'    => $this->input->post('date'),
                        'event_type'  => 'task',
                        'is_active'   => 'no',
                        'event_for'   => $this->input->post('user_id'),
                        'event_color' => '#000',

                    );
                    $this->event_model->saveEvent($data);
                    $array = array('status' => '1', 'msg' => 'Success');
                }
                json_output(200, $array);
            }

        }
    }

    public function updatetask()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {

                $_POST = json_decode(file_get_contents("php://input"), true);
                $this->form_validation->set_data($_POST);
                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('task_id', 'Task ID', 'required|trim');
                $this->form_validation->set_rules('status', 'Status', 'required|trim');

                if ($this->form_validation->run() == false) {
                    $errors = array(
                        'task_id' => form_error('task_id'),
                        'status'  => form_error('status'),
                    );
                    $array = array('status' => '0', 'error' => $errors);
                    // echo json_encode($array);

                } else {
                    //==================
                    $data = array(
                        'id'        => $this->input->post('task_id'),
                        'is_active' => $this->input->post('status'),

                    );
                    $this->event_model->saveEvent($data);
                    $array = array('status' => '1', 'msg' => 'Success');
                }
                json_output(200, $array);
            }

        }
    }

    public function deletetask()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {

                $_POST = json_decode(file_get_contents("php://input"), true);
                $this->form_validation->set_data($_POST);
                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('task_id', 'Task ID', 'required|trim');

                if ($this->form_validation->run() == false) {

                    $errors = array(
                        'task_id' => form_error('task_id'),

                    );
                    $array = array('status' => '0', 'error' => $errors);
                    // echo json_encode($array);

                } else {
                    //==================

                    $id = $this->input->post('task_id');

                    $this->event_model->deleteEvent($id);
                    $array = array('status' => '1', 'msg' => 'Success');
                }
                json_output(200, $array);
            }

        }
    }

    public function logout()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            // $check_auth_client = $this->auth_model->check_auth_client();
            // if ($check_auth_client == true) {
            $response = $this->auth_model->logout();
            json_output($response['status'], $response);
            // }
        }
    }

    public function forgot_password()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {

            $_POST = json_decode(file_get_contents("php://input"), true);
            $this->form_validation->set_data($_POST);
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('usertype', 'User Type', 'trim|required');
            if ($this->form_validation->run() == false) {
                $errors = validation_errors();
            }

            if (isset($errors)) {
                $respStatus = 400;
                $resp       = array('status' => 400, 'message' => $errors);
            } else {
                $email    = $this->input->post('email');
                $usertype = $this->input->post('usertype');
                $site_url = $this->input->post('site_url');

                $result = $this->user_model->forgotPassword($usertype, $email);

                if ($result && $result->email != "") {

                    $verification_code = $this->enc_lib->encrypt(uniqid(mt_rand()));
                    $update_record     = array('id' => $result->user_tbl_id, 'verification_code' => $verification_code);
                    $this->user_model->updateVerCode($update_record);
                    if ($usertype == "student") {
                        $name = $result->firstname . " " . $result->lastname;
                    } else {
                        $name = $result->guardian_email;
                    }
                    $resetPassLink = $site_url . '/user/resetpassword' . '/' . $usertype . "/" . $verification_code;

                    $body       = $this->forgotPasswordBody($name, $resetPassLink);
                    $body_array = json_decode($body);

                    if (!empty($this->mail_config)) {

                        $result = $this->mailer->send_mail($email, $body_array->subject, $body_array->body);
                        if ($result) {
                            $respStatus = 200;
                            $resp       = array('status' => 200, 'message' => "Please check your email to recover your password");
                        } else {
                            $respStatus = 200;
                            $resp       = array('status' => 200, 'message' => "Sending of message failed, Please contact to Admin.");
                        }
                    }

                } else {
                    $respStatus = 401;
                    $resp       = array('status' => 401, 'message' => "Invalid Email or User Type");

                }
            }
            json_output($respStatus, $resp);

        }

    }

    public function forgotPasswordBody($name, $resetPassLink)
    {
        //===============
        $subject = "Password Update Request";
        $body    = 'Dear ' . $name . ',
                <br/>Recently a request was submitted to reset password for your account. If you didn\'t make the request, just ignore this email. Otherwise you can reset your password using this link <a href="' . $resetPassLink . '"><button>Click here to reset your password</button></a>';
        $body .= '<br/><hr/>if you\'re having trouble clicking the password reset button, copy and paste the URL below into your web browser';
        $body .= '<br/>' . $resetPassLink;
        $body .= '<br/><br/>Regards,
                <br/>' . $this->customlib->getSchoolName();

        //======================
        return json_encode(array('subject' => $subject, 'body' => $body));
    }

    public function dashboard()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $date_list             = array();
                    $params                = json_decode(file_get_contents('php://input'), true);
                    $student_id            = $params['student_id'];
                    $date_from             = $params['date_from'];
                    $date_to               = $params['date_to'];
                    $student               = $this->student_model->get($student_id);
                    $student_login         = $this->user_model->getUserLoginDetails($student_id);
                    $attendence_percentage = 0;
                    $resp                  = array();
// print_r($student);
                    $student_session_id = $student->student_session_id;
                    $student_attendence = $this->attendencetype_model->getAttendencePercentage($date_from, $date_to, $student_session_id);
                    $student_homework   = $this->homework_model->getStudentHomeworkPercentage($student_session_id, $student->class_id, $student->section_id);
                    if ($student_attendence->present_attendance > 0 && $student_attendence->total_count > 0) {

                        $attendence_percentage = $student_attendence->present_attendance / $student_attendence->total_count * 100;
                    }

                    $school_setting = $this->setting_model->getSchoolDetail();
                    $resp['attendence_type'] = $school_setting->attendence_type;
                    $resp['class_id']                      = $student->class_id;
                    $resp['section_id']                    = $student->section_id;
                    $resp['student_attendence_percentage'] = round($attendence_percentage);
                    $resp['student_homework_incomplete']   = round($student_homework->total_homework - $student_homework->completed);
                    $resp['student_incomplete_task']       = $this->event_model->incompleteStudentTaskCounter($student_login['id']);
                    // $resp['public_events'] = $this->event_model->getPublicEvents($student_login['id']);
                    $resp['public_events'] = $this->event_model->getPublicEvents($student_login['id'], $date_from, $date_to);

                    foreach ($resp['public_events'] as &$ev_tsk_value) {
                        $evt_array = array();
                        if ($ev_tsk_value->event_type == "public") {
                            $start = strtotime($ev_tsk_value->start_date);
                            $end   = strtotime($ev_tsk_value->end_date);

                            for ($st = $start; $st <= $end; $st += 86400) {
                                if ($st >= strtotime($date_from) && $st <= strtotime($date_to)) {

                                    $date_list[date('Y-m-d', $st)] = date('Y-m-d', $st);
                                    $evt_array[]                   = date('Y-m-d', $st);
                                }
                            }

                            $ev_tsk_value->events_lists = implode(",", $evt_array);
                        } elseif ($ev_tsk_value->event_type == "task") {

                            $date_list[date('Y-m-d', strtotime($ev_tsk_value->start_date))] = date('Y-m-d', strtotime($ev_tsk_value->start_date));
                            $evt_array[]                                                    = date('Y-m-d', strtotime($ev_tsk_value->start_date));
                            $ev_tsk_value->events_lists                                     = implode(",", $evt_array);

                        }
                    }
                    $resp['date_lists'] = implode(",", $date_list);
                    
                    // for get the attendence data
                    $school_setting = $this->setting_model->getSchoolDetail();

                    $_POST                   = json_decode(file_get_contents("php://input"), true);
                    $year                    = date("Y");
                    $month                   = date("m");
                    $student_id              = $student_id;
                    $date                    = 6;
                    $student                 = $this->student_model->get($student_id);

                    $student_session_id      = $student->student_session_id;
                    $data                    = array();
                    $data['attendence_type'] = $school_setting->attendence_type;
                    if ($school_setting->attendence_type) {
                        $timestamp         = strtotime($date);
                        $day               = date('l', $timestamp);
                        $attendence_result = $this->attendencetype_model->studentAttendanceByDate($student->class_id, $student->section_id, $day, $date, $student_session_id);
                        $data['data']      = $attendence_result;

                    } else {

                        $result   = array();
                        $new_date = "01-" . $month . "-" . $year;

                        $totalDays            = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                        $first_day_this_month = date('01-m-Y');
                        $fst_day_str          = strtotime(date($new_date));
                        $array                = array();
                       // print_r($new_date);
                       // die;

                        for ($day = 1; $day <= $totalDays; $day++) {
                            $date               = date('Y-m-d', $fst_day_str);
                            $student_attendence = $this->attendencetype_model->getStudentAttendence($date, $student_session_id);
                            if (!empty($student_attendence)) {
                                $s         = array();
                                $s['date'] = $date;
                                $type      = $student_attendence->type;
                                $s['type'] = $type;
                                $array[]   = $s;
                            }
                            $fst_day_str        = ($fst_day_str + 86400);
                        }

                        $data['data'] = $array;
                    }
                    $resp['attendence_data'] = $array;
                    // End code

                    // for get the home
                    $result     = $this->student_model->get($student_id);

                    $class_id             = $result->class_id;
                    $section_id           = $result->section_id;
                    $section_id           = $result->section_id;
                    $homeworklist         = $this->homework_model->getStudentHomeworkforDashboard($class_id, $section_id, $result->student_session_id);
                    // $data2["homeworklist"] = $homeworklist;
                    // $data2["class_id"]     = $class_id;
                    // $data2["section_id"]   = $section_id;
                    // $data2["subject_id"]   = "";
                    // end code
                    $resp['homeworklist'] = $homeworklist;

                    // Code for get events_lists
                        $event_content = $this->config->item('ci_front_event_content');
                        $listResult = $this->cms_program_model->getByCategory($event_content);
                        $resp['eventList'] = $listResult;
                    // end code

                    // code for get galary
                        $notice_content = $this->config->item('ci_front_gallery_content');
                        $listResult = $this->cms_program_model->getByCategory($notice_content);
                        $finalOutput = [];
                        if(!empty($listResult)) {
                            $i=0;
                            foreach($listResult as $val) {
                                $finalOutput[$i]['id'] = $val['id'];
                                $finalOutput[$i]['type'] = $val['type'];
                                $finalOutput[$i]['slug'] = $val['slug'];
                                $finalOutput[$i]['url'] = $val['url'];
                                $finalOutput[$i]['title'] = $val['title'];
                                $finalOutput[$i]['date'] = $val['date'];
                                $finalOutput[$i]['event_start'] = $val['event_start'];
                                $finalOutput[$i]['event_end'] = $val['event_end'];
                                $finalOutput[$i]['event_venue'] = $val['event_venue'];
                                $finalOutput[$i]['description'] = $val['description'];
                                $finalOutput[$i]['is_active'] = $val['is_active'];
                                $finalOutput[$i]['created_at'] = $val['created_at'];
                                $finalOutput[$i]['meta_title'] = $val['meta_title'];
                                $finalOutput[$i]['meta_description'] = $val['meta_description'];
                                $finalOutput[$i]['meta_keyword'] = $val['meta_keyword'];
                                $finalOutput[$i]['feature_image'] = $val['feature_image'];
                                $finalOutput[$i]['publish_date'] = $val['publish_date'];
                                $finalOutput[$i]['publish'] = $val['publish'];
                                $finalOutput[$i]['sidebar'] = $val['sidebar'];
                                $gal = $this->cms_program_model->front_cms_program_photos($val['id']);
                                $finalOutput[$i]['album_images'] = $gal;
                                $i++;
                            }
                        }
                        $resp['galaryList'] = $finalOutput;
                    // end code

                    // code for get notice_content
                        $notice_content = $this->config->item('ci_front_notice_content');
                        $listResult = $this->cms_program_model->getByCategory($notice_content);
                        $resp['newsList'] = $listResult;
                        $getNoti = $this->webservice_model->getNotifications('student');
                        $resp['noticeList'] = $getNoti;
                    // end code
                    
                    // for the dashboard notification code
                        $student_id    = $student_id;
                        $chat_user     = $this->chatuser_model->getMyID($student_id, 'student');
                        
    
                          $notifications = array();
                            if (!empty($chat_user)) {
                               $notifications = $this->chatuser_model->getChatNotification($chat_user->id);
                        }
                        $resp['notificationsInfo'] = $notifications;
                    // end code
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getTask()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params  = json_decode(file_get_contents('php://input'), true);
                    $user_id = $params['user_id'];
                    $resp    = array();
                    // $student                 = $this->student_model->get($student_id);
                    // $student_login=$this->user_model->getUserLoginDetails($student_id);

                    $resp['tasks'] = $this->event_model->getTask($user_id);

                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getDocument()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST       = json_decode(file_get_contents("php://input"), true);
                    $student_id  = $this->input->post('student_id');
                    $student_doc = $this->student_model->getstudentdoc($student_id);
                    json_output($response['status'], $student_doc);
                }
            }
        }
    }

    public function getHomework()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST      = json_decode(file_get_contents("php://input"), true);
                    $student_id = $this->input->post('student_id');
                    $result     = $this->student_model->get($student_id);

                    $class_id             = $result->class_id;
                    $section_id           = $result->section_id;
                    $section_id           = $result->section_id;
                    $homeworklist         = $this->homework_model->getStudentHomework($class_id, $section_id, $result->student_session_id);
                    $data["homeworklist"] = $homeworklist;
                    $data["class_id"]     = $class_id;
                    $data["section_id"]   = $section_id;
                    $data["subject_id"]   = "";

                    json_output($response['status'], $data);
                }
            }
        }
    }

    public function addaa()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $data = $this->input->POST();

                    $this->form_validation->set_data($data);
                    $this->form_validation->set_error_delimiters('', '');
                    $this->form_validation->set_rules('student_id', 'Student', 'required|trim');
                    $this->form_validation->set_rules('homework_id', 'Homework', 'required|trim');
                    $this->form_validation->set_rules('message', 'Message', 'required|trim');

                    if (empty($_FILES['file']['name'])) {
                        $this->form_validation->set_rules('file', 'File', 'required|trim');
                    }

                    // $this->form_validation->set_rules('base_url', 'base_url', 'required|trim');

                    if ($this->form_validation->run() == false) {

                        $sss = array(
                            'student_id'  => form_error('student_id'),
                            'homework_id' => form_error('homework_id'),
                            'message'     => form_error('message'),
                            'file'        => form_error('file'),
                        );
                        $array = array('status' => '0', 'error' => $sss);
                        // echo json_encode($array);

                    } else {
                        //==================
                        $upload_path = $this->config->item('upload_path') . "/homework/assignment/";

                        if (isset($_FILES["file"]) && !empty($_FILES['file']['name'])) {
                            $time     = md5($_FILES["file"]['name'] . microtime());
                            $fileInfo = pathinfo($_FILES["file"]["name"]);
                            $img_name = $time . '.' . $fileInfo['extension'];
                            move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path . $img_name);

                            $data_insert = array(
                                'homework_id' => $this->input->post('homework_id'),
                                'student_id'  => $this->input->post('student_id'),
                                'docs'        => $img_name,
                            );

                            $this->homework_model->add($data_insert);
                        }

                        $array = array('status' => '1', 'msg' => 'Success');
                    }
                    json_output(200, $array);
                }
            }

        }
    }

    public function getTimeline()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params     = json_decode(file_get_contents('php://input'), true);
                    $student_id = $params['studentId'];
                    $timeline   = $this->timeline_model->getTimeline($student_id);
                    json_output($response['status'], $timeline);
                }
            }
        }
    }
    public function getOnlineExam()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params             = json_decode(file_get_contents('php://input'), true);
                    $student_id         = $params['student_id'];
                    $result             = $this->student_model->get($student_id);
                    $resp['onlineexam'] = $this->onlineexam_model->getStudentexam($result->student_session_id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }
    public function getOnlineExamQuestion()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params     = json_decode(file_get_contents('php://input'), true);
                    $student_id = $params['student_id'];
                    $recordid   = $params['online_exam_id'];

                    $result     = $this->student_model->get($student_id);
                    $onlineexam = array();
                    $exam       = $this->onlineexam_model->get($recordid);

                    $onlineexam_student            = $this->onlineexam_model->examstudentsID($result->student_session_id, $exam['id']);
                    $exam['onlineexam_student_id'] = $onlineexam_student->id;
                    $exam['student_session_id']    = $onlineexam_student->student_session_id;
                    $exam['is_submitted']          = $onlineexam_student->is_submitted;

                    $exam['questions']                        = $this->onlineexam_model->getExamQuestions($recordid);
                    $getStudentAttemts                        = $this->onlineexam_model->getStudentAttemts($onlineexam_student->id);
                    $onlineexam['exam_result_publish_status'] = $exam['publish_result'];
                    $onlineexam['exam_attempt_status']        = 0;

                    if (strtotime(date('Y-m-d H:i:s')) >= strtotime(date($exam['exam_to'] . ' 23:59:59'))) {
                        $question_status                   = 1;
                        $onlineexam['exam_attempt_status'] = 1;
                    } else if ($exam['attempt'] > $getStudentAttemts) {
                        $this->onlineexam_model->addStudentAttemts(array('onlineexam_student_id' => $onlineexam_student->id));
                    } else {
                        $question_status                   = 1;
                        $onlineexam['exam_attempt_status'] = 1;
                    }
                    $exam['status'] = $onlineexam;

// print_r($onlineexam);
                    // exit();
                    json_output($response['status'], array('exam' => $exam));
                }
            }
        }
    }

    public function getOnlineExamResult()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params                = json_decode(file_get_contents('php://input'), true);
                    $onlineexam_student_id = $params['onlineexam_student_id'];
                    $exam_id               = $params['exam_id'];

                    $exam = $this->onlineexam_model->get($exam_id);

                    $resp['question_result'] = $this->onlineexam_model->getResultByStudent($onlineexam_student_id, $exam_id);
                    $correct_ans             = 0;
                    $wrong_ans               = 0;
                    $not_attempted           = 0;
                    $total_question          = 0;
                    if (!empty($resp['question_result'])) {
                        $total_question = count($resp['question_result']);

                        foreach ($resp['question_result'] as $result_key => $question_value) {
                            if ($question_value->select_option != null) {

                                if ($question_value->select_option == $question_value->correct) {
                                    $correct_ans++;
                                } else {
                                    $wrong_ans++;
                                }
                            } else {
                                $not_attempted++;
                            }
                        }
                    }
                    $exam['correct_ans']    = $correct_ans;
                    $exam['wrong_ans']      = $wrong_ans;
                    $exam['not_attempted']  = $not_attempted;
                    $exam['total_question'] = $total_question;
                    $exam['score']          = ($correct_ans * 100) / $total_question;
                    $resp['exam']           = $exam;

                    json_output($response['status'], array('result' => $resp));
                }
            }
        }
    }

    public function getExamResult1()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                // $response = $this->auth_model->auth();
                // if ($response['status'] == 200) {
                    $params                = json_decode(file_get_contents('php://input'), true);
                    // $onlineexam_student_id = $params['onlineexam_student_id'];
                    $exam_id               = $params['exam_id'];

                    $exam = $this->onlineexam_model->get($exam_id);

                    $resp['question_result'] = $this->onlineexam_model->getResultByExam($exam_id);
                    $correct_ans             = 0;
                    $wrong_ans               = 0;
                    $not_attempted           = 0;
                    $total_question          = 0;
                    if (!empty($resp['question_result'])) {
                        $total_question = count($resp['question_result']);

                        foreach ($resp['question_result'] as $result_key => $question_value) {
                            if ($question_value->select_option != null) {

                                if ($question_value->select_option == $question_value->correct) {
                                    $correct_ans++;
                                } else {
                                    $wrong_ans++;
                                }
                            } else {
                                $not_attempted++;
                            }
                        }
                    }
                    $exam['correct_ans']    = $correct_ans;
                    $exam['wrong_ans']      = $wrong_ans;
                    $exam['not_attempted']  = $not_attempted;
                    $exam['total_question'] = $total_question;
                    $exam['score']          = ($correct_ans * 100) / $total_question;
                    $resp['exam']           = $exam;

                    json_output(200, array('result' => $resp));
                // }
            }
        }
    }
    
    public function saveOnlineExam()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), true);

                    $onlineexam_student_id = $params['onlineexam_student_id'];
                    $rows                  = $params['rows'];
                    $resp                  = array();
                    if (!empty($rows)) {
                        $save_result = array();

                        $insert_result = $this->onlineexam_model->add($rows, $onlineexam_student_id);
                        if ($insert_result == 1) {
                            $resp = array('status' => 1, 'msg' => 'record inserted');
                        } else if ($insert_result == 2) {
                            $resp = array('status' => 2, 'msg' => 'record already submitted');
                        } else if ($insert_result == 0) {
                            $resp = array('status' => 2, 'msg' => 'something wrong');
                        }

                    }

                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getExamList()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), true);

                    $student_id = $params['student_id'];
                    $result     = $this->student_model->get($student_id);

                    $examSchedule = $this->examgroup_model->studentExams($result->student_session_id);

                    $data['examSchedule'] = $examSchedule;

                    json_output($response['status'], $data);
                }
            }
        }
    }

    public function getExamListByClassandSection()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params    = json_decode(file_get_contents('php://input'), true);
                    $classId   = $params['classId'];
                    $sectionId = $params['sectionId'];
                    $sessionId = $params['sessionId'];
                    $resp      = $this->webservice_model->getExamListByClassandSection($classId, $sectionId, $sessionId);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getExamSchedule()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params                = json_decode(file_get_contents('php://input'), true);
                    $exam_id               = $params['exam_group_class_batch_exam_id'];
                    $exam_subjects         = $this->examgroup_model->getExamSubjects($exam_id);
                    $data['exam_subjects'] = $exam_subjects;
                    json_output($response['status'], $data);
                }
            }
        }
    }

    public function getNotifications()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $type   = $params['type'];
                    $resp   = $this->webservice_model->getNotifications($type);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getSubjectList()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params     = json_decode(file_get_contents('php://input'), true);
                    $class_id   = $params['class_id'];
                    $section_id = $params['section_id'];
                    $resp       = $this->subjecttimetable_model->getSubjects($class_id, $section_id);
                    $subjects   = array();
                    if (!empty($resp)) {

                        foreach ($resp as $res_key => $res_value) {

                            $subjects[] = array(
                                'subject_id' => $res_value->subject_id,
                                'subject'    => $res_value->subject_name,
                                'code'       => $res_value->code,
                                'type'       => $res_value->type,
                            );
                        }

                    }

                    // $resp       = $this->webservice_model->getSubjectList($class_id, $section_id);
                    json_output($response['status'], array('result_list' => $subjects));
                }
            }
        }
    }

    public function getSubjectTimetable()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params     = json_decode(file_get_contents('php://input'), true);
                    $class_id   = $params['class_id'];
                    $section_id = $params['section_id'];
                    $subject_id = $params['subject_id'];
                    $resp       = $this->subjecttimetable_model->getSubjectTimetable($class_id, $section_id, $subject_id);
                    $subjects   = array();

                    // $resp       = $this->webservice_model->getSubjectList($class_id, $section_id);
                    json_output($response['status'], array('result_list' => $resp));
                }
            }
        }
    }

    // public function getTeachersList111()
    // {
    //     $method = $this->input->server('REQUEST_METHOD');
    //     if ($method != 'POST') {
    //         json_output(400, array('status' => 400, 'message' => 'Bad request.'));
    //     } else {
    //         $check_auth_client = $this->auth_model->check_auth_client();
    //         if ($check_auth_client == true) {
    //             $response = $this->auth_model->auth();
    //             if ($response['status'] == 200) {
    //                 $params        = json_decode(file_get_contents('php://input'), true);
    //                 $user_id       = $params['user_id'];
    //                 $class_id      = $params['class_id'];
    //                 $section_id    = $params['section_id'];
    //                 $resp          = $this->subjecttimetable_model->getTeachers($class_id, $section_id);
    //                 $class_teacher = array();
    //                 if (!empty($resp)) {

    //                     foreach ($resp as $res_key => $res_value) {

    //                         $rating = $this->subjecttimetable_model->user_rating($user_id, $res_value->staff_id);
    //                         $rate   = 0;
    //                         if ($rating) {
    //                             $rate = $rating->rate;
    //                         }
    //                         $class_teacher[$res_value->staff_id] = array(
    //                             'staff_id'         => $res_value->staff_id,
    //                             'staff_name'       => $res_value->staff_name,
    //                             'staff_surname'    => $res_value->staff_surname,
    //                             'contact_no'       => $res_value->contact_no,
    //                             'class_teacher_id' => $res_value->class_teacher_id,
    //                             'rate'             => $rate,
    //                         );

    //                     }

    //                 }
    //                 // $resp      = $this->webservice_model->getTeachersList($sectionId);
    //                 json_output($response['status'], array('result_list' => $class_teacher));
    //             }
    //         }
    //     }
    // }
    public function getTeachersList()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params     = json_decode(file_get_contents('php://input'), true);
                    $user_id    = $params['user_id'];
                    $class_id   = $params['class_id'];
                    $section_id = $params['section_id'];
                    $resp       = $this->subjecttimetable_model->getTeachers($class_id, $section_id);

                    $class_teacher = array();
                    if (!empty($resp)) {

                        foreach ($resp as $res_key => $res_value) {
                            $is_duplicate = false;
                            $rating       = $this->subjecttimetable_model->user_rating($user_id, $res_value->staff_id);
                            $rate         = 0;
                            if ($rating) {
                                $rate = $rating->rate;
                            }

                            if (is_null($res_value->day)) {
                                $total_row = checkDuplicateTeacher($resp, $res_value->staff_id);
                                if ($total_row > 1) {
                                    $is_duplicate = true;

                                }
                            }
                            if (!$is_duplicate) {
                                if (array_key_exists($res_value->staff_id, $class_teacher)) {

                                    $class_teacher[$res_value->staff_id]['subjects'][] = array(
                                        'subject_id'   => $res_value->subject_id,
                                        'subject_name' => $res_value->subject_name,
                                        'code'         => $res_value->code,
                                        'type'         => $res_value->type,
                                        'day'          => $res_value->day,
                                        'time_from'    => $res_value->time_from,
                                        'time_to'      => $res_value->time_to,
                                        'room_no'      => $res_value->room_no,
                                    );
                                } else {

                                    $class_teacher[$res_value->staff_id] = array(
                                        'employee_id'      => $res_value->employee_id,
                                        'staff_id'         => $res_value->staff_id,
                                        'staff_name'       => $res_value->staff_name,
                                        'staff_surname'    => $res_value->staff_surname,
                                        'contact_no'       => $res_value->contact_no,
                                        'email'            => $res_value->email,
                                        'class_teacher_id' => $res_value->class_teacher_id,
                                        'rate'             => $rate,
                                        'subjects'         => array(),
                                    );
                                    if (!is_null($res_value->day)) {
                                        $class_teacher[$res_value->staff_id]['subjects'][] = array(
                                            'subject_id'   => $res_value->subject_id,
                                            'subject_name' => $res_value->subject_name,
                                            'code'         => $res_value->code,
                                            'type'         => $res_value->type,
                                            'day'          => $res_value->day,
                                            'time_from'    => $res_value->time_from,
                                            'time_to'      => $res_value->time_to,
                                            'room_no'      => $res_value->room_no,
                                        );
                                    }

                                }

                                // $class_teacher[] = array(
                                // 'staff_id'         => $res_value->staff_id,
                                // 'staff_name'       => $res_value->staff_name,
                                // 'staff_surname'    => $res_value->staff_surname,
                                // 'contact_no'       => $res_value->contact_no,
                                // 'class_teacher_id' => $res_value->class_teacher_id,
                                // 'subject_id' => $res_value->subject_id,
                                // 'subject_name' => $res_value->subject_name,
                                // 'code' => $res_value->code,
                                // 'type' => $res_value->type,
                                // 'day' => $res_value->day,
                                // 'time_from' => $res_value->time_from,
                                // 'time_to' => $res_value->time_to,
                                // 'room_no' => $res_value->room_no,
                                // 'rate'             => $rate,
                                // );
                            }

                        }

                    }
                    // $resp      = $this->webservice_model->getTeachersList($sectionId);
                    json_output($response['status'], array('result_list' => $class_teacher));
                }
            }
        }
    }

    public function getClassTimetable()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params     = json_decode(file_get_contents('php://input'), true);
                    $user_id    = $params['user_id'];
                    $class_id   = $params['class_id'];
                    $section_id = $params['section_id'];
                    $resp       = $this->subjecttimetable_model->getTeachers($class_id, $section_id);

                    $class_teacher = array();
                    if (!empty($resp)) {

                        foreach ($resp as $res_key => $res_value) {
                            $is_duplicate = false;
                            $rating       = $this->subjecttimetable_model->user_rating($user_id, $res_value->staff_id);
                            $rate         = 0;
                            if ($rating) {
                                $rate = $rating->rate;
                            }

                            if (is_null($res_value->day)) {
                                $total_row = checkDuplicateTeacher($resp, $res_value->staff_id);
                                if ($total_row > 1) {
                                    $is_duplicate = true;

                                }
                            }
                            if (!$is_duplicate) {

                                $class_teacher[] = array(
                                    'staff_id'         => $res_value->staff_id,
                                    'staff_name'       => $res_value->staff_name,
                                    'staff_surname'    => $res_value->staff_surname,
                                    'contact_no'       => $res_value->contact_no,
                                    'class_teacher_id' => $res_value->class_teacher_id,
                                    'subject_id'       => $res_value->subject_id,
                                    'subject_name'     => $res_value->subject_name,
                                    'code'             => $res_value->code,
                                    'type'             => $res_value->type,
                                    'day'              => $res_value->day,
                                    'time_from'        => $res_value->time_from,
                                    'time_to'          => $res_value->time_to,
                                    'room_no'          => $res_value->room_no,
                                    'rate'             => $rate,
                                );
                            }

                        }

                    }
                    // $resp      = $this->webservice_model->getTeachersList($sectionId);
                    json_output($response['status'], array('result_list' => $class_teacher));
                }
            }
        }
    }

    public function getTeacherSubject()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), true);

                    $staff_id   = $params['staff_id'];
                    $class_id   = $params['class_id'];
                    $section_id = $params['section_id'];
                    $resp       = $this->subjecttimetable_model->getTeacherSubject($class_id, $section_id, $staff_id);

                    // $resp      = $this->webservice_model->getTeachersList($sectionId);
                    json_output($response['status'], array('result_list' => $resp));
                }
            }
        }
    }

    public function addStaffRating()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {

                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $data   = array(
                        'user_id'  => $params['user_id'],
                        'staff_id' => $params['staff_id'],
                        'rate'     => $params['rate'],
                        'comment'  => $params['comment'],
                        'role'     => 'student',

                    );

                    $insert_result = $this->subjecttimetable_model->add_rating($data);
                    if ($insert_result) {
                        $resp = array('status' => 1, 'msg' => 'inserted');
                    } else {
                        $resp = array('status' => 0, 'msg' => 'something wrong or already submitted');
                    }

                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getLibraryBooks()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {

                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $resp = $this->webservice_model->getLibraryBooks();
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getLibraryBookIssued()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {

                    $params      = json_decode(file_get_contents('php://input'), true);
                    $studentId   = $params['studentId'];
                    $member_type = "student";
                    $resp        = $this->librarymember_model->checkIsMember($member_type, $studentId);

                    json_output($response['status'], $resp);
                }
            }
        }
    }
    // public function getTransportRoute()
    // {
    //     $method = $this->input->server('REQUEST_METHOD');
    //     if ($method != 'GET') {
    //         json_output(400, array('status' => 400, 'message' => 'Bad request.'));
    //     } else {
    //         $check_auth_client = $this->auth_model->check_auth_client();
    //         if ($check_auth_client == true) {
    //             $response = $this->auth_model->auth();
    //             if ($response['status'] == 200) {
    //                 $resp = $this->webservice_model->getTransportRoute();
    //                 json_output($response['status'], $resp);
    //             }
    //         }
    //     }
    // }

    public function getTransportroute()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {

                    $params       = json_decode(file_get_contents('php://input'), true);
                    $student_id   = $params['student_id'];
                    $student      = $this->student_model->get($student_id);
                    $vec_route_id = $student->vehroute_id;
                    $listroute    = $this->vehroute_model->listroute();

                    if ($vec_route_id != "") {
                        if (!empty($listroute)) {
                            foreach ($listroute as $listroute_key => $listroute_value) {

                                if (!empty($listroute_value['vehicles'])) {
                                    foreach ($listroute_value['vehicles'] as $route_key => $route_value) {
                                        if ($route_value->vec_route_id == $vec_route_id) {
                                            $route_value->assigned = "yes";
                                            break;
                                        } else {
                                            $route_value->assigned = "no";
                                        }

                                    }
                                }
                            }

                        }

                    }

                    json_output($response['status'], $listroute);
                }
            }
        }
    }

    public function getHostelList()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $resp = $this->webservice_model->getHostelList();
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getHostelDetails()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params     = json_decode(file_get_contents('php://input'), true);
                    $hostelId   = $params['hostelId'];
                    $student_id = $params['student_id'];
                    $resp       = $this->webservice_model->getHostelDetails($hostelId, $student_id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getDownloadsLinks()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params    = json_decode(file_get_contents('php://input'), true);
                    $tag       = $params['tag'];
                    $classId   = $params['classId'];
                    $sectionId = $params['sectionId'];
                    $resp      = $this->webservice_model->getDownloadsLinks($classId, $sectionId, $tag);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getTransportVehicleDetails()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params    = json_decode(file_get_contents('php://input'), true);
                    $vehicleId = $params['vehicleId'];
                    $resp      = $this->webservice_model->getTransportVehicleDetails($vehicleId);
                    json_output($response['status'], $resp);
                }
            }
        }
    }

    public function getAttendenceRecords1()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    ///===================
                    $_POST = json_decode(file_get_contents("php://input"), true);

                    $year       = $this->input->post('year');
                    $month      = $this->input->post('month');
                    $student_id = $this->input->post('student_id');
                    $student    = $this->student_model->get($student_id);

                    $student_session_id = $student['student_session_id'];
                    $result             = array();
                    $new_date           = "01-" . $month . "-" . $year;

                    $totalDays            = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                    $first_day_this_month = date('01-m-Y');
                    $fst_day_str          = strtotime(date($new_date));
                    $array                = array();
                    for ($day = 2; $day <= $totalDays; $day++) {
                        $fst_day_str        = ($fst_day_str + 86400);
                        $date               = date('Y-m-d', $fst_day_str);
                        $student_attendence = $this->attendencetype_model->getStudentAttendence($date, $student_session_id);
                        if (!empty($student_attendence)) {
                            $s         = array();
                            $s['date'] = $date;
                            $type      = $student_attendence->type;
                            $s['type'] = $type;
                            $array[]   = $s;
                        }
                    }
                    $data['status'] = 200;
                    $data['data']   = $array;
                    json_output($response['status'], $data);

                    //======================
                }
            }
        }
    }

    public function getAttendenceRecords()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $school_setting = $this->setting_model->getSchoolDetail();

                    $_POST                   = json_decode(file_get_contents("php://input"), true);
                    $year                    = $this->input->post('year');
                    $month                   = $this->input->post('month');
                    $student_id              = $this->input->post('student_id');
                    $date                    = $this->input->post('date');
                    $student                 = $this->student_model->get($student_id);
                    $student_session_id      = $student->student_session_id;
                    $data                    = array();
                    $data['attendence_type'] = $school_setting->attendence_type;
                    if ($school_setting->attendence_type) {
                        $timestamp         = strtotime($date);
                        $day               = date('l', $timestamp);
                        $attendence_result = $this->attendencetype_model->studentAttendanceByDate($student->class_id, $student->section_id, $day, $date, $student_session_id);
                        $data['data']      = $attendence_result;

                    } else {

                        $result   = array();
                        $new_date = "01-" . $month . "-" . $year;

                        $totalDays            = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                        $first_day_this_month = date('01-m-Y');
                        $fst_day_str          = strtotime(date($new_date));
                        $array                = array();

                        for ($day = 1; $day <= $totalDays; $day++) {
                            $date               = date('Y-m-d', $fst_day_str);
                            $student_attendence = $this->attendencetype_model->getStudentAttendence($date, $student_session_id);
                            if (!empty($student_attendence)) {
                                $s         = array();
                                $s['date'] = $date;
                                $type      = $student_attendence->type;
                                $s['type'] = $type;
                                $array[]   = $s;
                            }
                            $fst_day_str = ($fst_day_str + 86400);
                        }

                        $data['data'] = $array;
                    }

                    json_output($response['status'], $data);

                    //======================
                }
            }
        }
    }

    public function examSchedule()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST = json_decode(file_get_contents("php://input"), true);

                    $student_id           = $this->input->post('student_id');
                    $data                 = array();
                    $stu_record           = $this->student_model->getRecentRecord($student_id);
                    $data['status']       = "200";
                    $data['class_id']     = $stu_record['class_id'];
                    $data['section_id']   = $stu_record['section_id'];
                    $examSchedule         = $this->examschedule_model->getExamByClassandSection($data['class_id'], $data['section_id']);
                    $data['examSchedule'] = $examSchedule;
                    json_output($response['status'], $data);
                }
            }
        }
    }

    public function getexamscheduledetail()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST = json_decode(file_get_contents("php://input"), true);
                    $this->form_validation->set_data($_POST);
                    $exam_id      = $this->input->post('exam_id');
                    $section_id   = $this->input->post('section_id');
                    $class_id     = $this->input->post('class_id');
                    $examSchedule = $this->examschedule_model->getDetailbyClsandSection($class_id, $section_id, $exam_id);
                    json_output($response['status'], $examSchedule);
                }
            }
        }

    }

    public function getsyllabus()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST = json_decode(file_get_contents("php://input"), true);
                    $this->form_validation->set_data($_POST);
                   
                    $subject_group_subject_id        = $this->input->post('subject_group_subject_id');
                    $subject_group_class_sections_id = $this->input->post('subject_group_class_sections_id');
                    $time_from                       = $this->input->post('time_from');
                    $time_to                         = $this->input->post('time_to');
                    $date                            = $this->input->post('date');
                    $syllabus['data']            = $this->syllabus_model->getDetailbyDateandTime($subject_group_subject_id, $subject_group_class_sections_id, $time_from, $time_to, $date);
                    json_output($response['status'], $syllabus);
                }
            }
        }

    }

    public function getsyllabussubjects()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST = json_decode(file_get_contents("php://input"), true);
                    $this->form_validation->set_data($_POST);
                    $student_id         = $this->input->post('student_id');
                    $stu_record         = $this->student_model->getRecentRecord($student_id);
                    $data['class_id']   = $stu_record['class_id'];
                    $data['section_id'] = $stu_record['section_id'];
                    $subjects['subjects']          = $this->syllabus_model->getSyllabusSubjects($data['class_id'], $data['section_id']);
                    json_output($response['status'], $subjects);
                }
            }
        }

    }

    public function getSubjectsLessons()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST = json_decode(file_get_contents("php://input"), true);
                    $this->form_validation->set_data($_POST);
                    $subject_group_subject_id         = $this->input->post('subject_group_subject_id');
                     $subject_group_class_sections_id         = $this->input->post('subject_group_class_sections_id');                
                  
                    $subjects          = $this->syllabus_model->getSubjectsLesson($subject_group_subject_id,$subject_group_class_sections_id);
                    json_output($response['status'], $subjects);
                }
            }
        }

    }

    // public function fees()
    // {
    //     $method = $this->input->server('REQUEST_METHOD');

    //     if ($method != 'POST') {
    //         json_output(400, array('status' => 400, 'message' => 'Bad request.'));
    //     } else {

    //         $check_auth_client = $this->auth_model->check_auth_client();
    //         if ($check_auth_client == true) {
    //             $response = $this->auth_model->auth();
    //             if ($response['status'] == 200) {

    //                 $_POST      = json_decode(file_get_contents("php://input"), true);
    //                 $student_id = $this->input->post('student_id');

    //                 $student = $this->student_model->get($student_id);
    //                 // $studentSession     = $this->student_model->getStudentSession($student_id);
    //                 // $student_session_id = $studentSession["student_session_id"];
    //                 // $student_session    = $studentSession["session"];

    //                 $student_due_fee              = $this->studentfeemaster_model->getStudentFees($student['student_session_id']);
    //                 $student_discount_fee         = $this->feediscount_model->getStudentFeesDiscount($student['student_session_id']);
    //                 $data['student_due_fee']      = $student_due_fee;
    //                 $data['student_discount_fee'] = $student_discount_fee;
    //                 json_output($response['status'], $data);
    //             }
    //         }

    //     }
    // }

      public function fees()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $data       = array();
                    $pay_method = $this->paymentsetting_model->getActiveMethod();
                    $_POST      = json_decode(file_get_contents("php://input"), true);
                    $student_id = $this->input->post('student_id');
                    $student    = $this->student_model->get($student_id);

                    $student_due_fee      = $this->studentfeemaster_model->getStudentFees($student->student_session_id);
                    $student_discount_fee = $this->feediscount_model->getStudentFeesDiscount($student->student_session_id);
                    $init_amt             = 0;
                    $grand_amt            = 0;
                    $grand_total_paid     = 0;
                    $grand_total_discount = 0;
                    $grand_total_fine     = 0;

                    if (!empty($student_due_fee)) {

                        foreach ($student_due_fee as $student_due_fee_key => $student_due_fee_value) {

                            foreach ($student_due_fee_value->fees as $each_fees_key => $each_fees_value) {

                                $amt                                     = 0;
                                $total_paid                              = 0;
                                $total_discount                          = 0;
                                $total_fine                              = 0;
                                $each_fees_value->total_amount_paid      = number_format((float) $amt, 2, '.', '');
                                $each_fees_value->total_amount_discount  = number_format((float) $amt, 2, '.', '');
                                $each_fees_value->total_amount_fine      = number_format((float) $amt, 2, '.', '');
                                $each_fees_value->total_amount_display   = number_format((float) $amt, 2, '.', '');
                                $each_fees_value->total_amount_remaining = number_format((float) $each_fees_value->amount, 2, '.', '');
                                $each_fees_value->status                 = 'unpaid';

                                $grand_amt = $grand_amt + $each_fees_value->amount;

                                if (is_string($each_fees_value->amount_detail) && is_array(json_decode($each_fees_value->amount_detail, true)) && (json_last_error() == JSON_ERROR_NONE)) {
                                    $fess_list = json_decode($each_fees_value->amount_detail);

                                    foreach ($fess_list as $fee_key => $fee_value) {

                                        $grand_total_paid = $grand_total_paid + $fee_value->amount;
                                        $total_paid       = $total_paid + $fee_value->amount;

                                        $grand_total_discount = $grand_total_discount + $fee_value->amount_discount;
                                        $total_discount       = $total_discount + $fee_value->amount_discount;

                                        $grand_total_fine = $grand_total_fine + $fee_value->amount_fine;
                                        $total_fine       = $total_fine + $fee_value->amount_fine;

                                    }

                                    $each_fees_value->total_amount_paid     = number_format((float) $total_paid, 2, '.', '');
                                    $each_fees_value->total_amount_discount = number_format((float) $total_discount, 2, '.', '');
                                    $each_fees_value->total_amount_fine     = number_format((float) $total_fine, 2, '.', '');

                                    $each_fees_value->total_amount_display   = number_format((float) ($total_paid + $total_discount), 2, '.', '');
                                    $each_fees_value->total_amount_remaining = number_format((float) ($each_fees_value->amount - (($total_paid + $total_discount))), 2, '.', '');

                                    if ($each_fees_value->total_amount_remaining <= '0.00') {
                                        $each_fees_value->status = 'paid';
                                    } elseif ($each_fees_value->total_amount_remaining == number_format((float) $each_fees_value->amount, 2, '.', '')) {
                                        $each_fees_value->status = 'unpaid';
                                    } else {
                                        $each_fees_value->status = 'partial';

                                    }
                                }

                                if (($each_fees_value->amount - ($each_fees_value->total_amount_paid + $each_fees_value->total_amount_discount)) == 0) {
                                    $each_fees_value->status = 'paid';
                                }
                            }
                        }
                    }

                    $grand_fee = array('amount' => number_format((float) $grand_amt, 2, '.', ''), 'amount_discount' => number_format((float) $grand_total_discount, 2, '.', ''), 'amount_fine' => number_format((float) $grand_total_fine, 2, '.', ''), 'amount_paid' => number_format((float) $grand_total_paid, 2, '.', ''), 'amount_remaining' => number_format((float) ($grand_amt - ($grand_total_paid + $grand_total_discount)), 2, '.', ''));

                    $data['pay_method']           = empty($pay_method) ? 0 : 1;
                    $data['student_due_fee']      = $student_due_fee;
                    $data['student_discount_fee'] = $student_discount_fee;
                    $data['grand_fee']            = $grand_fee;
                    json_output($response['status'], $data);
                }
            }
        }
    }
    // public function class_schedule()
    //    {
    //        $method = $this->input->server('REQUEST_METHOD');
    //        if ($method != 'POST') {
    //            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
    //        } else {
    //            $check_auth_client = $this->auth_model->check_auth_client();
    //            if ($check_auth_client == true) {
    //                $response = $this->auth_model->auth();
    //                if ($response['status'] == 200) {
    //                    $_POST                   = json_decode(file_get_contents("php://input"), true);
    //                    $student_id              = $this->input->post('student_id');
    //                    $student                 = $this->student_model->get($student_id);
    //                    $class_id                = $student['class_id'];
    //                    $section_id              = $student['section_id'];
    //                    $data['class_id']        = $class_id;
    //                    $data['section_id']      = $section_id;
    //                    $result_subjects         = $this->teachersubject_model->getSubjectByClsandSection($class_id, $section_id);
    //                    $getDaysnameList         = $this->customlib->getDaysname();
    //                    $data['getDaysnameList'] = $getDaysnameList;
    //                    $dayListArray            = array();

    //                    foreach ($getDaysnameList as $Day_key => $Day_value) {
    //                        $dayListArray[$Day_value] = array();
    //                    }

    //                    $final_array = array();
    //                    if (!empty($result_subjects)) {
    //                        foreach ($result_subjects as $subject_k => $subject_v) {

    //                            foreach ($getDaysnameList as $day_key => $day_value) {
    //                                $where_array = array(
    //                                    'teacher_subject_id' => $subject_v['id'],
    //                                    'day_name'           => $day_value,
    //                                );
    //                                $obj    = new stdClass();
    //                                $result = $this->timetable_model->get($where_array);
    //                                if (!empty($result)) {
    //                                    $obj->status     = "Yes";
    //                                    $obj->start_time = $result[0]['start_time'];
    //                                    $obj->end_time   = $result[0]['end_time'];
    //                                    $obj->room_no    = $result[0]['room_no'];
    //                                    $obj->subject    = $subject_v['name'];
    //                                } else {

    //                                    $obj->status     = "No";
    //                                    $obj->start_time = "N/A";
    //                                    $obj->end_time   = "N/A";
    //                                    $obj->room_no    = "N/A";
    //                                    $obj->subject    = $subject_v['name'];

    //                                }

    //                                $dayListArray[$day_value][] = $obj;
    //                            }
    //                        }
    //                    }

    //                    $data['status']       = "200";
    //                    $data['result_array'] = array();
    //                    $data['result_array'] = $dayListArray;
    //                    json_output($response['status'], $data);
    //                }
    //            }
    //        }
    //    }

    public function class_schedule()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST      = json_decode(file_get_contents("php://input"), true);
                    $student_id = $this->input->post('student_id');
                    $student    = $this->student_model->get($student_id);
                    $class_id   = $student->class_id;
                    $section_id = $student->section_id;
                    // $data['class_id']   = $class_id;
                    // $data['section_id'] = $section_id;

                    $days        = $this->customlib->getDaysname();
                    $days_record = array();
                    foreach ($days as $day_key => $day_value) {

                        $days_record[$day_key] = $this->subjecttimetable_model->getSubjectByClassandSectionDay($class_id, $section_id, $day_key);
                    }
                    $data['timetable'] = $days_record;
                    $data['status']    = "200";
                    json_output($response['status'], $data);
                }
            }
        }
    }

    // public function getExamResultList()
    // {
    //     $method = $this->input->server('REQUEST_METHOD');
    //     if ($method != 'POST') {
    //         json_output(400, array('status' => 400, 'message' => 'Bad request.'));
    //     } else {
    //         $check_auth_client = $this->auth_model->check_auth_client();
    //         if ($check_auth_client == true) {
    //             $response = $this->auth_model->auth();
    //             if ($response['status'] == 200) {
    //                 $_POST      = json_decode(file_get_contents("php://input"), true);
    //                 $student_id = $this->input->post('student_id');
    //                 $student    = $this->student_model->get($student_id);
    //                 $examList   = $this->examschedule_model->getExamByClassandSection($student['class_id'], $student['section_id']);
    //                 $resp['status'] = 200;
    //                 $resp['examList'] = $examList;
    //                 json_output(200, $resp);
    //             }
    //         }
    //     }
    // }

    // public function getExamResultList()
    // {
    //     $method = $this->input->server('REQUEST_METHOD');
    //     if ($method != 'POST') {
    //         json_output(400, array('status' => 400, 'message' => 'Bad request.'));
    //     } else {
    //         $check_auth_client = $this->auth_model->check_auth_client();
    //         if ($check_auth_client == true) {
    //             $response = $this->auth_model->auth();
    //             if ($response['status'] == 200) {
    //                 $_POST          = json_decode(file_get_contents("php://input"), true);
    //                 $student_id     = $this->input->post('student_id');
    //                 $student        = $this->student_model->get($student_id);
    //                 $examList       = $this->examschedule_model->getExamByClassandSection($student['class_id'], $student['section_id']);
    //                 $resp['status'] = 200;

    //                 $resp['examList'] = array();
    //                 if (!empty($examList)) {
    //                     $new_array = array();
    //                     foreach ($examList as $ex_key => $ex_value) {
    //                         $array   = array();
    //                         $x       = array();
    //                         $exam_id = $ex_value['exam_id'];
    //                         $student['id'];
    //                         $exam_subjects = $this->examschedule_model->getresultByStudentandExam($exam_id, $student['id']);
    //                         $total_marks   = 0;
    //                         $get_marks     = 0;
    //                         $result        = "Pass";

    //                         foreach ($exam_subjects as $key => $value) {

    //                             $total_marks = $total_marks + $value['full_marks'];
    //                             $get_marks   = $get_marks + $value['get_marks'];

    //                             if (($value['get_marks'] < $value['passing_marks']) || ($value['attendence'] != 'pre')) {
    //                                 $result = 'Fail';
    //                             }

    //                         }

    //                         $exam_result              = new stdClass();
    //                         $exam_result->total_marks = $total_marks;
    //                         $exam_result->get_marks   = number_format($get_marks, 2);
    //                         $exam_result->percentage  = number_format((($get_marks * 100) / $total_marks), 2) . '%';
    //                         $exam_result->grade       = $this->getGradeByMarks(number_format((($get_marks * 100) / $total_marks), 2));
    //                         $exam_result->result      = $result;
    //                         $exam_result->exam_id     = $ex_value['exam_id'];
    //                         $array['exam_name']       = $ex_value['name'];
    //                         $array['exam_result']     = $exam_result;
    //                         $new_array[]              = $array;
    //                     }
    //                     $resp['examList'] = $new_array;
    //                 }

    //                 json_output(200, $resp);
    //             }
    //         }
    //     }
    // }

    public function getExamResult()
    {

        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST                          = json_decode(file_get_contents("php://input"), true);
                    $exam_group_class_batch_exam_id = $this->input->post('exam_group_class_batch_exam_id');
                    $student_id                     = $this->input->post('student_id');
                    $student                        = $this->student_model->get($student_id);

                    $dt          = array();
                    $exam_result = $this->examgroup_model->searchExamResult($student->student_session_id, $exam_group_class_batch_exam_id, true, true);

                    $exam_grade = $this->grade_model->getGradeDetails();

                    if (!empty($exam_result->exam_result)) {
                        $exam                                 = new stdClass;
                        $exam->exam_group_class_batch_exam_id = $exam_result->exam_group_class_batch_exam_id;
                        $exam->exam_group_id                  = $exam_result->exam_group_id;
                        $exam->exam                           = $exam_result->exam;
                        $exam->exam_group                     = $exam_result->name;
                        $exam->description                    = $exam_result->description;
                        $exam->exam_type                      = $exam_result->exam_type;
                        $exam->subject_result                 = array();
                        $exam->total_max_marks                = 0;
                        $exam->total_get_marks                = 0;
                        $exam->total_exam_points              = 0;
                        $exam->exam_quality_points            = 0;
                        $exam->exam_credit_hour               = 0;
                        $exam->exam_credit_hour               = 0;
                        $exam->exam_result_status             = "pass";
                        if ($exam_result->exam_result['exam_connection'] == 0) {
                            $exam->is_consolidate = 0;
                            foreach ($exam_result->exam_result['result'] as $exam_result_key => $exam_result_value) {

                                $subject_array = array();
                                if ($exam_result_value->attendence != "present") {
                                    $exam->exam_result_status = "fail";

                                } elseif ($exam_result_value->get_marks < $exam_result_value->min_marks) {

                                    $exam->exam_result_status = "fail";
                                }
                                $exam->total_max_marks = $exam->total_max_marks + $exam_result_value->max_marks;
                                $exam->total_get_marks = $exam->total_get_marks + $exam_result_value->get_marks;
                                // $subject_array['']=$exam_result_value->id;
                                $percentage                                       = ($exam_result_value->get_marks * 100) / $exam_result_value->max_marks;
                                $subject_array['name']                            = $exam_result_value->name;
                                $subject_array['code']                            = $exam_result_value->code;
                                $subject_array['exam_group_class_batch_exams_id'] = $exam_result_value->exam_group_class_batch_exams_id;
                                $subject_array['room_no']                         = $exam_result_value->room_no;
                                $subject_array['max_marks']                       = $exam_result_value->max_marks;
                                $subject_array['min_marks']                       = $exam_result_value->min_marks;
                                $subject_array['subject_id']                      = $exam_result_value->subject_id;
                                $subject_array['attendence']                      = $exam_result_value->attendence;
                                $subject_array['get_marks']                       = $exam_result_value->get_marks;

                                $subject_array['exam_group_exam_results_id'] = $exam_result_value->exam_group_exam_results_id;
                                $subject_array['note']                       = $exam_result_value->note;
                                $subject_array['duration']                   = $exam_result_value->duration;
                                $subject_array['credit_hours']               = $exam_result_value->credit_hours;
                                $subject_array['exam_grade']                 = findExamGrade($exam_grade, $exam_result->exam_type, $percentage);

                                if ($exam_result->exam_type == "gpa") {

                                    $point                                = findGradePoints($exam_grade, $exam_result->exam_type, $percentage);
                                    $exam->exam_quality_points            = $exam->exam_quality_points + ($exam_result_value->credit_hours * $point);
                                    $exam->exam_credit_hour               = $exam->exam_credit_hour + $exam_result_value->credit_hours;
                                    $exam->total_exam_points              = $exam->total_exam_points + $point;
                                    $subject_array['exam_grade_point']    = number_format($point, 2, '.', '');
                                    $subject_array['exam_quality_points'] = $exam_result_value->credit_hours * $point;
                                }
                                $exam->subject_result[] = $subject_array;
                            }
                            $exam->percentage = ($exam->total_get_marks * 100) / $exam->total_max_marks;
                            $exam->division   = getExamDivision($exam->percentage);

                        } else {
                            // print_r($exam_result);
                            $exam->is_consolidate = 1;
                            $exam_connected_exam  = ($exam_result->exam_result['exam_result']['exam_result_' . $exam_result->exam_group_class_batch_exam_id]);

                            if (!empty($exam_connected_exam)) {
                                foreach ($exam_connected_exam as $exam_result_key => $exam_result_value) {

                                    $subject_array = array();
                                    if ($exam_result_value->attendence != "present") {
                                        $exam->exam_result_status = "fail";

                                    } elseif ($exam_result_value->get_marks < $exam_result_value->min_marks) {

                                        $exam->exam_result_status = "fail";
                                    }
                                    $exam->total_max_marks = $exam->total_max_marks + $exam_result_value->max_marks;
                                    $exam->total_get_marks = $exam->total_get_marks + $exam_result_value->get_marks;
                                    // $subject_array['']=$exam_result_value->id;
                                    $percentage                                       = ($exam_result_value->get_marks * 100) / $exam_result_value->max_marks;
                                    $subject_array['name']                            = $exam_result_value->name;
                                    $subject_array['code']                            = $exam_result_value->code;
                                    $subject_array['exam_group_class_batch_exams_id'] = $exam_result_value->exam_group_class_batch_exams_id;
                                    $subject_array['room_no']                         = $exam_result_value->room_no;
                                    $subject_array['max_marks']                       = $exam_result_value->max_marks;
                                    $subject_array['min_marks']                       = $exam_result_value->min_marks;
                                    $subject_array['subject_id']                      = $exam_result_value->subject_id;
                                    $subject_array['attendence']                      = $exam_result_value->attendence;
                                    $subject_array['get_marks']                       = $exam_result_value->get_marks;

                                    $subject_array['exam_group_exam_results_id'] = $exam_result_value->exam_group_exam_results_id;
                                    $subject_array['note']                       = $exam_result_value->note;
                                    $subject_array['duration']                   = $exam_result_value->duration;
                                    $subject_array['credit_hours']               = $exam_result_value->credit_hours;
                                    $subject_array['exam_grade']                 = findExamGrade($exam_grade, $exam_result->exam_type, $percentage);

                                    if ($exam_result->exam_type == "gpa") {

                                        $point                             = findGradePoints($exam_grade, $exam_result->exam_type, $percentage);
                                        $exam->exam_quality_points         = $exam->exam_quality_points + ($exam_result_value->credit_hours * $point);
                                        $exam->exam_credit_hour            = $exam->exam_credit_hour + $exam_result_value->credit_hours;
                                        $exam->total_exam_points           = $exam->total_exam_points + $point;
                                        $subject_array['exam_grade_point'] = number_format($point, 2, '.', '');
                                    }
                                    $exam->subject_result[] = $subject_array;
                                }
                                $exam->percentage = ($exam->total_get_marks * 100) / $exam->total_max_marks;
                                $exam->division   = getExamDivision($exam->percentage);

                            }
                            $consolidate_result                     = new stdClass;
                            $consolidate_get_total                  = 0;
                            $consolidate_total_points               = 0;
                            $consolidate_max_total                  = 0;
                            $consolidate_subjects_total             = 0;
                            $consolidate_result->exam_array         = array();
                            $consolidate_result->consolidate_result = array();
                            $consolidate_result_status              = "pass";
                            if (!empty($exam_result->exam_result['exams'])) {
                                $consolidate_exam_result = "pass";
                                foreach ($exam_result->exam_result['exams'] as $each_exam_key => $each_exam_value) {
                                    if ($exam_result->exam_type != "gpa") {
                                        $consolidate_each = getCalculatedExam($exam_result->exam_result['exam_result'], $each_exam_value->id);

                                        if ($consolidate_each->exam_status == "fail") {
                                            $consolidate_result_status = "fail";
                                        }

                                        $consolidate_get_percentage_mark = getConsolidateRatio($exam_result->exam_result['exam_connection_list'], $each_exam_value->id, $consolidate_each->get_marks);

                                        $each_exam_value->percentage = $consolidate_get_percentage_mark;
                                        $consolidate_get_total       = $consolidate_get_total + ($consolidate_get_percentage_mark);
                                        $consolidate_max_total       = $consolidate_max_total + ($consolidate_each->max_marks);
                                    }

                                    if ($exam_result->exam_type == "gpa") {
                                        $consolidate_each = getCalculatedExamGradePoints($exam_result->exam_result['exam_result'], $each_exam_value->id, $exam_grade, $exam_result->exam_type);

                                        $each_exam_value->total_points = $consolidate_each->total_points;
                                        $each_exam_value->total_exams  = $consolidate_each->total_exams;

                                        $consolidate_exam_result         = ($consolidate_each->total_points / $consolidate_each->total_exams);
                                        $consolidate_get_percentage_mark = getConsolidateRatio($exam_result->exam_result['exam_connection_list'], $each_exam_value->id, $consolidate_exam_result);
                                        $each_exam_value->percentage     = $consolidate_get_percentage_mark;
                                        $consolidate_get_total           = $consolidate_get_total + ($consolidate_get_percentage_mark);

                                        $consolidate_subjects_total = $consolidate_subjects_total + $consolidate_each->total_exams;

                                        $each_exam_value->exam_result = number_format($consolidate_exam_result, 2, '.', '');
                                    }

                                    $consolidate_result->exam_array[] = $each_exam_value;
                                }
                                $consolidate_result->consolidate_result['marks_obtain'] = $consolidate_get_total;
                                $consolidate_result->consolidate_result['marks_total']  = $consolidate_max_total;

                                $consolidate_result->consolidate_result['percentage'] = ($consolidate_get_total * 100) / $consolidate_max_total;
                                $consolidate_result->consolidate_result['division']   = getExamDivision($consolidate_result->consolidate_result['percentage']);
                                if ($exam_result->exam_type != "gpa") {

                                    $consolidate_percentage_grade                            = ($consolidate_get_total * 100) / $consolidate_max_total;
                                    $consolidate_result->consolidate_result['result']        = $consolidate_get_total . "/" . $consolidate_max_total;
                                    $consolidate_result->consolidate_result['grade']         = findExamGrade($exam_grade, $exam_result->exam_type, $consolidate_percentage_grade);
                                    $consolidate_result->consolidate_result['result_status'] = $consolidate_result_status;

                                } elseif ($exam_result->exam_type == "gpa") {
                                    $consolidate_percentage_grade = ($consolidate_get_total * 100) / $consolidate_subjects_total;

                                    $consolidate_result->consolidate_result['result'] = $consolidate_get_total . "/" . $consolidate_subjects_total;

                                    $consolidate_result->consolidate_result['grade'] = findExamGrade($exam_grade, $exam_result->exam_type, $consolidate_percentage_grade);

                                }

                                $consolidate_exam_result_percentage = $consolidate_percentage_grade;

                            }
                            $exam->consolidated_exam_result = $consolidate_result;

                        }
                        $data['exam'] = $exam;
                    }

                    $data['status'] = "200";
                    json_output($response['status'], $data);
                }
            }
        }

    }

    public function getGradeByMarks($marks = 0)
    {
        $gradeList = $this->grade_model->get();
        if (empty($gradeList)) {
            return "empty list";
        } else {

            foreach ($gradeList as $grade_key => $grade_value) {
                if (round($marks) >= $grade_value['mark_from'] && round($marks) <= $grade_value['mark_upto']) {
                    return $grade_value['name'];
                    break;
                }
            }
            return "no record found";
        }
    }

    public function Parent_GetStudentsList()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $array = array();

                    $_POST           = json_decode(file_get_contents("php://input"), true);
                    $parent_id       = $this->input->post('parent_id');
                    $students_array  = $this->student_model->read_siblings_students($parent_id);
                    $array['childs'] = $students_array;
                    json_output($response['status'], $array);
                }
            }
        }

    }
   public function getModuleStatus()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST               = json_decode(file_get_contents("php://input"), true);
                    $user                = $this->input->post('user');
                    $resp['module_list'] = $this->module_model->get($user);
                    json_output($response['status'], $resp);
                }
            }

        }
    }

    public function searchuser()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $data = array();

                    $params     = json_decode(file_get_contents('php://input'), true);
                    $student_id = $params['student_id'];

                    $keyword = $params['keyword'];

                    $chat_user    = $this->chatuser_model->getMyID($student_id, 'student');
                    $chat_user_id = 0;
                    if (!empty($chat_user)) {
                        $chat_user_id = $chat_user->id;
                    }

                    $resp['chat_user'] = $this->chatuser_model->searchForUser($keyword, $chat_user_id, 'student', $student_id);
                    $msges = [];
                    if(!empty($resp['chat_user'])) {
                        $i=0;
                        $j=1;
                        foreach($resp['chat_user'] as $val) {
                            $msges[$i]['id'] = $j;
                            $msges[$i]['staff_id'] = $val->staff_id;
                            $msges[$i]['student_id'] = $val->student_id;
                            $msges[$i]['name'] = $val->name;
                            $msges[$i]['image'] = $val->image;
                            $j++;
                            $i++;

                        }
                    }
                    $resp['chat_user'] = $msges;
                    json_output($response['status'], $resp);
                }
            }

        }

    }

    public function addChatUser()
    {

        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $user_type   = $params['user_type'];
                    $user_id     = $params['user_id'];
                    $student_id  = $params['student_id'];
                    $first_entry = array(
                        'user_type'  => "student",
                        'student_id' => $student_id,
                    );
                    $insert_data = array('user_type' => strtolower($user_type), 'create_student_id' => null);

                    if ($user_type == "Student") {
                        $insert_data['student_id'] = $user_id;
                    } elseif ($user_type == "Staff") {
                        $insert_data['staff_id'] = $user_id;
                    }
                    $insert_message = array(
                        'message'            => 'you are now connected on chat',
                        'chat_user_id'       => 0,
                        'is_first'           => 1,
                        'chat_connection_id' => 0,
                    );

                    //===================
                    $new_user_record = $this->chatuser_model->addNewUserForStudent($first_entry, $insert_data, 'student', $student_id, $insert_message);
                    $json_record     = json_decode($new_user_record);

                    //==================

                    $new_user = $this->chatuser_model->getChatUserDetail($json_record->new_user_id);

                    $chat_user = $this->chatuser_model->getMyID($student_id, 'student');

                    $data['chat_user']  = $chat_user;
                    $chat_connection_id = $json_record->new_user_chat_connection_id;
                    $chat_to_user       = 0;
                    $user_last_chat     = $this->chatuser_model->getLastMessages($chat_connection_id);

                    $chat_connection = $this->chatuser_model->getChatConnectionByID($chat_connection_id);
                    if (!empty($chat_connection)) {
                        $chat_to_user       = $chat_connection->chat_user_one;
                        $chat_connection_id = $chat_connection->id;
                        if ($chat_connection->chat_user_one == $chat_user->id) {
                            $chat_to_user = $chat_connection->chat_user_two;
                        }
                    }

                    $array = array('status' => '1', 'error' => '', 'message' => $this->lang->line('success_message'), 'new_user' => $new_user, 'chat_connection_id' => $json_record->new_user_chat_connection_id, 'chat_records' => $chat_records, 'user_last_chat' => $user_last_chat);

                    json_output($response['status'], $array);
                }
            }

        }

    }

    public function liveclasses()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST      = json_decode(file_get_contents("php://input"), true);
                    $student_id = $this->input->post('student_id');
                    $result     = $this->student_model->get($student_id);

                    $class_id   = $result->class_id;
                    $section_id = $result->section_id;

                    $live_classes = $this->conference_model->getByStudentClassSection($class_id, $section_id);
                    if (!empty($live_classes)) {
                        foreach ($live_classes as $lc_key => $lc_value) {
                            $live_url                            = json_decode($lc_value->return_response);
                            $live_classes[$lc_key]->{'join_url'} = $live_url->join_url;
                            unset($lc_value->return_response);

                        }
                    }

                    $data["live_classes"] = $live_classes;

                    json_output($response['status'], $data);
                }
            }
        }
    }
        public function livehistory()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $_POST = json_decode(file_get_contents("php://input"), true);

                    $insert_data = array(
                        'student_id'    => $this->input->post('student_id'),
                        'conference_id' => $this->input->post('conference_id'),
                    );
                    $this->conference_model->updatehistory($insert_data);
                    $array = array('status' => '1', 'msg' => 'Success');

                    json_output($response['status'], $array);
                }
            }
        }
    }




	// Api by vishnu

    public function teacherLogin()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $email = $params['email'];
                $password = $params['password'];
                $app_key = $params['deviceToken'];
                $response = $this->staff_model->login($email, $password,$app_key);
                $setting_result = $this->setting_model->getSetting1();
                $siteInfo = array(
                    'url' => $setting_result->mobile_api_url,
                    'tracking_url' => $setting_result->tracking_url,
                    'app_logo' => $setting_result->app_logo,
                    'app_primary_color_code' => $setting_result->app_primary_color_code,
                    'app_secondary_color_code' => $setting_result->app_secondary_color_code,
                    'lang_code' => $setting_result->language_code,
                );
                $response['siteInfo'] = $siteInfo;
                
                json_output($response['status'], $response);

            }
        }
    }

    public function getClasses()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $cls = $this->class_model->getCls();
                $data['data'] = $cls;
                json_output('200', $data);
                // json_output('status' => 200, 'message' => 'Successfully login.');
            }
        }
    }

    public function getSectionByClass()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $class_id = $params['class_id'];
                $data = $this->section_model->getClassBySection($class_id);
                json_output('200', $data);

            }
        }
    }

    public function getStudentListByDate()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $class = $params['class_id'];
                $section = $params['section_id'];
                $date = $params['date'];
                $attendencetypes = $this->attendencetype_model->get();
                $data['attendencetypeslist'] = $attendencetypes;
                $resultlist = $this->stuattendence_model->searchAttendenceClassSection($class, $section, date('Y-m-d', strtotime($date)));
                $data['resultlist'] = $resultlist;
                json_output('200', $data);

            }
        }
    }

    public function saveAttendanceByDate()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $class = $params['class_id'];
                $section = $params['section_id'];
                $date = $params['date'];
                $session_ary = $params['student_session'];
                $absent_student_list = array();
                foreach ($session_ary as $key => $value) {
                    $checkForUpdate = $params['attendendence_id' . $value];
                    if ($checkForUpdate != 0) {
                        if (isset($holiday)) {
                            $arr = array(
                                'id' => $checkForUpdate,
                                'student_session_id' => $value,
                                'attendence_type_id' => 5,
                                'remark' => $params["remark" . $value],
                                'date' => date('Y-m-d', $this->customlib->datetostrtotime($date))
                            );
                        } else {
                            $arr = array(
                                'id' => $checkForUpdate,
                                'student_session_id' => $value,
                                'attendence_type_id' => $params['attendencetype' . $value],
                                'remark' => $params["remark" . $value],
                                'date' => date('Y-m-d', strtotime($date))
                            );
                        }
                        $insert_id = $this->stuattendence_model->add($arr);
                    } else {
                        if (isset($holiday)) {
                        $arr = array(
                            'student_session_id' => $value,
                            'attendence_type_id' => 5,
                            'remark' => $params["remark" . $value],
                            'date' => date('Y-m-d', $this->customlib->datetostrtotime($date))
                        );
                        } else {
                            $arr = array(
                                'student_session_id' => $value,
                                'attendence_type_id' => $params['attendencetype' . $value],
                                'remark' => $params["remark" . $value],
                                // 'date' => date('Y-m-d', $this->customlib->datetostrtotime($date))
                                'date' => date('Y-m-d', strtotime($date))
                            );
                        }
                        $insert_id = $this->stuattendence_model->add($arr);
                    }
                    

                    if ($arr['attendence_type_id'] == 4) {
                        $absent_student_list[] = $value;
                    }
                }

                $absent_config = $this->config_attendance['absent'];
                if (!empty($absent_student_list)) {

                    $this->mailsmsconf->mailsms1('absent_attendence', $absent_student_list, $date);
                }
                // print_r($params);
                // die();
                // $attendencetypes = $this->attendencetype_model->get();
                // $data['attendencetypeslist'] = $attendencetypes;
                // $resultlist = $this->stuattendence_model->searchAttendenceClassSection($class, $section, date('Y-m-d', $this->customlib->datetostrtotime($date)));
                $data['msg'] = 'attendence save Successfully!';
                json_output('200', $data);

            }
        }
    }

    public function getStudentAttendanceByDate()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $class = $params['class_id'];
                $section = $params['section_id'];
                $date = $params['date'];
                $attendencetypes = $this->attendencetype_model->get();
                $data['attendencetypeslist'] = $attendencetypes;
                $resultlist = $this->stuattendence_model->searchAttendenceClassSectionPrepare($class, $section, date('Y-m-d', strtotime($date)));
                $data['resultlist'] = $resultlist;
                json_output('200', $data);

            }
        }
    }

    public function getEvents()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $event_content = $this->config->item('ci_front_event_content');
                $listResult = $this->cms_program_model->getByCategory($event_content);
                $data['listResult'] = $listResult;
                json_output('200', $data);
                // json_output('status' => 200, 'message' => 'Successfully login.');
            }
        }
    }

    public function getGallert()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $notice_content = $this->config->item('ci_front_gallery_content');
                $listResult = $this->cms_program_model->getByCategory($notice_content);
                $finalOutput = [];
                if(!empty($listResult)) {
                    $i=0;
                    foreach($listResult as $val) {
                        $finalOutput[$i]['id'] = $val['id'];
                        $finalOutput[$i]['type'] = $val['type'];
                        $finalOutput[$i]['slug'] = $val['slug'];
                        $finalOutput[$i]['url'] = $val['url'];
                        $finalOutput[$i]['title'] = $val['title'];
                        $finalOutput[$i]['date'] = $val['date'];
                        $finalOutput[$i]['event_start'] = $val['event_start'];
                        $finalOutput[$i]['event_end'] = $val['event_end'];
                        $finalOutput[$i]['event_venue'] = $val['event_venue'];
                        $finalOutput[$i]['description'] = $val['description'];
                        $finalOutput[$i]['is_active'] = $val['is_active'];
                        $finalOutput[$i]['created_at'] = $val['created_at'];
                        $finalOutput[$i]['meta_title'] = $val['meta_title'];
                        $finalOutput[$i]['meta_description'] = $val['meta_description'];
                        $finalOutput[$i]['meta_keyword'] = $val['meta_keyword'];
                        $finalOutput[$i]['feature_image'] = $val['feature_image'];
                        $finalOutput[$i]['publish_date'] = $val['publish_date'];
                        $finalOutput[$i]['publish'] = $val['publish'];
                        $finalOutput[$i]['sidebar'] = $val['sidebar'];
                        $gal = $this->cms_program_model->front_cms_program_photos($val['id']);
                        $finalOutput[$i]['album_images'] = $gal;
                        $i++;
                    }
                }
                // echo "<pre>";
                // print_r($listResult);
                // die;
                $data['listResult'] = $finalOutput;
                json_output('200', $data);
                // json_output('status' => 200, 'message' => 'Successfully login.');
            }
        }
    }

    public function getNews()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $notice_content = $this->config->item('ci_front_notice_content');
                $listResult = $this->cms_program_model->getByCategory($notice_content);

                $data['listResult'] = $listResult;
                json_output('200', $data);
                // json_output('status' => 200, 'message' => 'Successfully login.');
            }
        }
    }

    public function getTrackingUrl()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $setting_result = $this->setting_model->getUrl();

                $data['school_url_details'] = $setting_result[0];
                json_output('200', $data);
                // json_output('status' => 200, 'message' => 'Successfully login.');
            }
        }
    }

    

    public function SendPushNotification()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $tokens = $params['device_token'];
                // json_output('200', $tokens);
                $t = "mdMvQSHoSZhnh7nW96Gw53";
                $key = "ExponentPushToken[".$t."]";
                $userId = 'userId from your database';
                $notification = ['title' => 'this is title11111111 live','body' => 'final testing11111 live'];
                  try{

                      $expo = \ExponentPhpSDK\Expo::normalSetup();
                      $expo->notify($userId,$notification);//$userId from database
                      $expo->subscribe($userId, $key); //$userId from database
                        $expo->notify($userId,$notification);
                      $status = 'success';
                }catch(Exception $e){
                        $expo->subscribe($userId, $key); //$userId from database
                        $expo->notify($userId,$notification);
                        $status = 'new subscribtion';
                }
                json_output('200', $status);
                  // echo $status;
                //  $channelName = 'news';
                //   $recipient= 'ExponentPushToken[AAAAbnw9lIU:APA91bHaK2zyhprRzkmoSRDQGvNTG2KC3ZNsOsB0hEeSvHnh_x_dnoHIToHolHgVkcwA3Qn8j7oro4pjTrnrUMXvG4_WAAz6UJqSWU6dIPo7sHpkTCE9qYuziLR_pqB0PZe1QA_2k0o2]';
                  
                //   // You can quickly bootup an expo instance
                //   $expo = \ExponentPhpSDK\Expo::normalSetup();
                  
                //   // Subscribe the recipient to the server
                //   $expo->subscribe($channelName, $recipient);
                  
                //   // Build the notification data
                //   $notification = ['body' => 'Hello World!'];
                  
                //   // Notify an interest with a notification
                //   $expo->notify($channelName, $notification);
                // json_output('200', $expo);
                // $API_ACCESS_KEY = "AAAAZDTRjsM:APA91bFfPp3Z3EewvMs3pcLKiR4XnlL7dPbHuR6vUIdURwZMu0YlHZ_y9Q_g7fn8mUt7JG_RvK47Xn5PfuhWc-CYi_YtYw5O3qC7pzTJ9cyBb5GNb0aI9xLn0qKGXrncN6HqwTif2fgA";
                // $API_ACCESS_KEY = "AAAAbnw9lIU:APA91bHaK2zyhprRzkmoSRDQGvNTG2KC3ZNsOsB0hEeSvHnh_x_dnoHIToHolHgVkcwA3Qn8j7oro4pjTrnrUMXvG4_WAAz6UJqSWU6dIPo7sHpkTCE9qYuziLR_pqB0PZe1QA_2k0o2";
                // $fcmUrl         = "https://fcm.googleapis.com/fcm/send";
                // $notificationData = [
                //     'title'  => 'message title22',
                //     'body'   => 'message description22',
                //     'action' => "",
                //     'sound' => 'mySound'
                // ];

                // $fcmNotification = [

                //     'to'          => $tokens, //single token
                //     'collapseKey' => "{$tokens}",
                //     'data'        => $notificationData,

                // ];
                // $headers = [
                //     'Authorization: key=' . $API_ACCESS_KEY,
                //     'Content-Type: application/json',
                // ];

                // $ch = curl_init();
                // curl_setopt($ch, CURLOPT_URL, $fcmUrl);
                // curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                // $result = curl_exec($ch);
                // json_output('200', $result);
                // curl_close($ch);
                


                // $point1_lat = $params['student_lat'];
                // $point1_long = $params['student_long'];
                // $point2_lat = $params['device_lat'];
                // $point2_long = $params['device_long'];
                
                // $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
                // $distance = round($degrees * 111.13384, 2);
                
                // $busSpeed = 45;
                // $timeTaken = $distance/$busSpeed;
                // $data['distance'] = $distance;
                // $data['timeTaken'] = $timeTaken;
                // json_output('200', $data);
            }
        }
    }

    public function getProfileDetails()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $teacher_id = $params['id'];
                $staff_info = $this->staff_model->getProfile($teacher_id);
                 $custom_fields  = $this->customfield_model->staff_fields();
                $staff_info['custom_fields']  = $custom_fields;
                // $data = $this->section_model->getClassBySection($class_id);
                json_output('200', $staff_info);

            }
        }
    }

    public function getSchoolDetailsByTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
               $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->staff_model->auth();
                if ($response['status'] == 200) {                  

                    $result=  $this->setting_model->getSchoolDisplay();
                    $result->start_month_name=ucfirst($this->customlib->getMonthList($result->start_month));
      
                    

                    json_output($response['status'], $result);
                    // json_output($response['status'], $response);
                }
            }
          
        }
    }

    public function getNotificationsByTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->staff_model->auth();
                if ($response['status'] == 200) {
                    $resp = $this->webservice_model->getNotifications('student');
                    json_output($response['status'], $resp);
                }
            }
        }
    }
    
    public function SaveDashboardToken()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $tokens = $params['device_token'];
                $user_id = $params['user_id'];
                $userType = $params['user_type'];
                if($userType == 'student') {
                    $response = $this->student_model->saveToken($user_id,$tokens);
                    json_output('200', 'Successfully save.');
                }
                if($userType == 'teacher') {
                    $response = $this->staff_model->saveToken($user_id,$tokens);
                    json_output('200', 'Successfully save.');
                } 
            }
        }
    }
    
    public function getSubjectGuropBySection()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $class_id = $params['class_id'];
                $section_id = $params['section_id'];
                // $data = $this->section_model->getClassBySection($class_id);
                
                $data       = $this->subjectgroup_model->getGroupByClassandSection($class_id, $section_id);
                json_output('200', $data);

            }
        }
    }

    public function getGroupsubjects()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $subject_group_id = $params['subject_group_id'];
                $data             = $this->subjectgroup_model->getGroupsubjects($subject_group_id);
                json_output('200', $data);

            }
        }
    }

    public function saveHomework()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                // $params   = json_decode(file_get_contents('php://input'), true);
                $params = $this->input->POST();
                // print_r($params);
                // die;
                $modal_class_id = $params['modal_class_id'];
                $modal_section_id = $params['modal_section_id'];
                $modal_subject_group_id = $params['modal_subject_group_id'];
                $modal_subject_id = $params['modal_subject_id'];
                $homework_date = $params['homework_date'];
                $submit_date = $params['submit_date'];
                $description = $params['description'];
                $staff_id = $params['staff_id'];
                

                $session_id = $this->setting_model->getCurrentSession();

                $record_id = 0;
                $data      = array(
                    'id'                       => $record_id,
                    'session_id'               => $session_id,
                    'class_id'                 => $modal_class_id,
                    'section_id'               => $modal_section_id,
                    'homework_date'            => date('Y-m-d', strtotime($homework_date)),
                    'submit_date'              => date('Y-m-d', strtotime($submit_date)),
                    'staff_id'                 => $staff_id,
                    'subject_group_subject_id' => $modal_subject_id,
                    'description'              => $description,
                    'create_date'              => date("Y-m-d"),
                    'created_by'               => $staff_id,
                    'evaluated_by'             => '',
                );

                $id = $this->homework_model->add1($data);

                if ($record_id > 0) {
                    $id = $record_id;
                } else {

                }

                if (isset($_FILES["userfile"]) && !empty($_FILES['userfile']['name'])) {
                    $uploaddir = '../uploads/homework/';
                    if (!is_dir($uploaddir) && !mkdir($uploaddir)) {
                        die("Error creating folder $uploaddir");
                    }
                    $fileInfo = pathinfo($_FILES["userfile"]["name"]);
                    $document = basename($_FILES['userfile']['name']);

                    $img_name = $id . '.' . $fileInfo['extension'];
                    move_uploaded_file($_FILES["userfile"]["tmp_name"], $uploaddir . $img_name);
                } else {
                    $img_name = "";
                    $document = "";
                }

                $upload_data = array('id' => $id, 'document' => $img_name);
                $this->homework_model->add1($upload_data);
                if ($record_id == 0) {
                    $homework_detail = $this->homework_model->get($id);

                    $sender_details = array(

                        'class_id'      => $modal_class_id,
                        'section_id'    => $modal_section_id,
                        'homework_date' => date($this->customlib->getSchoolDateFormat(), $this->customlib->dateYYYYMMDDtoStrtotime($homework_detail['homework_date'])),
                        'submit_date'   => date($this->customlib->getSchoolDateFormat(), $this->customlib->dateYYYYMMDDtoStrtotime($homework_detail['submit_date'])),
                        'subject'       => $homework_detail['subject_name'],

                    );

                    // $this->mailsmsconf->mailsms('homework', $sender_details);
                }



                json_output('200', 'homework save Successfully!');

            }
        }
    }
    
    public function getOnlineExamTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params             = json_decode(file_get_contents('php://input'), true);
                    $student_id         = $params['student_id'];
                    $result             = $this->student_model->get($student_id);
                    $resp['onlineexam'] = $this->onlineexam_model->getStudentexam($result->student_session_id);
                    json_output($response['status'], $resp);
                }
            }
        }
    }
    
    public function getOnlineExamResultTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params                = json_decode(file_get_contents('php://input'), true);
                    $onlineexam_student_id = $params['onlineexam_student_id'];
                    $exam_id               = $params['exam_id'];

                    $exam = $this->onlineexam_model->get($exam_id);

                    $resp['question_result'] = $this->onlineexam_model->getResultByStudent($onlineexam_student_id, $exam_id);
                    $correct_ans             = 0;
                    $wrong_ans               = 0;
                    $not_attempted           = 0;
                    $total_question          = 0;
                    if (!empty($resp['question_result'])) {
                        $total_question = count($resp['question_result']);

                        foreach ($resp['question_result'] as $result_key => $question_value) {
                            if ($question_value->select_option != null) {

                                if ($question_value->select_option == $question_value->correct) {
                                    $correct_ans++;
                                } else {
                                    $wrong_ans++;
                                }
                            } else {
                                $not_attempted++;
                            }
                        }
                    }
                    $exam['correct_ans']    = $correct_ans;
                    $exam['wrong_ans']      = $wrong_ans;
                    $exam['not_attempted']  = $not_attempted;
                    $exam['total_question'] = $total_question;
                    $exam['score']          = ($correct_ans * 100) / $total_question;
                    $resp['exam']           = $exam;

                    json_output($response['status'], array('result' => $resp));
                }
            }
        }
    }
    
    public function wLog($tag = 'Tag', $data = [])
    {   
        $logFile = null;
        $timezone = date_default_timezone_get();
        
        date_default_timezone_set('Asia/Kolkata');
        
        if(!file_exists(WWW_ROOT . DS . 'ErLogs' . DS . 'logs.txt')) {
            mkdir(WWW_ROOT . DS . 'ErLogs');
            $logFile = fopen(WWW_ROOT . DS . 'ErLogs' . DS . 'logs.txt', "w"); 
        } else {
            $logFile = fopen(WWW_ROOT . DS . 'ErLogs' . DS . 'logs.txt', "a");
        }

        
        $hr = "\n\n" . "----" . date('D j M, Y - h:i:s A',strtotime('now')) . "------\n\n";
        $tag = "\t\t--". $tag . "--\n\n";
        $hrEnd = "\n" . "----------------------------------------\r\n" . "\n";
        ob_start();
        echo print_r($data, true);
        $data = ob_get_clean();
        $data = $hr . $tag . $data . $hrEnd;

        fwrite($logFile, $data);
        fclose($logFile);
        
        date_default_timezone_set($timezone);
    }
    
    public function evaluateHomework()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params             = json_decode(file_get_contents('php://input'), true);
                    $insert_prev  = array();
                    $insert_array = array();
                    $homework_id  = $this->input->post("homework_id");
                    $students     = $this->input->post("student_list");
                    $home_id      = $this->input->post("homework_id");
                    $this->wLog('All Deals', $this->input->post());
                    // echo "<pre>";
                    // print_r($this->input->post());
                    // die;
                    // json_output($response['status'], $this->input->post());
                    foreach ($students as $std_key => $std_value) {
                        if ($std_value == 0) {
                            $insert_array[] = $std_key;
                        } else {
                            $insert_prev[] = $std_value;
                        }
                    }
                    $evaluation_date = $this->input->post('evaluation_date');
                    $evaluated_by    = $this->input->post("evaluated_by");
                    $a = $this->homework_model->addEvaluation($insert_prev, $insert_array, $homework_id, $evaluation_date, $evaluated_by);
                    $array         = array('status' => '1', 'message' => 'data save Successfully');
                    json_output($response['status'], $array);
                    // $result             = $this->student_model->get($student_id);
                    // $resp['onlineexam'] = $this->onlineexam_model->getStudentexam($result->student_session_id);
                    // json_output($response['status'], $student_id);
                }
            }
        }
    }

    public function evaluation()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params             = json_decode(file_get_contents('php://input'), true);
                    $id         = $params['homework_id'];
                    $result               = $this->homework_model->getRecord($id);
                    $class_id             = $result["class_id"];
                    $section_id           = $result["section_id"];
                    $studentlist          = $this->homework_model->getStudents($id);
                    $data["studentlist"]  = $studentlist;
                    $data["result"]       = $result;
                    if (!empty($result)) {
                        $create_data          = $this->staff_model->get($result["created_by"]);
                        $eval_data            = $this->staff_model->get($result["evaluated_by"]);

                        $created_by           = $create_data["name"] . " " . $create_data["surname"];
                        $evaluated_by         = $eval_data["name"] . " " . $eval_data["surname"];
                        $data["created_by"]   = $created_by;
                        $data["evaluated_by"] = $evaluated_by;
                    }
                    $result1 = $this->homework_model->get_homeworkDocByid($id);
                    $data['docs'] = $result1;
                    // $student_List         = $this->input->post("student_List");
                    // echo "<pre>";
                    // print_r($data);
                    // die;
                    json_output($response['status'], $data);
                    // $result             = $this->student_model->get($student_id);
                    // $resp['onlineexam'] = $this->onlineexam_model->getStudentexam($result->student_session_id);
                    // json_output($response['status'], $student_id);
                }
            }
        }
    }
    
    public function getHomeWorkList()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params             = json_decode(file_get_contents('php://input'), true);
                    $class_id                 = $params['class_id'];
                    $section_id               = $params['section_id'];
                    $subject_group_id         = $params['subject_group_id'];
                    $subject_id               = $params['subject_id'];
                    $data['class_id']         = $class_id;
                    $data['section_id']       = $section_id;
                    $data['subject_group_id'] = $subject_group_id;
                    $data['subject_id']       = $subject_id;
                    $homeworklist             = $this->homework_model->search_homework($class_id, $section_id, $subject_group_id, $subject_id);
                      
                    $data["homeworklist"] = $homeworklist;
                     // echo "<pre>";
                     //    print_r($this->input->post());
                     //    print_r($homeworklist);
                     //    die;
                      foreach ($data["homeworklist"] as $key => $value) {
                        $report                                     = $this->homework_model->getEvaluationReport($value["id"]);
                        $data["homeworklist"][$key]["report"]       = $report;
                        $create_data                                = $this->staff_model->get($value["created_by"]);
                        $eval_data                                  = $this->staff_model->get($value["evaluated_by"]);
                        $created_by                                 = $create_data["name"] . " " . $create_data["surname"];
                        $evaluated_by                               = $eval_data["name"] . " " . $create_data["surname"];
                        $data["homeworklist"][$key]["created_by"]   = $created_by;
                        $data["homeworklist"][$key]["evaluated_by"] = $evaluated_by;
                    }
                    json_output($response['status'], $data);
                    
                }
            }
        }
    }
    
    public function teacherDashboard()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                   $params   = json_decode(file_get_contents('php://input'), true);
                    $teacher_id = $params['id'];
                    $staff_info = $this->staff_model->getProfile($teacher_id);
                    $resp['profileInfo'] = $staff_info;
                    // Code for get events_lists
                        $event_content = $this->config->item('ci_front_event_content');
                        $listResult = $this->cms_program_model->getByCategory($event_content);
                        $resp['eventList'] = $listResult;
                    // end code
                    // code for get galary
                        $notice_content = $this->config->item('ci_front_gallery_content');
                        $listResult = $this->cms_program_model->getByCategory($notice_content);
                        $finalOutput = [];
                        if(!empty($listResult)) {
                            $i=0;
                            foreach($listResult as $val) {
                                $finalOutput[$i]['id'] = $val['id'];
                                $finalOutput[$i]['type'] = $val['type'];
                                $finalOutput[$i]['slug'] = $val['slug'];
                                $finalOutput[$i]['url'] = $val['url'];
                                $finalOutput[$i]['title'] = $val['title'];
                                $finalOutput[$i]['date'] = $val['date'];
                                $finalOutput[$i]['event_start'] = $val['event_start'];
                                $finalOutput[$i]['event_end'] = $val['event_end'];
                                $finalOutput[$i]['event_venue'] = $val['event_venue'];
                                $finalOutput[$i]['description'] = $val['description'];
                                $finalOutput[$i]['is_active'] = $val['is_active'];
                                $finalOutput[$i]['created_at'] = $val['created_at'];
                                $finalOutput[$i]['meta_title'] = $val['meta_title'];
                                $finalOutput[$i]['meta_description'] = $val['meta_description'];
                                $finalOutput[$i]['meta_keyword'] = $val['meta_keyword'];
                                $finalOutput[$i]['feature_image'] = $val['feature_image'];
                                $finalOutput[$i]['publish_date'] = $val['publish_date'];
                                $finalOutput[$i]['publish'] = $val['publish'];
                                $finalOutput[$i]['sidebar'] = $val['sidebar'];
                                $gal = $this->cms_program_model->front_cms_program_photos($val['id']);
                                $finalOutput[$i]['album_images'] = $gal;
                                $i++;
                            }
                        }
                        $resp['galaryList'] = $finalOutput;
                    // end code
                        $notice_content = $this->config->item('ci_front_notice_content');
                        $listResult = $this->cms_program_model->getByCategory($notice_content);
                        $resp['newsList'] = $listResult;
                        $resp['noticeList'] = $this->webservice_model->getNotifications('teacher');
                        // code for the dashboard notification
                        $staff_id = $teacher_id;
                        $chat_user = $this->chatuser_model->getMyID($staff_id, 'staff');
                         $notifications = array();
                            if (!empty($chat_user)) {
                              $notifications = $this->chatuser_model->getChatNotification($chat_user->id);
                        }
                        $resp['notificationsInfo'] = $notifications;
                        // end code
                        $custom_fields  = $this->customfield_model->staff_fields();
                        $resp['custom_fields']  = $custom_fields;
                        json_output('200', $resp);
                }
            }
        }
    }
    
    public function chatsearchuserforTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params             = json_decode(file_get_contents('php://input'), true);
                $keyword                 = $params['keyword'];
                $staff_id = $params['staff_id'];

                $chat_user = $this->chatuser_model->getMyID($staff_id, 'staff');


                $chat_user_id = 0;
                if (!empty($chat_user)) {
                    $chat_user_id = $chat_user->id;
                }
                $data['chat_user'] = $this->chatuser_model->searchForUser($keyword, $chat_user_id,'staff',$staff_id);
                $msges = [];
                if(!empty($data['chat_user'])) {
                    $i=0;
                    $j=1;
                    foreach($data['chat_user'] as $val) {
                        $msges[$i]['id'] = $j;
                        $msges[$i]['staff_id'] = $val->staff_id;
                        $msges[$i]['student_id'] = $val->student_id;
                        $msges[$i]['name'] = $val->name;
                        $msges[$i]['image'] = $val->image;
                        $j++;
                        $i++;

                    }
                }
                $data['chat_user'] = $msges;
                
                json_output(200, $data);
                   
            }
        }
    }

    public function chatAddUserByTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $user_type   = $params['user_type'];
                    $user_id     = $params['user_id'];
                    $staff_id  = $params['staff_id'];
                   $first_entry = array(
                        'user_type' => "staff",
                        'staff_id' => $staff_id
                    );
                    $insert_data = array('user_type' => strtolower($user_type), 'create_staff_id' => NULL);

                    if ($user_type == "Student") {
                        $insert_data['student_id'] = $user_id;
                    } elseif ($user_type == "Staff") {
                        $insert_data['staff_id'] = $user_id;
                    }
                    $insert_message = array(
                        'message' => 'you are now connected on chat',
                        'chat_user_id' => 0,
                        'is_first' => 1,
                        'chat_connection_id' => 0
                    );

                    //===================
                    $new_user_record = $this->chatuser_model->addNewUser($first_entry, $insert_data, 'staff', $staff_id, $insert_message);
                    $json_record = json_decode($new_user_record);

                    //==================

                    $new_user = $this->chatuser_model->getChatUserDetail($json_record->new_user_id);

                    $chat_user = $this->chatuser_model->getMyID($staff_id, 'staff');
                    $data['chat_user'] = $chat_user;
                    $chat_connection_id = $json_record->new_user_chat_connection_id;
                    $chat_to_user = 0;
                    $user_last_chat = $this->chatuser_model->getLastMessages($chat_connection_id);

                    $chat_connection = $this->chatuser_model->getChatConnectionByID($chat_connection_id);
                    if (!empty($chat_connection)) {
                        $chat_to_user = $chat_connection->chat_user_one;
                        $chat_connection_id = $chat_connection->id;
                        if ($chat_connection->chat_user_one == $chat_user->id) {
                            $chat_to_user = $chat_connection->chat_user_two;
                        }
                    }

                    $data['chatList'] = $this->chatuser_model->myChatAndUpdate($chat_connection_id, $chat_user->id);
                    // $chat_records = $this->load->view('admin/chat/_partialChatRecord', $data, true);
                    $array = array('status' => '1', 'error' => '', 'message' => $this->lang->line('success_message'), 'new_user' => $new_user,'chat_records' => $data, 'user_last_chat' => $user_last_chat);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }
    
    public function myuserforTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $staff_id = $params['staff_id'];
                    $chat_user = $this->chatuser_model->getMyID($staff_id, 'staff');

                    $data['chat_user'] = array();
                    $data['userList'] = array();

                    if (!empty($chat_user)) {
                        $data['chat_user'] = $chat_user;
                        $data['userList'] = json_decode($this->chatuser_model->myUser($staff_id, $chat_user->id));

                    }
                    // json_output(200, $data);
                    json_output($response['status'], $data);
                }
            }

        }
    }

    public function myuserforStudents()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $student_id = $params['student_id'];;
                    $chat_user  = $this->chatuser_model->getMyID($student_id, 'student');
                    $data['chat_user'] = array();
                    $data['userList']  = array();

                    if(!empty($chat_user)){
                            $data['chat_user'] = $chat_user;
                            $data['userList']  = json_decode($this->chatuser_model->myUser($student_id, $chat_user->id, 'student'));

                    }
                    // json_output(200, $data);
                    json_output($response['status'], $data);
                }
            }

        }
    }

    public function getChatRecordforTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $staff_id = $params['staff_id'];
                    $chat_user = $this->chatuser_model->getMyID($staff_id, 'staff');
                    $data['chat_user'] = $chat_user;
                    $chat_connection_id = $params['chat_connection_id'];
                    $chat_to_user = 0;
                    $user_last_chat = $this->chatuser_model->getLastMessages($chat_connection_id);

                    $chat_connection = $this->chatuser_model->getChatConnectionByID($chat_connection_id);
                    if (!empty($chat_connection)) {
                        $chat_to_user = $chat_connection->chat_user_one;
                        $chat_connection_id = $chat_connection->id;
                        if ($chat_connection->chat_user_one == $chat_user->id) {
                            $chat_to_user = $chat_connection->chat_user_two;
                        }
                    }

                    $data['chatList'] = $this->chatuser_model->myChatAndUpdate($chat_connection_id, $chat_user->id);
                   $msges = [];
                    if(!empty($data['chatList'])) {
                        $i=0;
                        foreach($data['chatList'] as $val) {
                            if($val->message == 'you are now connected on chat') {

                            } else {
                                $msges[$i] = $val;
                                $i++;
                            }

                        }
                    }
                    $data['chatList'] = $msges;
                    $array = array('status' => '1', 'error' => '', 'page' => $data['chatList'], 'chat_to_user' => $chat_to_user, 'chat_connection_id' => $chat_connection_id, 'user_last_chat' => $user_last_chat);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }

    public function getChatRecordforStudent()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $student_id = $params['student_id'];
                    $chat_user  = $this->chatuser_model->getMyID($student_id, 'student');

                    $data['chat_user'] = $chat_user;

                    $chat_connection_id = $params['chat_connection_id'];
                    $chat_to_user       = 0;

                    $user_last_chat = $this->chatuser_model->getLastMessages($chat_connection_id);

                    $chat_connection = $this->chatuser_model->getChatConnectionByID($chat_connection_id);
                    if (!empty($chat_connection)) {
                        $chat_to_user       = $chat_connection->chat_user_one;
                        $chat_connection_id = $chat_connection->id;
                        if ($chat_connection->chat_user_one == $chat_user->id) {
                            $chat_to_user = $chat_connection->chat_user_two;
                        }
                    }

                    $data['chatList'] = $this->chatuser_model->myChatAndUpdate($chat_connection_id, $chat_user->id);
                    $msges = [];
                    if(!empty($data['chatList'])) {
                        $i=0;
                        foreach($data['chatList'] as $val) {
                            if($val->message == 'you are now connected on chat') {

                            } else {
                                $msges[$i] = $val;
                                $i++;
                            }

                        }
                    }
                    $data['chatList'] = $msges;
                    $array            = array('status' => '1', 'error' => '', 'page' => $data['chatList'], 'chat_to_user' => $chat_to_user, 'chat_connection_id' => $chat_connection_id, 'user_last_chat' => $user_last_chat);
                        // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }
    
    
    public function newMessageByTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $chat_connection_id = $params['chat_connection_id'];
                    $chat_to_user = $params['chat_to_user'];
                    $message = $params['message'];
                    $time = $params['time'];
                    $insert_record = array(
                        'chat_user_id' => $chat_to_user,
                        'message' => trim($message),
                        'chat_connection_id' => $chat_connection_id,
                        // 'created_at' => $this->customlib->chatDateTimeformat($time),
                        'created_at' => $time,
                    );

                    $last_insert_id = $this->chatuser_model->addMessage($insert_record);

                    $array = array('status' => '1', 'last_insert_id' => $last_insert_id, 'error' => '', 'message' => $this->lang->line('inserted'));
                        // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }

    public function newMessageByStudent()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $chat_connection_id = $params['chat_connection_id'];
                    $chat_to_user = $params['chat_to_user'];
                    $message = $params['message'];
                    $time = $params['time'];
                    $insert_record      = array(
                        'chat_user_id'       => $chat_to_user,
                        'message'            => trim($message),
                        'chat_connection_id' => $chat_connection_id,
                        'created_at' => $time,
                    );

                    $last_insert_id = $this->chatuser_model->addMessage($insert_record);

                    $array = array('status' => '1', 'last_insert_id' => $last_insert_id, 'error' => '', 'message' => $this->lang->line('inserted'));
                        // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }

    public function chatUpdateByTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $chat_connection_id = $params['chat_connection_id'];
                    $chat_user_id = $params['chat_to_user'];
                    $last_chat_id = $params['last_chat_id'];
                    $staff_id = $params['staff_id'];
                    $user_last_chat = $this->chatuser_model->getLastMessages($chat_connection_id);
                    $data['chat_user_id'] = $chat_user_id;
                    $chat_user = $this->chatuser_model->getMyID($staff_id, 'staff');

                    $data['updated_chat'] = $this->chatuser_model->getUpdatedchat($chat_connection_id, $last_chat_id, $chat_user->id);

                    
                    $array = array('status' => '1', 'error' => '', 'page' => $data['updated_chat'], 'user_last_chat' => $user_last_chat);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }

    public function chatUpdateByStudent()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $chat_connection_id = $params['chat_connection_id'];
                    $chat_user_id = $params['chat_to_user'];
                    $last_chat_id = $params['last_chat_id'];
                    $user_last_chat       = $this->chatuser_model->getLastMessages($chat_connection_id);
                    $data['chat_user_id'] = $chat_user_id;
                    $student_id           = $params['student_id'];
                    $chat_user            = $this->chatuser_model->getMyID($student_id, 'student');

                    $data['updated_chat'] = $this->chatuser_model->getUpdatedchat($chat_connection_id, $last_chat_id, $chat_user->id);

                   
                    $array    = array('status' => '1', 'error' => '', 'page' => $data['updated_chat'], 'user_last_chat' => $user_last_chat);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }
    
    public function mychatnotificationByTeacher()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $staff_id = $params['staff_id'];
                    $chat_user = $this->chatuser_model->getMyID($staff_id, 'staff');
                     $notifications = array();
                        if (!empty($chat_user)) {
                           $notifications = $this->chatuser_model->getChatNotification($chat_user->id);
                    }  
                    $array = array('status' => '1', 'message' => $this->lang->line('success_message'), 'notifications' => $notifications);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }

    public function mychatnotificationByStudent()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $student_id    = $params['student_id'];
                    $chat_user     = $this->chatuser_model->getMyID($student_id, 'student');
                    

                      $notifications = array();
                        if (!empty($chat_user)) {
                           $notifications = $this->chatuser_model->getChatNotification($chat_user->id);
                    }  

                    $array         = array('status' => '1', 'message' => $this->lang->line('success_message'), 'notifications' => $notifications);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }
    
    public function saveMeetingLink()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $staff_id    = $params['staff_id'];
                    $modal_class_id    = $params['modal_class_id'];
                    $modal_section_id    = $params['modal_section_id'];
                    $modal_subject_group_id    = $params['modal_subject_group_id'];
                    $modal_subject_id    = $params['modal_subject_id'];
                    $meeting_time    = $params['meeting_time'];
                    // get staff info
                    $getStaffDetails=array('id'=>$staff_id);
                    $staffInfo = $this->auth_model->selectDatabyWhere('staff',$getStaffDetails);
                    $staffName = $staffInfo[0]->name;
                    // end code
                    // get class info
                    $getClassDetails=array('id'=>$modal_class_id);
                    $ClassInfo = $this->auth_model->selectDatabyWhere('classes',$getClassDetails);
                    $className = $ClassInfo[0]->class;
                    // end code
                    // get section info
                    $getSectionDetails=array('id'=>$modal_section_id);
                    $SectionInfo = $this->auth_model->selectDatabyWhere('sections',$getSectionDetails);
                    $sectionName = $SectionInfo[0]->section;
                    // end code
                    // get subject group info
                    $getSubjectGroupDetails=array('id'=>$modal_subject_group_id);
                    $SubjectGroupInfo = $this->auth_model->selectDatabyWhere('subject_groups',$getSubjectGroupDetails);
                    $subjectGroupName = $SubjectGroupInfo[0]->name;
                    // end code
                    // get subject group info
                    $getSubjectGSDetails=array('id'=>$modal_subject_id);
                    $SubjectGSInfo = $this->auth_model->selectDatabyWhere('subject_group_subjects',$getSubjectGSDetails);
                    $subject_id = $SubjectGSInfo[0]->subject_id;
                    // end code
                    // get subject info
                    $getSubjectDetails=array('id'=>$subject_id);
                    $SubjectInfo = $this->auth_model->selectDatabyWhere('subjects',$getSubjectDetails);
                    $subjectName = $SubjectInfo[0]->name;
                    // end code
                    $sname = str_replace(' ','_',$staffName);
                    $cname = str_replace(' ','_',$className);
                    $secname = str_replace(' ','_',$sectionName);
                    $subname = str_replace(' ','_',$subjectName);
                    $meetT = str_replace('-','_',$meeting_time);
                    $meetT = str_replace(':','_',$meetT);
                    $roomName = 'https://meet.jit.si/'.$sname.'-'.$cname.'-'.$secname.'-'.$subname.'-'.$meetT;
                    $res['staffName']=$staffName;
                    $res['className']=$className;
                    $res['sectionName']=$sectionName;
                    $res['subjectGroupName']=$subjectGroupName;
                    $res['subjectName']=$subjectName;
                    $res['roomName']=$roomName;

                    $roomInfo = array('staff_id'=>$staff_id, 'modal_class_id'=>$modal_class_id, 'modal_section_id'=>$modal_section_id, 'modal_subject_group_id'=>$modal_subject_group_id,'modal_subject_id'=>$modal_subject_id, 'meeting_time'=>$meeting_time,'staffName'=>$staffName,'className'=>$className,'sectionName'=>$sectionName,'subjectGroupName'=>$subjectGroupName,'subjectName'=>$subjectName,'roomName'=>$roomName,'created_at'=>date('Y-m-d H:i:s'));
                     $result = $this->auth_model->insertData('meeting_rooms',$roomInfo);
                    $array         = array('status' => '1', 'message' => 'ok', 'data' => $roomName);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }

    public function getMeetings()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $todayDate = date('Y-m-d H:i:s');
                // get class info
                    $getClassDetails=array('meeting_time >'=>$todayDate);
                    $meetingInfo = $this->auth_model->selectDatabyWhere('meeting_rooms',$getClassDetails);
                   
                // end code

                    $array         = array('status' => '1', 'message' => 'ok', 'data' => $meetingInfo);
                    json_output(200, $array);
                    // json_output($response['status'], $array);
                // }
            }

        }
    }

    public function updateMeetingLink()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $meeting_id    = $params['meeting_id'];
                    $staff_id    = $params['staff_id'];
                    $modal_class_id    = $params['modal_class_id'];
                    $modal_section_id    = $params['modal_section_id'];
                    $modal_subject_group_id    = $params['modal_subject_group_id'];
                    $modal_subject_id    = $params['modal_subject_id'];
                    $meeting_time    = $params['meeting_time'];
                    // get staff info
                    $getStaffDetails=array('id'=>$staff_id);
                    $staffInfo = $this->auth_model->selectDatabyWhere('staff',$getStaffDetails);
                    $staffName = $staffInfo[0]->name;
                    // end code
                    // get class info
                    $getClassDetails=array('id'=>$modal_class_id);
                    $ClassInfo = $this->auth_model->selectDatabyWhere('classes',$getClassDetails);
                    $className = $ClassInfo[0]->class;
                    // end code
                    // get section info
                    $getSectionDetails=array('id'=>$modal_section_id);
                    $SectionInfo = $this->auth_model->selectDatabyWhere('sections',$getSectionDetails);
                    $sectionName = $SectionInfo[0]->section;
                    // end code
                    // get subject group info
                    $getSubjectGroupDetails=array('id'=>$modal_subject_group_id);
                    $SubjectGroupInfo = $this->auth_model->selectDatabyWhere('subject_groups',$getSubjectGroupDetails);
                    $subjectGroupName = $SubjectGroupInfo[0]->name;
                    // end code
                    // get subject group info
                    $getSubjectGSDetails=array('id'=>$modal_subject_id);
                    $SubjectGSInfo = $this->auth_model->selectDatabyWhere('subject_group_subjects',$getSubjectGSDetails);
                    $subject_id = $SubjectGSInfo[0]->subject_id;
                    // end code
                    // get subject info
                    $getSubjectDetails=array('id'=>$subject_id);
                    $SubjectInfo = $this->auth_model->selectDatabyWhere('subjects',$getSubjectDetails);
                    $subjectName = $SubjectInfo[0]->name;
                    // end code
                    $sname = str_replace(' ','_',$staffName);
                    $cname = str_replace(' ','_',$className);
                    $secname = str_replace(' ','_',$sectionName);
                    $subname = str_replace(' ','_',$subjectName);
                    $meetT = str_replace('-','_',$meeting_time);
                    $meetT = str_replace(':','_',$meetT);
                    $roomName = 'https://meet.jit.si/'.$sname.'-'.$cname.'-'.$secname.'-'.$subname.'-'.$meetT;
                    $res['staffName']=$staffName;
                    $res['className']=$className;
                    $res['sectionName']=$sectionName;
                    $res['subjectGroupName']=$subjectGroupName;
                    $res['subjectName']=$subjectName;
                    $res['roomName']=$roomName;

                    $roomInfo = array('staff_id'=>$staff_id, 'modal_class_id'=>$modal_class_id, 'modal_section_id'=>$modal_section_id, 'modal_subject_group_id'=>$modal_subject_group_id,'modal_subject_id'=>$modal_subject_id, 'meeting_time'=>$meeting_time,'staffName'=>$staffName,'className'=>$className,'sectionName'=>$sectionName,'subjectGroupName'=>$subjectGroupName,'subjectName'=>$subjectName,'roomName'=>$roomName,'created_at'=>date('Y-m-d H:i:s'));
                    $where=array('id'=>$meeting_id);
                     $result = $this->auth_model->updateData('meeting_rooms',$roomInfo,$where);
                    $array         = array('status' => '1', 'message' => 'ok', 'data' => $roomName);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }
    
     public function liveClassList()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $staff_id    = $params['staff_id'];
                    $conferencesInfo = $this->conference_model->getByStaff($staff_id);
                    $res=[];
                    $i=0;
                    if(!empty($conferencesInfo)){
                        foreach($conferencesInfo as $val) {
                            // echo "<pre>";
                            // print_r($val->id);
                            // die;
                            $res[$i]['id'] = $val->id;
                            $res[$i]['purpose'] = $val->purpose;
                            $res[$i]['staff_id'] = $val->staff_id;
                            $res[$i]['created_id'] = $val->created_id;
                            $res[$i]['title'] = $val->title;
                            $res[$i]['date'] = $val->date;
                            $res[$i]['duration'] = $val->duration;
                            $res[$i]['password'] = $val->password;
                            $res[$i]['subject'] = $val->subject;
                            $res[$i]['class_id'] = $val->class_id;
                            $res[$i]['section_id'] = $val->section_id;
                            $res[$i]['session_id'] = $val->session_id;
                            $res[$i]['host_video'] = $val->host_video;
                            $res[$i]['client_video'] = $val->client_video;
                            $res[$i]['description'] = $val->description;
                            $res[$i]['timezone'] = $val->timezone;
                            $res[$i]['return_response'] = json_decode($val->return_response);
                            $res[$i]['api_type'] = $val->api_type;
                            $res[$i]['status'] = $val->status;
                            $res[$i]['created_at'] = $val->created_at;
                            $res[$i]['create_for_surname'] = $val->create_for_surname;
                            $res[$i]['create_by_name'] = $val->create_by_name;
                            $res[$i]['create_by_surname'] = $val->create_by_surname;
                            $res[$i]['create_by_role_name'] = $val->create_by_role_name;
                            $res[$i]['create_by_employee_id'] = $val->create_by_employee_id;
                            $i++;
                        }
                    }
                    
                    $array         = array('status' => '1', 'message' => 'ok', 'data' => $res);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }
    
    public function liveMeeting()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $staff_id    = $params['staff_id'];
                    $conferencesInfo = $this->conference_model->getStaffMeeting($staff_id);
                    $res=[];
                    $i=0;
                    if(!empty($conferencesInfo)){
                        foreach($conferencesInfo as $val) {
                            // echo "<pre>";
                            // print_r($val->id);
                            // die;
                            $res[$i]['id'] = $val->id;
                            $res[$i]['purpose'] = $val->purpose;
                            $res[$i]['staff_id'] = $val->staff_id;
                            $res[$i]['created_id'] = $val->created_id;
                            $res[$i]['title'] = $val->title;
                            $res[$i]['date'] = $val->date;
                            $res[$i]['duration'] = $val->duration;
                            $res[$i]['password'] = $val->password;
                            $res[$i]['subject'] = $val->subject;
                            $res[$i]['class_id'] = $val->class_id;
                            $res[$i]['section_id'] = $val->section_id;
                            $res[$i]['session_id'] = $val->session_id;
                            $res[$i]['host_video'] = $val->host_video;
                            $res[$i]['client_video'] = $val->client_video;
                            $res[$i]['description'] = $val->description;
                            $res[$i]['timezone'] = $val->timezone;
                            $res[$i]['return_response'] = json_decode($val->return_response);
                            $res[$i]['api_type'] = $val->api_type;
                            $res[$i]['status'] = $val->status;
                            $res[$i]['created_at'] = $val->created_at;
                            $res[$i]['create_for_surname'] = $val->create_for_surname;
                            $res[$i]['create_by_name'] = $val->create_by_name;
                            $res[$i]['create_by_surname'] = $val->create_by_surname;
                            $res[$i]['create_by_role_name'] = $val->create_by_role_name;
                            $res[$i]['create_by_employee_id'] = $val->create_by_employee_id;
                            $i++;
                        }
                    }
                    // $res['id']
                    // echo "<pre>";
                    // print_r($conferencesInfo);
                    // print_r(json_decode($conferencesInfo[0]->return_response));
                    // die;
                    $array         = array('status' => '1', 'message' => 'ok', 'data' => $res);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }
    
    public function class_report()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $response = $this->auth_model->auth1();
                if ($response['status'] == 200) {
                    $params      = json_decode(file_get_contents('php://input'), true);
                    $data = array();
                    $class_id    = $params['class_id'];
                    $section_id    = $params['section_id'];
                    $liveclassList = $this->conferencehistory_model->getclass($class_id, $section_id);
                    
                    $array         = array('status' => '1', 'message' => 'ok', 'data' => $liveclassList);
                    // json_output(200, $array);
                    json_output($response['status'], $array);
                }
            }

        }
    }
    
    
    
    

}
