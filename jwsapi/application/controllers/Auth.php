<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model');
        $this->load->model('setting_model');
        // $this->load->library('mailer');
        // $this->load->library(array('customlib', 'enc_lib'));
    }

    public function login()
    {
        $method = $this->input->server('REQUEST_METHOD');

        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad request.'));
        } else {
            $check_auth_client = $this->auth_model->check_auth_client();
            if ($check_auth_client == true) {
                $params   = json_decode(file_get_contents('php://input'), true);
                $username = $params['username'];
                $password = $params['password'];
                $app_key  = $params['deviceToken'];
                $response = $this->auth_model->login($username, $password, $app_key);
                $setting_result = $this->setting_model->getSetting1();
                $siteInfo = array(
                    'url' => $setting_result->mobile_api_url,
                    'tracking_url' => $setting_result->tracking_url,
                    'app_logo' => $setting_result->app_logo,
                    'app_primary_color_code' => $setting_result->app_primary_color_code,
                    'app_secondary_color_code' => $setting_result->app_secondary_color_code,
                    'lang_code' => $setting_result->language_code,
                );
                $response['siteInfo'] = $siteInfo;
                json_output($response['status'], $response);

            }
        }
    }

}
