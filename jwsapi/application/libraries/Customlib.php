<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Customlib
{

    public $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('Setting_model');
        $this->CI->load->model('Notificationsetting_model');
    }

    public function getMonthList($month=0)
    {
        $months = array(
            0  => '',
            1  => 'january',
            2  => 'february',
            3  => 'march',
            4  => 'april',
            5  => 'may',
            6  => 'june',
            7  => 'july',
            8  => 'august',
            9  => 'september',
            10 => 'october',
            11 => 'november',
            12 => 'decmber');
     
        return $months[$month];
    }

    public function getDaysname()
    {
        $status              = array();
        $status['Monday']    = 'Monday';
        $status['Tuesday']   = 'Tuesday';
        $status['Wednesday'] = 'Wednesday';
        $status['Thursday']  = 'Thursday';
        $status['Friday']    = 'Friday';
        $status['Saturday']  = 'Saturday';
        $status['Sunday']    = 'Sunday';
        return $status;
    }
    public function getSchoolName()
    {
        $admin = $this->CI->Setting_model->getSetting();
        return $admin->name;
    }

    function getSchoolDateFormat() {
        $admin = $this->CI->session->userdata('admin');
        if ($admin) {
            return $admin['date_format'];
        } else if ($this->CI->session->userdata('student')) {
            $student = $this->CI->session->userdata('student');
            return $student['date_format'];
        }
    }
    
    function datetostrtotime($date) {
        $format = $this->getSchoolDateFormat();

        if ($format == 'd-m-Y')
            list($day, $month, $year) = explode('-', $date);
        if ($format == 'd/m/Y')
            list($day, $month, $year) = explode('/', $date);
        if ($format == 'd-M-Y')
            list($day, $month, $year) = explode('-', $date);
        if ($format == 'd.m.Y')
            list($day, $month, $year) = explode('.', $date);
        if ($format == 'm-d-Y')
            list($month, $day, $year) = explode('-', $date);
        if ($format == 'm/d/Y')
            list($month, $day, $year) = explode('/', $date);
        if ($format == 'm.d.Y')
            list($month, $day, $year) = explode('.', $date);
        $date = $year . "-" . $month . "-" . $day;
        return strtotime($date);
    }

    function sendMailSMS($find) {
        print_r($find);
        die;
        $notifications = $this->CI->notificationsetting_model->get();


        if (!empty($notifications)) {
            foreach ($notifications as $note_key => $note_value) {
                if ($note_value->type == $find) {
                    return array('mail' => $note_value->is_mail, 'sms' => $note_value->is_sms, 'notification' => $note_value->is_notification);
                }
            }
        }
        return false;
    }

    public function getUserData()
    {
        $result         = $this->getLoggedInUserData();
        $id             = $result["id"];
        $data           = $this->CI->staff_model->get($id);
        $setting_result = $this->CI->setting_model->get();
        if (!empty($setting_result)) {
            $data["class_teacher"] = $setting_result[0]["class_teacher"];
        } else {
            $data["class_teacher"] = "yes";
        }
        return $data;
    }
    public function getLoggedInUserData()
    {
        $admin = $this->CI->session->userdata('admin');
        if ($admin) {
            return $admin;
        } else if ($this->CI->session->userdata('student')) {
            $student = $this->CI->session->userdata('student');
            return $student;
        }
    }
    public function dateYYYYMMDDtoStrtotime($date = null)
    {

        if (strtotime($date) == 0) {
            return "";
        }

        $date_formated = date_parse_from_format('Y-m-d', $date);
        $year          = $date_formated['year'];
        $month         = $date_formated['month'];
        $day           = $date_formated['day'];

        $date = $year . "-" . $month . "-" . $day;

        return strtotime($date);
    }
    public function getSchoolDateFormat1($date_only = true, $time = false)
    {

        $setting_result = $this->CI->setting_model->get();

        $time_format = $setting_result[0]['time_format'];

        $hi_format = ' h:i A';
        $Hi_format = ' H:i';

        $admin = $this->CI->session->userdata('admin');
        if ($admin) {
            if ($date_only && !$time) {

                return $admin['date_format'];
            } elseif ($time_format == "24-hour") {

                return $admin['date_format'] . $Hi_format;
            } elseif ($time_format == "12-hour") {

                return $admin['date_format'] . $hi_format;
            }
        } else if ($this->CI->session->userdata('student')) {

            $student = $this->CI->session->userdata('student');
            if ($date_only && !$time) {

                return $student['date_format'];
            } elseif ($time_format == "24-hour") {

                return $student['date_format'] . $Hi_format;
            } elseif ($time_format == "12-hour") {

                return $student['date_format'] . $hi_format;
            }
        }
    }
    
    public function chatDateTimeformat($date)
    {

        $date_formated = date_parse_from_format('d M Y, H:i:s', $date);
        $year          = $date_formated['year'];
        $month         = str_pad($date_formated['month'], 2, "0", STR_PAD_LEFT);
        $day           = str_pad($date_formated['day'], 2, "0", STR_PAD_LEFT);
        $hour          = str_pad($date_formated['hour'], 2, "0", STR_PAD_LEFT);
        $minute        = str_pad($date_formated['minute'], 2, "0", STR_PAD_LEFT);
        $second        = str_pad($date_formated['second'], 2, "0", STR_PAD_LEFT);

        $format_date = $year . "-" . $month . "-" . $day . " " . $hour . ":" . $minute . ":" . $second;
        return $format_date;
    }
}
