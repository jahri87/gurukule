<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mailsmsconf {

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->config->load("mailsms");
        $this->CI->load->library('smsgateway');
        $this->CI->load->library('noticegateway');
        $this->CI->load->library('mailgateway');
        $this->CI->load->model('examresult_model');
        $this->CI->load->model('student_model');
        $this->config_mailsms = $this->CI->config->item('mailsms');
    }

    public function mailsms($send_for, $sender_details, $date = NULL, $exam_schedule_array = NULL) {

        $send_for = $this->config_mailsms[$send_for];
        // $send_for = $send_for;
        $send_for1 = $send_for;
//=========
        $chk_mail_sms = $this->CI->customlib->sendMailSMS($send_for);
        if (!empty($chk_mail_sms)) {
            if ($send_for == "student_admission") {
                if ($chk_mail_sms['mail']) {
                    $this->CI->mailgateway->sentRegisterMail($sender_details['student_id'], $sender_details['email']);
                }
                if ($chk_mail_sms['sms']) {
                    $this->CI->smsgateway->sentRegisterSMS($sender_details['student_id'], $sender_details['contact_no']);
                }
               
            } elseif ($send_for == "exam_result") {
                $this->sendResult($chk_mail_sms, $sender_details, $exam_schedule_array);
            } elseif ($send_for == "login_credential") {
                if ($chk_mail_sms['mail']) {

                    $this->CI->mailgateway->sendLoginCredential($chk_mail_sms, $sender_details);
                }
                if ($chk_mail_sms['sms']) {
                    $this->CI->smsgateway->sendLoginCredential($chk_mail_sms, $sender_details);
                }
                

            } elseif ($send_for == "fee_submission") {
                $invoice = json_decode($sender_details['invoice']);

                if ($chk_mail_sms['mail']) {
                    $this->CI->mailgateway->sentAddFeeMail($invoice->invoice_id, $invoice->sub_invoice_id, $sender_details['email']);
                }
                if ($chk_mail_sms['sms']) {
                    $this->CI->smsgateway->sentAddFeeSMS($invoice->invoice_id, $invoice->sub_invoice_id, $sender_details['contact_no']);
                }
                 if ($chk_mail_sms['notification']) {
                    $this->CI->noticegateway->sentAddFeeNotification($invoice->invoice_id, $invoice->sub_invoice_id, $sender_details);
                }
            } elseif ($send_for == "absent_attendence") {

                $this->sendAbsentAttendance($chk_mail_sms, $sender_details, $date);
            }elseif ($send_for == "notification") {
                if ($chk_mail_sms['notification']) {
                    
                    $this->CI->noticegateway->sentNotification($chk_mail_sms, $sender_details);
                }
            }  elseif ($send_for1 == "homework") {


                $this->sendHomework($chk_mail_sms, $sender_details, $chk_mail_sms['template']);
            } else {
                
            }
        }
        //===============
    }

    public function sendResult($chk_mail_sms, $exam_result, $exam_schedule_array) {
        if ($chk_mail_sms['mail'] OR $chk_mail_sms['sms']) {
            $get_result = $this->getStudentResult($exam_result, $exam_schedule_array);

            foreach ($get_result as $res_key => $res_value) {
                $result = "passed";
                $exam_name = "";
                $total_marks = 0;
                $notification_to = array();
                $achive_marks = 0;
                $student_name = "";
                $guardian_phone = "";
                $email = "";
                foreach ($res_value as $each_key => $each_value) {
                    $subject_passing_marks = $each_value['passing_marks'];
                    $subject_get_marks = $each_value['get_marks'];
                    if ($subject_passing_marks > $subject_get_marks) {
                        $result = "failed";
                    }

                    $total_marks = $total_marks + $each_value['full_marks'];
                    $achive_marks = $achive_marks + $each_value['get_marks'];
                    $student_name = $each_value['firstname'] . " " . $each_value['lastname'];
                    $email = $each_value['email'];
                    $notification_to=array('app_key'=>$each_value['app_key'],'parent_app_key'=>$each_value['parent_app_key']);
                    $exam_name = $each_value['exam_name'];
                    $guardian_phone = $each_value['guardian_phone'];
                }

                //===========send Mail and sms================
                $detail = array(
                    'result' => $result,
                    'notification_to'=>$notification_to,
                    'total_marks' => $total_marks,
                    'achive_marks' => $achive_marks,
                    'student_name' => $student_name,
                    'exam_name' => $exam_name,
                    'email' => $email,
                    'guardian_phone' => $guardian_phone,
                );
                if ($chk_mail_sms['mail']) {
                    $this->CI->mailgateway->sentExamResultMail($detail);
                }
                if ($chk_mail_sms['sms']) {
                    $this->CI->smsgateway->sentExamResultSMS($detail);
                }
                  if ($chk_mail_sms['notification']) {
                    $this->CI->noticegateway->sentExamResultNotification($detail);
                }

            }
   
        }
    }

    public function getStudentResult($ex_array, $exam_schedule_array) {
        $result = array();
        $exam_schedule = implode(',', $exam_schedule_array);
        foreach ($ex_array as $ex_k => $ex_v) {
            $result[] = $this->CI->examresult_model->getStudentExamResultByStudent($ex_v, $ex_k, $exam_schedule);
        }
        return $result;
    }

    public function sendAbsentAttendance($chk_mail_sms, $student_session_array, $date) {

        if ($chk_mail_sms['mail'] OR $chk_mail_sms['sms']) {
            $student_result = $this->getAbsentStudentlist($student_session_array);

            if (!empty($student_result)) {
                foreach ($student_result as $student_result_k => $student_result_v) {
                    $detail = array(
                        'parent_app_key' => $student_result_v->parent_app_key,
                     
                        'contact_no' => $student_result_v->guardian_phone,
                        'email' => $student_result_v->guardian_email,
                        'date' => $date,
                        'student_name' => $student_result_v->firstname . " " . $student_result_v->lastname,
                    );

                    if ($chk_mail_sms['mail']) {
                        $this->CI->mailgateway->sentAbsentStudentMail($detail);
                    }
                    if ($chk_mail_sms['sms']) {

                        $this->CI->smsgateway->sentAbsentStudentSMS($detail);
                    } 
                    if ($chk_mail_sms['notification']) {

                        $this->CI->noticegateway->sentAbsentStudentNotification($detail);
                    }
                }
            }
        }
    }

    public function getAbsentStudentlist($student_session_array) {

        $result = $this->CI->student_model->getStudentListBYStudentsessionID($student_session_array);
        if (!empty($result)) {
            return $result;
        }
        return false;
    }

    public function mailsms1($send_for, $sender_details, $date = NULL, $exam_schedule_array = NULL) {

        $send_for = $this->config_mailsms[$send_for];
        
//=========
        // $chk_mail_sms = $this->CI->customlib->sendMailSMS($send_for);
        
        if ($send_for == "student_admission") {
            if ($chk_mail_sms['mail']) {
                $this->CI->mailgateway->sentRegisterMail($sender_details['student_id'], $sender_details['email']);
            }
            if ($chk_mail_sms['sms']) {
                $this->CI->smsgateway->sentRegisterSMS($sender_details['student_id'], $sender_details['contact_no']);
            }
           
        } elseif ($send_for == "exam_result") {
            $this->sendResult($chk_mail_sms, $sender_details, $exam_schedule_array);
        } elseif ($send_for == "login_credential") {
            if ($chk_mail_sms['mail']) {

                $this->CI->mailgateway->sendLoginCredential($chk_mail_sms, $sender_details);
            }
            if ($chk_mail_sms['sms']) {
                $this->CI->smsgateway->sendLoginCredential($chk_mail_sms, $sender_details);
            }
            

        } elseif ($send_for == "fee_submission") {
            $invoice = json_decode($sender_details['invoice']);

            if ($chk_mail_sms['mail']) {
                $this->CI->mailgateway->sentAddFeeMail($invoice->invoice_id, $invoice->sub_invoice_id, $sender_details['email']);
            }
            if ($chk_mail_sms['sms']) {
                $this->CI->smsgateway->sentAddFeeSMS($invoice->invoice_id, $invoice->sub_invoice_id, $sender_details['contact_no']);
            }
             if ($chk_mail_sms['notification']) {
                $this->CI->noticegateway->sentAddFeeNotification($invoice->invoice_id, $invoice->sub_invoice_id, $sender_details);
            }
        } elseif ($send_for == "absent_attendence") {
            // $this->sendAbsentAttendance($chk_mail_sms, $sender_details, $date);
            $this->sendAbsentAttendance1($send_for, $sender_details, $date);
        }elseif ($send_for == "notification") {
            if ($chk_mail_sms['notification']) {
                
                $this->CI->noticegateway->sentNotification($chk_mail_sms, $sender_details);
            }
        } else {
            
        }
        
        //===============
    }

    public function sendAbsentAttendance1($chk_mail_sms, $student_session_array, $date) {
        $student_result = $this->getAbsentStudentlist($student_session_array);

        if (!empty($student_result)) {
            foreach ($student_result as $student_result_k => $student_result_v) {
                $detail = array(
                    'parent_app_key' => $student_result_v->parent_app_key,
                 
                    'contact_no' => $student_result_v->guardian_phone,
                    'email' => $student_result_v->guardian_email,
                    'date' => $date,
                    'student_name' => $student_result_v->firstname . " " . $student_result_v->lastname,
                );
                // print_r($detail);
                // die;
                $this->CI->mailgateway->sentAbsentStudentMail($detail);
                $this->CI->smsgateway->sentAbsentStudentSMS($detail);
                $this->CI->noticegateway->sentAbsentStudentNotification($detail);
                
            }
        }
    }

    public function sendHomework($chk_mail_sms, $student_details, $template)
    {
        $student_sms_list          = array();
        $student_email_list        = array();
        $student_notification_list = array();
        if ($chk_mail_sms['mail'] or $chk_mail_sms['sms'] or $chk_mail_sms['notification']) {
            $class_id      = ($student_details['class_id']);
            $section_id    = ($student_details['section_id']);
            $homework_date = $student_details['homework_date'];
            $submit_date   = $student_details['submit_date'];
            $subject       = $student_details['subject'];
            $student_list  = $this->CI->student_model->getStudentByClassSectionID($class_id, $section_id);

            if (!empty($student_list)) {
                foreach ($student_list as $student_key => $student_value) {

                    if ($student_value['app_key'] != "") {
                        $student_notification_list[] = array(
                            'app_key'         => $student_value['app_key'],
                            'class'         => $student_value['class'],
                            'section'       => $student_value['section'],
                            'homework_date' => $homework_date,
                            'submit_date'   => $submit_date,
                            'subject'       => $subject,
                            'admission_no'  => $student_value['admission_no'],
                            'student_name'  => $student_value['firstname'] . " " . $student_value['lastname'],
                        );

                    }

                    if ($student_value['email'] != "") {
                        $student_email_list[$student_value['email']] = array(
                            'class'         => $student_value['class'],
                            'section'       => $student_value['section'],
                            'homework_date' => $homework_date,
                            'submit_date'   => $submit_date,
                            'subject'       => $subject,
                            'admission_no'  => $student_value['admission_no'],
                            'student_name'  => $student_value['firstname'] . " " . $student_value['lastname'],
                        );

                    }
                    if ($student_value['guardian_email'] != "") {
                        $student_email_list[$student_value['guardian_email']] = array(
                            'class'         => $student_value['class'],
                            'section'       => $student_value['section'],
                            'homework_date' => $homework_date,
                            'submit_date'   => $submit_date,
                            'subject'       => $subject,
                            'admission_no'  => $student_value['admission_no'],
                            'student_name'  => $student_value['firstname'] . " " . $student_value['lastname'],
                        );

                    }
                    if ($student_value['mobileno'] != "") {
                        $student_sms_list[$student_value['mobileno']] = array(
                            'class'         => $student_value['class'],
                            'section'       => $student_value['section'],
                            'homework_date' => $homework_date,
                            'submit_date'   => $submit_date,
                            'subject'       => $subject,
                            'admission_no'  => $student_value['admission_no'],
                            'student_name'  => $student_value['firstname'] . " " . $student_value['lastname'],
                        );

                    }
                    if ($student_value['guardian_phone'] != "") {
                        $student_sms_list[$student_value['guardian_phone']] = array(
                            'class'         => $student_value['class'],
                            'section'       => $student_value['section'],
                            'homework_date' => $homework_date,
                            'submit_date'   => $submit_date,
                            'subject'       => $subject,
                            'admission_no'  => $student_value['admission_no'],
                            'student_name'  => $student_value['firstname'] . " " . $student_value['lastname'],
                        );
                    }

                }

                if ($student_email_list) {
                    $this->CI->mailgateway->sentHomeworkStudentMail($student_email_list, $template);

                }

                if ($student_sms_list) {
                    $this->CI->smsgateway->sentHomeworkStudentSMS($student_sms_list, $template);
                }

                 if (!empty($student_notification_list)) {
                    //  echo "<pre>";
                    //  print_r($student_notification_list);
                    //  die;
                    // $this->CI->smsgateway->sentHomeworkStudentNotification($student_notification_list, $template);
                }

            }

        }
    }

}
