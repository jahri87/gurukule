#
# TABLE STRUCTURE FOR: attendence_type
#

DROP TABLE IF EXISTS `attendence_type`;

CREATE TABLE `attendence_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `key_value` varchar(50) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `attendence_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (1, 'Present', '<b class=\"text text-success\">P</b>', 'yes', '2016-06-23 14:11:37', '0000-00-00 00:00:00');
INSERT INTO `attendence_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (2, 'Late With Excuse', '<b class=\"text text-warning\">E</b>', 'no', '2018-05-29 04:19:48', '0000-00-00 00:00:00');
INSERT INTO `attendence_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (3, 'Late', '<b class=\"text text-warning\">L</b>', 'yes', '2016-06-23 14:12:28', '0000-00-00 00:00:00');
INSERT INTO `attendence_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (4, 'Absent', '<b class=\"text text-danger\">A</b>', 'yes', '2016-10-11 07:35:40', '0000-00-00 00:00:00');
INSERT INTO `attendence_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (5, 'Holiday', 'H', 'yes', '2016-10-11 07:35:01', '0000-00-00 00:00:00');
INSERT INTO `attendence_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (6, 'Half Day', '<b class=\"text text-warning\">F</b>', 'yes', '2016-06-23 14:12:28', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: book_issues
#

DROP TABLE IF EXISTS `book_issues`;

CREATE TABLE `book_issues` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `is_returned` int(11) DEFAULT 0,
  `member_id` int(11) DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: books
#

DROP TABLE IF EXISTS `books`;

CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_title` varchar(100) NOT NULL,
  `book_no` varchar(50) NOT NULL,
  `isbn_no` varchar(100) NOT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `rack_no` varchar(100) NOT NULL,
  `publish` varchar(100) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `perunitcost` float(10,2) DEFAULT NULL,
  `postdate` date DEFAULT NULL,
  `description` text DEFAULT NULL,
  `available` varchar(10) DEFAULT 'yes',
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: categories
#

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(100) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: certificates
#

DROP TABLE IF EXISTS `certificates`;

CREATE TABLE `certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certificate_name` varchar(100) NOT NULL,
  `certificate_text` text NOT NULL,
  `left_header` varchar(100) NOT NULL,
  `center_header` varchar(100) NOT NULL,
  `right_header` varchar(100) NOT NULL,
  `left_footer` varchar(100) NOT NULL,
  `right_footer` varchar(100) NOT NULL,
  `center_footer` varchar(100) NOT NULL,
  `background_image` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_for` tinyint(1) NOT NULL COMMENT '1 = staff, 2 = students',
  `status` tinyint(1) NOT NULL,
  `header_height` int(11) NOT NULL,
  `content_height` int(11) NOT NULL,
  `footer_height` int(11) NOT NULL,
  `content_width` int(11) NOT NULL,
  `enable_student_image` tinyint(1) NOT NULL COMMENT '0=no,1=yes',
  `enable_image_height` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `certificates` (`id`, `certificate_name`, `certificate_text`, `left_header`, `center_header`, `right_header`, `left_footer`, `right_footer`, `center_footer`, `background_image`, `created_at`, `updated_at`, `created_for`, `status`, `header_height`, `content_height`, `footer_height`, `content_width`, `enable_student_image`, `enable_image_height`) VALUES (1, 'Sample Transfer Certificate', 'This is certify that <b>[name]</b> has born on [dob]  <br> and have following details [present_address] [guardian] [created_at] [admission_no] [roll_no] [class] [section] [gender] [admission_date] [category] [cast] [father_name] [mother_name] [religion] [email] [phone] .<br>We wish best of luck for future endeavors.', 'Reff. No.......................', '', 'Date: ___/____/_______', '.................................<br>Authority Sign', '.................................<br>Principal Sign', '.................................<br>Class Teacher Sign', 'sampletc12.png', '2018-09-05 15:47:33', '0000-00-00 00:00:00', 2, 1, 360, 400, 480, 810, 1, 230);


#
# TABLE STRUCTURE FOR: class_sections
#

DROP TABLE IF EXISTS `class_sections`;

CREATE TABLE `class_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `section_id` (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: class_teacher
#

DROP TABLE IF EXISTS `class_teacher`;

CREATE TABLE `class_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: classes
#

DROP TABLE IF EXISTS `classes`;

CREATE TABLE `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(60) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: complaint
#

DROP TABLE IF EXISTS `complaint`;

CREATE TABLE `complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `complaint_type` varchar(15) NOT NULL,
  `source` varchar(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `email` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `action_taken` varchar(200) NOT NULL,
  `assigned` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: complaint_type
#

DROP TABLE IF EXISTS `complaint_type`;

CREATE TABLE `complaint_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `complaint_type` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: content_for
#

DROP TABLE IF EXISTS `content_for`;

CREATE TABLE `content_for` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `content_id` (`content_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `content_for_ibfk_1` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_for_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: contents
#

DROP TABLE IF EXISTS `contents`;

CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `is_public` varchar(10) DEFAULT 'No',
  `class_id` int(11) DEFAULT NULL,
  `cls_sec_id` int(10) NOT NULL,
  `file` varchar(250) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `note` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: department
#

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(200) NOT NULL,
  `is_active` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: dispatch_receive
#

DROP TABLE IF EXISTS `dispatch_receive`;

CREATE TABLE `dispatch_receive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(50) NOT NULL,
  `to_title` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `note` varchar(500) NOT NULL,
  `from_title` varchar(200) NOT NULL,
  `date` varchar(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: email_config
#

DROP TABLE IF EXISTS `email_config`;

CREATE TABLE `email_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email_type` varchar(100) DEFAULT NULL,
  `smtp_server` varchar(100) DEFAULT NULL,
  `smtp_port` varchar(100) DEFAULT NULL,
  `smtp_username` varchar(100) DEFAULT NULL,
  `smtp_password` varchar(100) DEFAULT NULL,
  `ssl_tls` varchar(100) DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `email_config` (`id`, `email_type`, `smtp_server`, `smtp_port`, `smtp_username`, `smtp_password`, `ssl_tls`, `is_active`, `created_at`) VALUES (1, 'sendmail', '', '', '', '', '', '', '2017-12-04 17:33:22');


#
# TABLE STRUCTURE FOR: enquiry
#

DROP TABLE IF EXISTS `enquiry`;

CREATE TABLE `enquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `address` mediumtext NOT NULL,
  `reference` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(500) NOT NULL,
  `follow_up_date` date NOT NULL,
  `note` mediumtext NOT NULL,
  `source` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `assigned` varchar(100) NOT NULL,
  `class` int(11) NOT NULL,
  `no_of_child` varchar(11) DEFAULT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: enquiry_type
#

DROP TABLE IF EXISTS `enquiry_type`;

CREATE TABLE `enquiry_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enquiry_type` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: events
#

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_title` varchar(200) NOT NULL,
  `event_description` varchar(300) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `event_type` varchar(100) NOT NULL,
  `event_color` varchar(200) NOT NULL,
  `event_for` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: exam_results
#

DROP TABLE IF EXISTS `exam_results`;

CREATE TABLE `exam_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attendence` varchar(10) NOT NULL,
  `exam_schedule_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `get_marks` float(10,2) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `exam_schedule_id` (`exam_schedule_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: exam_schedules
#

DROP TABLE IF EXISTS `exam_schedules`;

CREATE TABLE `exam_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `teacher_subject_id` int(11) DEFAULT NULL,
  `date_of_exam` date DEFAULT NULL,
  `start_to` varchar(50) DEFAULT NULL,
  `end_from` varchar(50) DEFAULT NULL,
  `room_no` varchar(50) DEFAULT NULL,
  `full_marks` int(11) DEFAULT NULL,
  `passing_marks` int(11) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `teacher_subject_id` (`teacher_subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: exams
#

DROP TABLE IF EXISTS `exams`;

CREATE TABLE `exams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `sesion_id` int(11) NOT NULL,
  `note` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: expense_head
#

DROP TABLE IF EXISTS `expense_head`;

CREATE TABLE `expense_head` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exp_category` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'yes',
  `is_deleted` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: expenses
#

DROP TABLE IF EXISTS `expenses`;

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exp_head_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `invoice_no` varchar(200) NOT NULL,
  `date` date DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `documents` varchar(255) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'yes',
  `is_deleted` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: fee_groups
#

DROP TABLE IF EXISTS `fee_groups`;

CREATE TABLE `fee_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `is_system` int(1) NOT NULL DEFAULT 0,
  `description` text DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: fee_groups_feetype
#

DROP TABLE IF EXISTS `fee_groups_feetype`;

CREATE TABLE `fee_groups_feetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fee_session_group_id` int(11) DEFAULT NULL,
  `fee_groups_id` int(11) DEFAULT NULL,
  `feetype_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fee_session_group_id` (`fee_session_group_id`),
  KEY `fee_groups_id` (`fee_groups_id`),
  KEY `feetype_id` (`feetype_id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `fee_groups_feetype_ibfk_1` FOREIGN KEY (`fee_session_group_id`) REFERENCES `fee_session_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fee_groups_feetype_ibfk_2` FOREIGN KEY (`fee_groups_id`) REFERENCES `fee_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fee_groups_feetype_ibfk_3` FOREIGN KEY (`feetype_id`) REFERENCES `feetype` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fee_groups_feetype_ibfk_4` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: fee_receipt_no
#

DROP TABLE IF EXISTS `fee_receipt_no`;

CREATE TABLE `fee_receipt_no` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: fee_session_groups
#

DROP TABLE IF EXISTS `fee_session_groups`;

CREATE TABLE `fee_session_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fee_groups_id` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `fee_groups_id` (`fee_groups_id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `fee_session_groups_ibfk_1` FOREIGN KEY (`fee_groups_id`) REFERENCES `fee_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fee_session_groups_ibfk_2` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: feecategory
#

DROP TABLE IF EXISTS `feecategory`;

CREATE TABLE `feecategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: feemasters
#

DROP TABLE IF EXISTS `feemasters`;

CREATE TABLE `feemasters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) DEFAULT NULL,
  `feetype_id` int(11) NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: fees_discounts
#

DROP TABLE IF EXISTS `fees_discounts`;

CREATE TABLE `fees_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `fees_discounts_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: feetype
#

DROP TABLE IF EXISTS `feetype`;

CREATE TABLE `feetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_system` int(1) NOT NULL DEFAULT 0,
  `feecategory_id` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: follow_up
#

DROP TABLE IF EXISTS `follow_up`;

CREATE TABLE `follow_up` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enquiry_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `next_date` date NOT NULL,
  `response` text NOT NULL,
  `note` text NOT NULL,
  `followup_by` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: front_cms_media_gallery
#

DROP TABLE IF EXISTS `front_cms_media_gallery`;

CREATE TABLE `front_cms_media_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(300) DEFAULT NULL,
  `thumb_path` varchar(300) DEFAULT NULL,
  `dir_path` varchar(300) DEFAULT NULL,
  `img_name` varchar(300) DEFAULT NULL,
  `thumb_name` varchar(300) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `file_type` varchar(100) NOT NULL,
  `file_size` varchar(100) NOT NULL,
  `vid_url` mediumtext NOT NULL,
  `vid_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (2, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'top-banner2.jpg', 'top-banner2.jpg', '2019-04-02 17:04:18', 'image/jpeg', '248860', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (3, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'top-banner3.jpg', 'top-banner3.jpg', '2019-04-02 17:04:26', 'image/jpeg', '267786', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (5, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'about.jpg', 'about.jpg', '2019-04-02 17:24:06', 'image/jpeg', '93002', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (6, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'courseimg1.jpg', 'courseimg1.jpg', '2019-04-02 17:25:56', 'image/jpeg', '57289', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (7, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'g8.jpg', 'g8.jpg', '2019-04-03 15:07:11', 'image/jpeg', '234203', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (8, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher1.jpg', 'teacher1.jpg', '2019-04-03 15:07:45', 'image/jpeg', '435645', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (9, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher2.jpg', 'teacher2.jpg', '2019-04-03 15:07:45', 'image/jpeg', '33956', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (10, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'scholarship-icon.png', 'scholarship-icon.png', '2019-04-03 17:08:39', 'image/png', '6386', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (11, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'book-icon.png', 'book-icon.png', '2019-04-03 17:30:18', 'image/png', '6070', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (12, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher-icon.png', 'teacher-icon.png', '2019-04-03 17:30:24', 'image/png', '5031', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (22, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'facebook.png', 'facebook.png', '2019-04-04 16:58:05', 'image/png', '1882', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (26, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'bus.jpg', 'bus.jpg', '2019-04-16 16:12:49', 'image/jpeg', '973099', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (27, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'classrooms.jpg', 'classrooms.jpg', '2019-04-16 16:44:25', 'image/jpeg', '4732096', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (28, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'iconic-f14.png', 'iconic-f14.png', '2019-04-23 15:16:03', 'image/png', '3075', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (29, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'iconic-f15.png', 'iconic-f15.png', '2019-04-23 15:16:14', 'image/png', '3362', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (30, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'iconic-f16.png', 'iconic-f16.png', '2019-04-23 15:16:20', 'image/png', '2637', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (31, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'iconic-f17-1.png', 'iconic-f17-1.png', '2019-04-23 16:47:31', 'image/png', '3045', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (35, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'courseimg4.jpg', 'courseimg4.jpg', '2019-04-26 15:18:45', 'image/jpeg', '80475', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (37, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'testim-img1.jpg', 'testim-img1.jpg', '2019-04-26 15:58:05', 'image/jpeg', '56640', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (38, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'testim-img2.jpg', 'testim-img2.jpg', '2019-04-26 15:58:10', 'image/jpeg', '88574', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (39, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'testim-img3.jpg', 'testim-img3.jpg', '2019-04-26 16:02:51', 'image/jpeg', '93427', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (40, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'testim-img4.jpg', 'testim-img4.jpg', '2019-04-26 16:07:39', 'image/jpeg', '82597', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (41, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher1-1556236800.jpg', 'teacher1-1556236800.jpg', '2019-04-26 16:27:56', 'image/jpeg', '55751', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (42, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher2-1556236800.jpg', 'teacher2-1556236800.jpg', '2019-04-26 16:37:22', 'image/jpeg', '69626', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (43, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher3.jpg', 'teacher3.jpg', '2019-04-26 16:41:50', 'image/jpeg', '115762', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (44, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'whatwedo.jpg', 'whatwedo.jpg', '2019-04-26 16:58:49', 'image/jpeg', '51651', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (45, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'laboratories.jpg', 'laboratories.jpg', '2019-04-26 17:06:44', 'image/jpeg', '112505', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (46, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'library.jpg', 'library.jpg', '2019-04-26 17:14:33', 'image/jpeg', '205105', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (47, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'courseimg5.jpg', 'courseimg5.jpg', '2019-04-26 17:31:04', 'image/jpeg', '90066', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (48, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'courseimg6.jpg', 'courseimg6.jpg', '2019-04-26 17:31:04', 'image/jpeg', '108078', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (49, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'principal.jpg', 'principal.jpg', '2019-04-26 17:38:35', 'image/jpeg', '128311', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (50, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher4.jpg', 'teacher4.jpg', '2019-04-27 15:20:03', 'image/jpeg', '64358', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (51, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher5.jpg', 'teacher5.jpg', '2019-04-27 15:20:08', 'image/jpeg', '58069', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (52, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher6.jpg', 'teacher6.jpg', '2019-04-27 15:25:38', 'image/jpeg', '68465', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (53, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'teacher7.jpg', 'teacher7.jpg', '2019-04-27 15:25:38', 'image/jpeg', '59319', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (54, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal4.jpg', 'gal4.jpg', '2019-04-27 16:06:06', 'image/jpeg', '161949', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (55, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal7.jpg', 'gal7.jpg', '2019-04-27 16:06:06', 'image/jpeg', '179431', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (56, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal21.jpg', 'gal21.jpg', '2019-04-27 16:06:06', 'image/jpeg', '176341', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (57, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal25.jpg', 'gal25.jpg', '2019-04-27 16:06:06', 'image/jpeg', '123757', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (58, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal26.jpg', 'gal26.jpg', '2019-04-27 16:06:06', 'image/jpeg', '167483', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (59, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal34.jpg', 'gal34.jpg', '2019-04-27 16:06:06', 'image/jpeg', '202985', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (60, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal5.jpg', 'gal5.jpg', '2019-04-27 17:22:45', 'image/jpeg', '157400', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (69, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal38.jpg', 'gal38.jpg', '2019-04-30 14:36:07', 'image/jpeg', '129403', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (70, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal39.jpg', 'gal39.jpg', '2019-04-30 14:36:53', 'image/jpeg', '139338', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (73, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'gal43.jpg', 'gal43.jpg', '2019-04-30 14:39:04', 'image/jpeg', '146945', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (75, NULL, 'uploads/gallery/youtube_video/thumb/', 'uploads/gallery/youtube_video/', '5d9725281d1c1.jpg', '5d9725281d1c1.jpg', '2019-10-04 10:55:36', 'video', '0', 'https://www.youtube.com/watch?v=5u1AfygxyvI', 'Black / Red / Aqua / Abstract Landscape Painting Demo / For beginners / Daily Art Therapy /Day #0179');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (82, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'v-4banner3-3-1576368000.jpg', 'v-4banner3-3-1576368000.jpg', '2019-12-15 01:51:20', 'image/jpeg', '227581', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (83, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'top-banner2-2-1576368000.jpg', 'top-banner2-2-1576368000.jpg', '2019-12-15 01:51:26', 'image/jpeg', '330384', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (84, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'v-4banner4-1576368000.jpg', 'v-4banner4-1576368000.jpg', '2019-12-15 01:51:32', 'image/jpeg', '190592', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (85, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'v-4banner1-1576368000.jpg', 'v-4banner1-1576368000.jpg', '2019-12-15 01:51:38', 'image/jpeg', '253646', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (86, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG-20190730-WA0001_2.jpg', 'IMG-20190730-WA0001_2.jpg', '2019-12-15 13:19:58', 'image/jpeg', '36907', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (87, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG-20190905-WA0020.jpg', 'IMG-20190905-WA0020.jpg', '2019-12-15 13:20:07', 'image/jpeg', '111030', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (88, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG-20190510-WA0014.jpg', 'IMG-20190510-WA0014.jpg', '2019-12-15 13:20:12', 'image/jpeg', '134004', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (89, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG-20191003-WA0005.jpg', 'IMG-20191003-WA0005.jpg', '2019-12-15 13:20:19', 'image/jpeg', '142752', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (90, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG-20190816-WA0005.jpg', 'IMG-20190816-WA0005.jpg', '2019-12-15 13:34:05', 'image/jpeg', '159340', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (91, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG-20191025-WA0061.jpg', 'IMG-20191025-WA0061.jpg', '2019-12-15 13:34:15', 'image/jpeg', '198183', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (92, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG-20191121-WA0062.jpg', 'IMG-20191121-WA0062.jpg', '2019-12-15 14:02:02', 'image/jpeg', '123673', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (93, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG_20191023_081821_2.jpg', 'IMG_20191023_081821_2.jpg', '2019-12-15 14:02:27', 'image/jpeg', '1448260', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (94, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG_20191107_083131_2.jpg', 'IMG_20191107_083131_2.jpg', '2019-12-15 14:02:35', 'image/jpeg', '1206207', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (95, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG_20191024_105048.jpg', 'IMG_20191024_105048.jpg', '2019-12-15 14:04:04', 'image/jpeg', '258544', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (96, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG_20191024_134035.jpg', 'IMG_20191024_134035.jpg', '2019-12-15 14:04:54', 'image/jpeg', '274204', '', '');
INSERT INTO `front_cms_media_gallery` (`id`, `image`, `thumb_path`, `dir_path`, `img_name`, `thumb_name`, `created_at`, `file_type`, `file_size`, `vid_url`, `vid_title`) VALUES (97, NULL, 'uploads/gallery/media/thumb/', 'uploads/gallery/media/', 'IMG_20191126_100120_2.jpg', 'IMG_20191126_100120_2.jpg', '2019-12-15 14:06:00', 'image/jpeg', '294534', '', '');


#
# TABLE STRUCTURE FOR: front_cms_menu_items
#

DROP TABLE IF EXISTS `front_cms_menu_items`;

CREATE TABLE `front_cms_menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `menu` varchar(100) DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ext_url` mediumtext DEFAULT NULL,
  `open_new_tab` int(11) DEFAULT 0,
  `ext_url_link` mediumtext DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `publish` int(11) NOT NULL DEFAULT 0,
  `description` mediumtext DEFAULT NULL,
  `is_active` varchar(10) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (2, 1, 'Home', 1, 0, NULL, NULL, NULL, 'home', 1, 0, NULL, 'no', '2019-04-25 17:26:35');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (3, 1, 'Contact', 76, 0, NULL, NULL, NULL, 'contact', 21, 0, NULL, 'no', '2019-12-06 17:13:20');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (4, 2, 'Home', 1, 0, NULL, NULL, NULL, 'home-1', 1, 0, NULL, 'no', '2019-04-09 16:12:31');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (5, 2, 'Contact Us', 76, 0, NULL, NULL, NULL, 'contact-us', 7, 0, NULL, 'no', '2019-04-10 16:28:03');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (6, 1, 'About Us', 78, 0, NULL, NULL, NULL, 'about-us', 2, 0, NULL, 'no', '2019-10-29 16:12:31');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (7, 2, 'About Us', 78, 0, NULL, NULL, NULL, 'about-us-1', 2, 0, NULL, 'no', '2019-04-09 16:12:31');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (8, 1, 'Course', 79, 11, NULL, NULL, NULL, 'course', 6, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (9, 2, 'Course', 79, 0, NULL, NULL, NULL, 'course-1', 3, 0, NULL, 'no', '2019-04-09 16:58:19');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (10, 1, 'School Uniform', 81, 11, NULL, NULL, NULL, 'school-uniform', 7, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (11, 1, 'INFORMATION', 0, 0, NULL, NULL, NULL, 'information', 3, 0, NULL, 'no', '2019-12-15 13:41:38');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (12, 1, 'Gallery', 82, 0, NULL, NULL, NULL, 'gallery', 18, 0, NULL, 'no', '2019-12-06 17:13:20');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (13, 1, 'News', 83, 0, NULL, NULL, NULL, 'news', 20, 0, NULL, 'no', '2019-12-06 17:13:20');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (14, 2, 'News', 83, 0, NULL, NULL, NULL, 'news-1', 6, 0, NULL, 'no', '2019-04-10 16:28:03');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (15, 1, 'Events', 84, 0, NULL, NULL, NULL, 'events', 19, 0, NULL, 'no', '2019-12-06 17:13:20');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (16, 2, 'Events', 84, 0, NULL, NULL, NULL, 'events-1', 5, 0, NULL, 'no', '2019-04-10 16:28:03');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (17, 2, 'Gallery', 82, 0, NULL, NULL, NULL, 'gallery-1', 4, 0, NULL, 'no', '2019-04-10 16:28:03');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (18, 1, 'Teacher', 85, 11, NULL, NULL, NULL, 'teacher', 13, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (19, 1, 'Facilities', 88, 11, NULL, NULL, NULL, 'facilities', 5, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (20, 1, 'Principal Message', 89, 11, NULL, NULL, NULL, 'principal-message', 8, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (21, 1, 'School Management', 90, 11, NULL, NULL, NULL, 'school-management', 9, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (22, 1, 'Know Us', 91, 11, NULL, NULL, NULL, 'know-us', 10, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (23, 1, 'Approach', 92, 11, NULL, NULL, NULL, 'approach', 11, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (24, 1, 'Pre Primary', 93, 11, NULL, NULL, NULL, 'pre-primary', 12, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (26, 1, 'Student Council', 96, 11, NULL, NULL, NULL, 'student-council', 15, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (27, 1, 'Houses & Mentoring', 97, 11, NULL, NULL, NULL, 'houses-mentoring', 14, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (28, 1, 'Career Counselling', 98, 11, NULL, NULL, NULL, 'career-counselling', 16, 0, NULL, 'no', '2019-12-15 14:10:32');
INSERT INTO `front_cms_menu_items` (`id`, `menu_id`, `menu`, `page_id`, `parent_id`, `ext_url`, `open_new_tab`, `ext_url_link`, `slug`, `weight`, `publish`, `description`, `is_active`, `created_at`) VALUES (31, 1, 'Academic Information', 104, 11, NULL, NULL, NULL, 'academic-information', 4, 0, NULL, 'no', '2019-12-15 14:10:32');


#
# TABLE STRUCTURE FOR: front_cms_menus
#

DROP TABLE IF EXISTS `front_cms_menus`;

CREATE TABLE `front_cms_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(100) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `open_new_tab` int(10) NOT NULL DEFAULT 0,
  `ext_url` mediumtext NOT NULL,
  `ext_url_link` mediumtext NOT NULL,
  `publish` int(11) NOT NULL DEFAULT 0,
  `content_type` varchar(10) NOT NULL DEFAULT 'manual',
  `is_active` varchar(10) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO `front_cms_menus` (`id`, `menu`, `slug`, `description`, `open_new_tab`, `ext_url`, `ext_url_link`, `publish`, `content_type`, `is_active`, `created_at`) VALUES (1, 'Main Menu', 'main-menu', 'Main menu', 0, '', '', 0, 'default', 'no', '2018-04-20 14:54:49');
INSERT INTO `front_cms_menus` (`id`, `menu`, `slug`, `description`, `open_new_tab`, `ext_url`, `ext_url_link`, `publish`, `content_type`, `is_active`, `created_at`) VALUES (2, 'Bottom Menu', 'bottom-menu', 'Bottom Menu', 0, '', '', 0, 'default', 'no', '2018-04-20 14:54:55');


#
# TABLE STRUCTURE FOR: front_cms_page_contents
#

DROP TABLE IF EXISTS `front_cms_page_contents`;

CREATE TABLE `front_cms_page_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `content_type` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `front_cms_page_contents_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `front_cms_pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `front_cms_page_contents` (`id`, `page_id`, `content_type`, `created_at`) VALUES (7, 82, 'gallery', '2019-12-15 02:57:19');
INSERT INTO `front_cms_page_contents` (`id`, `page_id`, `content_type`, `created_at`) VALUES (8, 83, 'notice', '2019-12-15 02:57:30');
INSERT INTO `front_cms_page_contents` (`id`, `page_id`, `content_type`, `created_at`) VALUES (9, 84, 'events', '2019-12-15 02:58:10');


#
# TABLE STRUCTURE FOR: front_cms_pages
#

DROP TABLE IF EXISTS `front_cms_pages`;

CREATE TABLE `front_cms_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_type` varchar(10) NOT NULL DEFAULT 'manual',
  `is_homepage` int(1) DEFAULT 0,
  `title` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `meta_title` mediumtext DEFAULT NULL,
  `meta_description` mediumtext DEFAULT NULL,
  `meta_keyword` mediumtext DEFAULT NULL,
  `feature_image` varchar(200) NOT NULL,
  `description` longtext DEFAULT NULL,
  `publish_date` date NOT NULL,
  `publish` int(10) DEFAULT 0,
  `sidebar` int(10) DEFAULT 0,
  `is_active` varchar(10) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (1, 'default', 1, 'Home', 'page/home', 'page', 'home', '', '', '', '', '<section class=\"services\">\r\n<div class=\"service-inner\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-4 col-sm-4 service-box\">\r\n<div class=\"service-box-content\">\r\n<h3><a href=\"#\">Laboratories</a></h3>\r\n\r\n<p>The school has laboratories for Physics, Chemistry,&nbsp;Biology,&nbsp;Montessori, IT.</p>\r\n\r\n<div class=\"service-box-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/scholarship-icon.png\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4 col-sm-4 service-box\">\r\n<div class=\"service-box-content\">\r\n<h3><a href=\"#\">Books & Library</a></h3>\r\n\r\n<p>The school has Library having enough Books available for Students.</p>\r\n\r\n<div class=\"service-box-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/book-icon.png\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-4 col-sm-4 service-box\">\r\n<div class=\"service-box-content\">\r\n<h3><a href=\"#\">IT Enabled Classrooms</a></h3>\r\n\r\n<p>The school has provided smart classes to incorporate innovative teaching methods.</p>\r\n\r\n<div class=\"service-box-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher-icon.png\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<div class=\"container spacet40\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-sm-12 col-md-offset-2 text-center\">\r\n<h2 class=\"head-title\">SARASWATI INTERNATIONAL SCHOOL</h2>\r\n\r\n<p class=\"pb40\"><span style=\"box-sizing: border-box; font-size: 16px;\">“Education is about awakening – awakening to the power and beauty that lies within all of us.”<span style=\"box-sizing: border-box;\">&nbsp;</span></span></p>\r\n<img class=\"img-responsive center-block\" src=\"https://demo.smart-school.in/uploads/gallery/media/about_bg.jpg\" /></div>\r\n</div>\r\n</div>\r\n\r\n<section class=\"bg-gray fullwidth spaceb40 spacet40\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-sm-12 col-md-offset-2 text-center\">\r\n<h2 class=\"head-title\">ABOUT US</h2>\r\n\r\n<p><span style=\"box-sizing: border-box; font-weight: bolder; color: rgb(136, 136, 136); font-family: Raleway, sans-serif; font-size: 14px;\">“The Lord has given you a vineyard: you will be fortunate if you make it fruitful. Rear those little plants for heaven”</span></p>\r\n\r\n<div class=\"divider\">&nbsp;</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<div class=\"about_img\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/about.jpg\" /></div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<div class=\"about-right\">\r\n<h3>WELCOME TO SIS</h3>\r\n\r\n<p class=\"pt10 pb10\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>\r\n</div>\r\n\r\n<div class=\"panel-group accrodion2\" id=\"accordion\">\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\">\r\n<h4 class=\"panel-title\"><a class=\"accordion-toggle\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapseOne\">Collapsible Group Item #1</a></h4>\r\n</div>\r\n\r\n<div class=\"panel-collapse collapse in\" id=\"collapseOne\">\r\n<div class=\"panel-body\">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\">\r\n<h4 class=\"panel-title\"><a class=\"accordion-toggle\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapseTwo\">Collapsible Group Item #2</a></h4>\r\n</div>\r\n\r\n<div class=\"panel-collapse collapse\" id=\"collapseTwo\">\r\n<div class=\"panel-body\">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"panel panel-default\">\r\n<div class=\"panel-heading\">\r\n<h4 class=\"panel-title\"><a class=\"accordion-toggle\" data-parent=\"#accordion\" data-toggle=\"collapse\" href=\"#collapseThree\">Collapsible Group Item #3</a></h4>\r\n</div>\r\n\r\n<div class=\"panel-collapse collapse\" id=\"collapseThree\">\r\n<div class=\"panel-body\">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class=\"spaceb40 spacet40\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-sm-12 col-md-offset-2 text-center pb30\">\r\n<h2 class=\"head-title\">OUR MAIN COURSES</h2>\r\n\r\n<p>Fusce sem dolor, interdum in fficitur at</p>\r\n\r\n<div class=\"divider\">&nbsp;</div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n<div class=\"owl-carousel courses-carousel\">\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg1.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg2.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg3.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg4.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class=\"countdown_bg fullwidth counter\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 col-lg-6 col-sm-6\">\r\n<div class=\"counter-content\">\r\n<h2 class=\"counter-title mt0\">ACHIEVEMENTS</h2>\r\n\r\n<div class=\"counter-text\">\r\n<p>SIS was started by regional education and welfare society in 2002. The school is affiliated to C.B.S.E, Affiliation No : 530935 and follows all CBSE rules and regulations. The school is open to all students. If also follows the directive of government of taking in 25% of student from the economical weaker section.</p>\r\n</div>\r\n\r\n<div class=\"counter-img\">\r\n<div class=\"about_img\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/achivement.jpg\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-lg-6 col-sm-6\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 col-lg-6 col-sm-6\">\r\n<div class=\"counter-main\"><img class=\"svg\" src=\"https://demo.smart-school.in/uploads/gallery/media/cap.svg\" />\r\n<h3 class=\"counter-value\" data-count=\"1000\">2000</h3>\r\nStudents</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-lg-6 col-sm-6\">\r\n<div class=\"counter-main\"><img class=\"svg\" src=\"https://demo.smart-school.in/uploads/gallery/media/award.svg\" />\r\n<h3 class=\"counter-value\" data-count=\"500\">70</h3>\r\nCertified Teachers</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-lg-6 col-sm-6\">\r\n<div class=\"counter-main\"><img class=\"svg\" src=\"https://demo.smart-school.in/uploads/gallery/media/building-o.svg\" />\r\n<h3 class=\"counter-value\" data-count=\"100\">1</h3>\r\nStudent Campus</div>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-lg-6 col-sm-6\">\r\n<div class=\"counter-main\"><img class=\"svg\" src=\"https://demo.smart-school.in/uploads/gallery/media/people.svg\" />\r\n<h3 class=\"counter-value\" data-count=\"6285\">1600</h3>\r\nStudents</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class=\"spaceb40 spacet40\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-sm-12 col-md-offset-2 text-center\">\r\n<h2 class=\"head-title\">OUR EXPERIENCED STAFFS</h2>\r\n\r\n<p>Considering desire as primary motivation for the generation of narratives is a useful concept.</p>\r\n\r\n<div class=\"divider\">&nbsp;</div>\r\n</div>\r\n\r\n<div class=\"teamstaff\">\r\n<div class=\"row\">\r\n<div class=\"owl-carousel staff-carousel\">\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img alt=\"\" src=\"https://demo.smart-school.in/uploads/gallery/media/teacher2.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Stella Roffin</h3>\r\n<span class=\"post\">Drawing Teacher</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher1-1556236800.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Princy Flora</h3>\r\n<span class=\"post\">English Tutor</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher2-1556236800.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3 class=\"title\">Jesica Matt</h3>\r\n<span class=\"post\">Art Teacher</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher3.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3 class=\"title\">Janaton Doe</h3>\r\n<span class=\"post\">Math Teacher</span></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<section class=\"spaceb40 spacet40 testimonial_bg fullwidth text-white\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-sm-12 col-md-offset-2 text-center\">\r\n<h2 class=\"text-white\">WHAT PEOPLE SAYS</h2>\r\n\r\n<p>Fusce sem dolor, interdum in efficitur at, faucibus nec lorem. Sed nec molestie justo.</p>\r\n\r\n<div class=\"divider\">&nbsp;</div>\r\n</div>\r\n\r\n<div class=\"owl-carousel testimonial-carousel\">\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"testimonial\">\r\n<div class=\"testimonialimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/testim-img2.jpg\" /></div>\r\n\r\n<div class=\"testi_description\">\r\n<h4>Sidney W. Yarber</h4>\r\n\r\n<h5>Manager</h5>\r\n\r\n<p>Etiam non elit nec augue tempor gravida et sed velit. Aliquam tempus eget lorem ut malesuada. Phasellus dictum est sed libero posuere dignissim.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"testimonial\">\r\n<div class=\"testimonialimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/testim-img1.jpg\" /></div>\r\n\r\n<div class=\"testi_description\">\r\n<h4>Kayla H. Seaman</h4>\r\n\r\n<h5>Co & Founder</h5>\r\n\r\n<p>Etiam non elit nec augue tempor gravida et sed velit. Aliquam tempus eget lorem ut malesuada. Phasellus dictum est sed libero posuere dignissim.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"testimonial\">\r\n<div class=\"testimonialimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/testim-img4.jpg\" /></div>\r\n\r\n<div class=\"testi_description\">\r\n<h4>Terence M. Witzel</h4>\r\n\r\n<h5>Businessman</h5>\r\n\r\n<p>Etiam non elit nec augue tempor gravida et sed velit. Aliquam tempus eget lorem ut malesuada. Phasellus dictum est sed libero posuere dignissim.</p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"testimonial\">\r\n<div class=\"testimonialimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/testim-img3.jpg\" /></div>\r\n\r\n<div class=\"testi_description\">\r\n<h4>Williamson</h4>\r\n\r\n<h5>Manager</h5>\r\n\r\n<p>Etiam non elit nec augue tempor gravida et sed velit. Aliquam tempus eget lorem ut malesuada. Phasellus dictum est sed libero posuere dignissim.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>', '0000-00-00', 1, NULL, 'no', '2019-12-15 12:54:09');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (2, 'default', 0, 'Complain', 'page/complain', 'page', 'complain', 'Complain form', '                                                                                                                                                                                    complain form                                                                                                                                                                                                                                ', 'complain form', '', '<p>\r\n[form-builder:complain]</p>', '0000-00-00', 1, 1, 'no', '2018-05-09 11:14:34');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (54, 'default', 0, '404 page', 'page/404-page', 'page', '404-page', '', '                                ', '', '', '<html>\r\n<head>\r\n <title></title>\r\n</head>\r\n<body>\r\n<p>404 page found</p>\r\n</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2018-05-18 10:46:04');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (76, 'default', 0, 'Contact us', 'page/contact-us', 'page', 'contact-us', '', '', '', '', '<section class=\"contact\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<h2 class=\"col-md-12 col-sm-12\">Send In Your Query</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"col-md-12 col-sm-12\">[form-builder:contact_us]<!--./row--></div>\r\n<!--./col-md-12--></div>\r\n<!--./row--></div>\r\n<!--./container--></section>\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"contact-item\"><img src=\"http://192.168.1.81/repos/smartschool/uploads/gallery/media/pin.svg\" />\r\n<h3>Our Location</h3>\r\n\r\n<p>350 Fifth Avenue, 34th floor New York NY 10118-3299 USA</p>\r\n</div>\r\n<!--./contact-item--></div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"contact-item\"><img src=\"http://192.168.1.81/repos/smartschool/uploads/gallery/media/phone.svg\" />\r\n<h3>CALL US</h3>\r\n\r\n<p>E-mail : info@abcschool.com</p>\r\n\r\n<p>Mobile : +91-9009987654</p>\r\n</div>\r\n<!--./contact-item--></div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"contact-item\"><img src=\"http://192.168.1.81/repos/smartschool/uploads/gallery/media/clock.svg\" />\r\n<h3>Working Hours</h3>\r\n\r\n<p>Mon-Fri : 9 am to 5 pm</p>\r\n\r\n<p>Sat : 9 am to 3 pm</p>\r\n</div>\r\n<!--./contact-item--></div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-md-12 col-sm-12\">\r\n<div class=\"mapWrapper fullwidth\"><iframe frameborder=\"0\" height=\"500\" marginheight=\"0\" marginwidth=\"0\" scrolling=\"no\" src=\"http://maps.google.com/maps?f=q&source=s_q&hl=EN&q=time+square&aq=&sll=40.716558,-73.931122&sspn=0.40438,1.056747&ie=UTF8&rq=1&ev=p&split=1&radius=33.22&hq=time+square&hnear=&ll=37.061753,-95.677185&spn=0.438347,0.769043&z=9&output=embed\" width=\"100%\"></iframe></div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-12-15 05:17:16');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (77, 'manual', 0, 'Complain', 'page/complain-1', 'page', 'complain-1', '', '', '', '', '<div class=\"col-md-12 col-sm-12\">\r\n<h2 class=\"text-center\">Complain</h2>\r\n\r\n<p class=\"text-center\">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy<br />\r\nnibh euismod tincidunt ut laoreet dolore magna.</p>\r\n\r\n<p class=\"text-center\">&nbsp;</p>\r\n\r\n<p class=\"text-center\">[form-builder:complain]</p>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-04-03 16:50:42');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (78, 'manual', 0, 'About Us', 'page/about-us', 'page', 'about-us', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 col-sm-6\">\r\n<h2>Who We Are</h2>\r\n\r\n<p>Saraswati International School is a public service oriented non-profit organization . The mission of the school is to provide the right inputs to help the children grow up into caring, sharing, individual equipment to make the right choices in life and grow up to responsible citizen of the country.</p>\r\n\r\n<p>The main mission of the school is to lay a story foundation for children, making them like long learners and keeping them abreast the place of rapid changes.</p>\r\n\r\n<p>The school is a public service, non profit organization from the time of its inception, the school has excelled in academics as well as co-curricular activities. At present there are over 750 students from varied background studying in the school. Mrs. Nancy Sharma is the principal of the school.\r\nAll school affairs are managed by regional education and welfare society registered at Chandigarh. C.B.S.E Recognised Saraswati High School will be identified by Saraswati International School (Affi No : 530935).</p>\r\n</div>\r\n<!--./col-md-6-->\r\n\r\n<div class=\"col-md-6 col-sm-6\"><img class=\"img-rounded img-responsive\" src=\"https://demo.smart-school.in/uploads/gallery/media/whatwedo.jpg\" /></div>\r\n<!--./col-md-6--></div>\r\n<!--./row--></div>\r\n<!--./container-->\r\n\r\n<section class=\"bggray fullwidth\">\r\n<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-lg-offset-2 col-sm-12 text-center\">\r\n<h2>Why Choose Our Institution?</h2>\r\n\r\n<p>Tmply dummy text of the printing and typesetting industry. Lorem Ipsum has been theindustry\'s standard dummy text ever since the 1500s, when an unknown printer took.</p>\r\n</div>\r\n<!--./col-md-12-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/cap.svg\" /></div>\r\n\r\n<h3><a href=\"#\">Scholarship Facility</a></h3>\r\n\r\n<p>Dorem Ipsum has been the industry\'s standard dummy text ever since the en an unknown printer galley dear.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/people.svg\" /></div>\r\n\r\n<h3><a href=\"#\">Skilled Lecturers</a></h3>\r\n\r\n<p>Dorem Ipsum has been the industry\'s standard dummy text ever since the en an unknown printer galley dear.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/building-o.svg\" /></div>\r\n\r\n<h3><a href=\"#\">Book Library & Store</a></h3>\r\n\r\n<p>Dorem Ipsum has been the industry\'s standard dummy text ever since the en an unknown printer galley dear.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4--></div>\r\n<!--./row--></div>\r\n<!--./container--></section>\r\n\r\n<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<h2 class=\"head-title\">Our History 2018</h2>\r\n\r\n<p>Tmply dummy text of the printing and typesetting indust Lorem Ipsum has been theitry\'s snce simply dummy text of the printing.Phasellus enim libero, blandit vel sapien vita their.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<h2 class=\"head-title\">Over 100 Facalties</h2>\r\n\r\n<p>Tmply dummy text of the printing and typesetting indust Lorem Ipsum has been theitry\'s snce simply dummy text of the printing.Phasellus enim libero, blandit vel sapien vita their.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<h2 class=\"head-title\">We Have 15,000 Students</h2>\r\n\r\n<p>Tmply dummy text of the printing and typesetting indust Lorem Ipsum has been theitry\'s snce simply dummy text of the printing.Phasellus enim libero, blandit vel sapien vita their.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>\r\n</div>\r\n<!--./col-md-4--></div>\r\n<!--./row--></div>\r\n<!--./container-->', '0000-00-00', 0, NULL, 'no', '2019-12-15 13:37:43');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (79, 'manual', 0, 'Course', 'page/course', 'page', 'course', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg1.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n<!--./courses-box--></div>\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg2.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n<!--./courses-box--></div>\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg3.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n<!--./courses-box--></div>\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg4.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n<!--./courses-box--></div>\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg6.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n<!--./courses-box--></div>\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"courses-box\">\r\n<div class=\"courses-box-img\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/courseimg5.jpg\" /></div>\r\n\r\n<div class=\"course-inner\"><a class=\"course-subject\" href=\"#\">Science</a>\r\n\r\n<h4>Electrical Engineering</h4>\r\n\r\n<p>All over the world, human beings create an immense and ever-increasing volume of data, with new kinds of data regularly...</p>\r\n<a class=\"btn-read\" href=\"#\">apply now</a></div>\r\n</div>\r\n<!--./courses-box--></div>\r\n<!--./row--></div>\r\n<!--./container--></div>', '0000-00-00', 0, NULL, 'no', '2019-05-11 12:45:55');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (81, 'manual', 0, 'School Uniform', 'page/school-uniform', 'page', 'school-uniform', '', '', '', '', '<h2>School Uniform</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>SCHOOL UNIFORM</h4>\r\n\r\n<p>The school uniform is to be worn as specified here below during the week; and each item must be labelled with the students Name & Class. All students must wear the School uniform daily. A pupil who is not dressed neatly and tidily in the full school uniform may not be permitted to sit in the class. The newly admitted students must start wearing the school uniform within two weeks of their admission.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>GENERAL UNIFORM (MON, TUE, THU & FRI)</h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>BOYS â€“ CLASS KG-I, KG-II & I - V</h4>\r\n\r\n<p>Bottle green short pants, white & green striped shirt, white socks with two green stripes and black lace-up Velcro shoes.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>BOYS CLASS VI- XII</h4>\r\n\r\n<p>Bottle green full pants, white and green striped shirt ,tie and school belt, white socks with two green stripes black lace-up shoes.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>GIRLS CLASS KG-I, KG-II & I - VII</h4>\r\n\r\n<p>Bottle green frock attached with short sleeved shirt with green stripes, box pleats till knee , green bloomer, green striped white stockings, green hair band and school Tie & Belt.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>GIRLS CLASS VIIIâ€“XII</h4>\r\n\r\n<p>Bottle green salwar, white kurta with green stripes & checkered jacket (pattern given by the school ) white socks and buckled black shoes.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>SPORTS UNIFORM [WEDNESDAY & SATURDAY]</h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>BOYS & GIRLS CLASS KG-I & KG-II :</h4>\r\n\r\n<p>Green track suit with school logo, beige T-shirt with green collar and shoulder stripes, black sports shoes and white socks with two green stripes.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>BOYS & GIRLS [CLASS I-XII]</h4>\r\n\r\n<p>Green track suit with school logo, beige T-shirt with house coloured collar and shoulder stripes, black sports shoes and white socks with two green stripes. House colour ribbons for girls.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>WINTER UNIFORM</h4>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<h4>BOYS & GIRLS â€“ CLASSES KG-I, KG-II & I - V</h4>\r\n\r\n<p>Long pants, white stockings with two green stripes & Sweaters.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>BOYS & GIRLS - CLASS VIâ€“XII</h4>\r\n\r\n<p>Along with school uniform, bottle green blazer and socks/Stockings with two green stripes to be worn by both boys and girls during winter.</p>', '0000-00-00', 0, 1, 'no', '2019-04-09 17:20:00');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (82, 'manual', 0, 'Gallery', 'page/gallery', 'page', 'gallery', '', '', '', '', '<h2>Gallery</h2>', '0000-00-00', 0, NULL, 'no', '2019-04-10 15:53:39');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (83, 'manual', 0, 'News', 'page/news', 'page', 'news', '', '', '', '', '<p>NEWS</p>', '0000-00-00', 0, NULL, 'no', '2019-12-15 02:03:35');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (84, 'manual', 0, 'Events', 'page/events', 'page', 'events', '', '', '', '', '<h2>Events</h2>', '0000-00-00', 0, NULL, 'no', '2019-05-01 17:16:35');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (85, 'manual', 0, 'Teacher', 'page/teacher', 'page', 'teacher', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-3 col-sm-3\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher1-1556236800.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Princy Flora</h3>\r\n<span class=\"post\">English Tutor</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-3\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher2-1556236800.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Jesica Matt</h3>\r\n<span class=\"post\">Art Teacher</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-3\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher3.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Janaton Doe</h3>\r\n<span class=\"post\">Math Teacher</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-3\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher4.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Janaton Doe</h3>\r\n<span class=\"post\">English Teacher</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-3\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher5.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Broklyn Doel</h3>\r\n<span class=\"post\">History</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-3\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher7.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Williamson</h3>\r\n<span class=\"post\">web developer</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-3\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/teacher6.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Steve Alia</h3>\r\n<span class=\"post\">Science Teacher</span></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-3 col-sm-3\">\r\n<div class=\"staffteam\">\r\n<div class=\"staffteamimg\"><img alt=\"\" src=\"https://demo.smart-school.in/uploads/gallery/media/teacher2.jpg\" />\r\n<ul class=\"social-links\">\r\n	<li>Facebook</li>\r\n	<li>Twitter</li>\r\n	<li>Linkedin</li>\r\n	<li>Google Plus</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"staff-content\">\r\n<h3>Williamson</h3>\r\n<span class=\"post\">web developer</span></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-05-04 15:48:20');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (86, 'manual', 0, 'Introduction', 'page/introduction', 'page', 'introduction', '', '', '', '', '<h2>Introduction</h2>', '0000-00-00', 0, NULL, 'no', '2019-04-10 17:28:39');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (87, 'manual', 0, 'School History', 'page/school-history', 'page', 'school-history', '', '', '', '', '<h2>School History</h2>', '0000-00-00', 0, NULL, 'no', '2019-04-10 17:29:13');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (88, 'manual', 0, 'Facilities', 'page/facilities', 'page', 'facilities', '', '', '', '', '<div class=\"container spaceb60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12\">\r\n<h2 class=\"counter-title text-center pb20\">Outreach,Exposure & International Exchange Program</h2>\r\n\r\n<p><span background-color:=\"\" font-size:=\"\" helvetica=\"\" style=\"color: rgb(51, 51, 51); font-family: \">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></p>\r\n\r\n<p><span background-color:=\"\" font-size:=\"\" helvetica=\"\" style=\"color: rgb(51, 51, 51); font-family: \">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></p>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<section class=\"fullwidth facility_bg\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 mb20\">\r\n<h2 class=\"counter-title text-center\">Bus Transportation</h2>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n</div>\r\n<!--./col-md-6-->\r\n\r\n<div class=\"col-md-6 col-sm-6\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/bus.jpg\" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 mb20\">\r\n<h2 class=\"counter-title text-center\">AC Smart Classrooms</h2>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/classrooms.jpg\" /></div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n\r\n<ul class=\"list-group\" style=\"margin-left: 15px; margin-bottom: 15px;\">\r\n	<li>Visual learning, animated multimedia lessons</li>\r\n	<li>Quick and immersive learning</li>\r\n	<li>Progressive improvement in core student learning</li>\r\n	<li>High Overall School performance</li>\r\n	<li>Student assessment and evaluation</li>\r\n	<li>Report cards</li>\r\n</ul>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n</div>\r\n<!--./col-md-6--></div>\r\n</div>\r\n\r\n<section class=\"fullwidth facility_bg\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 mb20\">\r\n<h2 class=\"counter-title text-center\">Laboratories</h2>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n</div>\r\n<!--./col-md-6-->\r\n\r\n<div class=\"col-md-6 col-sm-6\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/laboratories.jpg\" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n\r\n<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 mb20\">\r\n<h2 class=\"counter-title text-center\">Library</h2>\r\n</div>\r\n\r\n<div class=\"col-md-6 col-sm-6\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/library.jpg\" /></div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n\r\n<ul class=\"list-group\" style=\"margin-left: 15px; margin-bottom: 15px;\">\r\n	<li>Visual learning, animated multimedia lessons</li>\r\n	<li>Quick and immersive learning</li>\r\n	<li>Progressive improvement in core student learning</li>\r\n	<li>High Overall School performance</li>\r\n	<li>Student assessment and evaluation</li>\r\n	<li>Report cards</li>\r\n</ul>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n</div>\r\n<!--./col-md-6--></div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-05-11 12:17:27');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (89, 'manual', 0, 'Principal Message', 'page/principal-message', 'page', 'principal-message', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 col-sm-6\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/principal.jpg\" /></div>\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<h2 class=\"mt0\">Message From Principal</h2>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-05-04 15:58:13');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (90, 'manual', 0, 'School Management', 'page/school-management', 'page', 'school-management', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12\">\r\n<h4>School Management</h4>\r\n\r\n<p>School Management System is a multipurpose school management software which is used for all administration, management and learning related activities. Use School management information system to manage students, teachers, employees, courses and all the system and process related to running your institute efficiently. Online school management system delivers premier online fee payment, registration and data management tools for schools, administrators, finance personnel and students. It\'s simple, web-based technologies help improve efficiency and reduce costs associated with managing school registration, budgeting, fund accounting, fundraising, after-school care, and more. Online school management solution streamlines school registration with automatic back-office administration, campus recreation and intramurals, facilities, student data, school accounting, web site content management and enables educational institutions to focus on teaching and enriching the lives of students.</p>\r\n\r\n<ol type=\"1\">\r\n	<li>User friendly Interface</li>\r\n	<li>School Detail Management</li>\r\n	<li>Registration /Admission Management</li>\r\n	<li>Fee Category Management</li>\r\n	<li>Student Fee Management (Fee Detail, Fee Receipt etc.)</li>\r\n	<li>Student Administration</li>\r\n	<li>Class/div Management</li>\r\n	<li>ID Cards Printing</li>\r\n	<li>Student Information Management</li>\r\n	<li>Leave & Attendance Management</li>\r\n	<li>Advance Search for Student Information</li>\r\n	<li>Lesson Planning</li>\r\n	<li>Time Table</li>\r\n	<li>Examination & Evaluation Process</li>\r\n	<li>Online Exam</li>\r\n	<li>SMS Alerts to Students</li>\r\n	<li>Transfer Certificate Management</li>\r\n	<li>Reports Management (Daily Collection, Fee Outstanding, Scroll Sheet etc.)</li>\r\n	<li>Transport Management</li>\r\n	<li>Transport Fee Collection Management</li>\r\n	<li>Visitor Management</li>\r\n	<li>Compliant Management</li>\r\n	<li>Employee Registration Management</li>\r\n	<li>Employee Attendance Management</li>\r\n	<li>Holiday Management</li>\r\n	<li>Salary Structure Management</li>\r\n	<li>Employee Salary Management</li>\r\n	<li>Employee Attendance and Salary Reports</li>\r\n	<li>Secure and Reliable</li>\r\n	<li>Many more Feature Available</li>\r\n</ol>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-05-04 15:58:41');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (91, 'manual', 0, 'Know Us', 'page/know-us', 'page', 'know-us', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-6 col-sm-6\">\r\n<h2 class=\"pb20\">Introduction</h2>\r\n\r\n<p>Mount Carmel School , Baghmugalia, Bhopal is a recognized unaided Christian minority institution founded in the year 1999, affiliated to the Council for the Indian School Certificate Examination , New Delhi (School Code Mp023). The School prepares its students for the ICSE and the ISC Examination at X and XII Level respectively.</p>\r\n\r\n<p>The school is managed by the Sisters of the Congregation of the Mother of Carmel . The Congregation is a religious and charitable organization and its members offer their dedicated and selfless service for the intellectual , moral and aesthetic development of the people in the vicinity.</p>\r\n</div>\r\n<!--./col-md-6-->\r\n\r\n<div class=\"col-md-6 col-sm-6\">\r\n<div class=\"visionbg text-center\">\r\n<h2 class=\"pb20\">Our Vision</h2>\r\n\r\n<p>To give to the world \"Icons\" who can win the hearts and minds of others, put service before self, and retain their humility and values even as they soar high.</p>\r\n\r\n<p>Tmply dummy text of the printing and typesetting indust Lorem Ipsum has been theitry\'s snce simply dummy text of the printing.Phasellus enim libero, blandit vel sapien vita their.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-6--></div>\r\n<!--./row--></div>\r\n<!--./container-->\r\n\r\n<section class=\"bggray fullwidth\">\r\n<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-lg-offset-2 col-sm-12 text-center pb20\">\r\n<h2>Why Choose Our Institution?</h2>\r\n\r\n<p>Tmply dummy text of the printing and typesetting industry. Lorem Ipsum has been theindustry\'s standard dummy text ever since the 1500s, when an unknown printer took.</p>\r\n</div>\r\n<!--./col-md-12-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses misssion\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/cap.svg\" /></div>\r\n\r\n<h3>Admissions</h3>\r\n\r\n<p>To develop an understanding & appreciation of the global dimension of our world; making our children appreciative of their history, culture & traditions, whilst being open to other cultures and alternative views of the world.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses misssion\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/people.svg\" /></div>\r\n\r\n<h3>Skilled Lecturers</h3>\r\n\r\n<p>Dorem Ipsum has been the industry\'s standard dummy text ever since the en an unknown printer galley dear.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses misssion\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/building-o.svg\" /></div>\r\n\r\n<h3>Book Library & Store</h3>\r\n\r\n<p>Dorem Ipsum has been the industry\'s standard dummy text ever since the en an unknown printer galley dear.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses misssion\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/iconic-f14.png\" /></div>\r\n\r\n<h3>Admissions</h3>\r\n\r\n<p>To develop an understanding & appreciation of the global dimension of our world; making our children appreciative of their history, culture & traditions, whilst being open to other cultures and alternative views of the world.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses misssion\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/iconic-f15.png\" /></div>\r\n\r\n<h3>Skilled Lecturers</h3>\r\n\r\n<p>Dorem Ipsum has been the industry\'s standard dummy text ever since the en an unknown printer galley dear.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4-->\r\n\r\n<div class=\"col-lg-4 col-md-4 col-sm-4\">\r\n<div class=\"whychooses misssion\">\r\n<div class=\"whychooses-icon\"><img src=\"https://demo.smart-school.in/uploads/gallery/media/iconic-f16.png\" /></div>\r\n\r\n<h3>Book Library & Store</h3>\r\n\r\n<p>Dorem Ipsum has been the industry\'s standard dummy text ever since the en an unknown printer galley dear.</p>\r\n</div>\r\n</div>\r\n</div>\r\n<!--./row--></div>\r\n<!--./container--></section>', '0000-00-00', 0, NULL, 'no', '2019-05-04 15:59:01');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (92, 'manual', 0, 'Approach', 'page/approach', 'page', 'approach', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-sm-8\">\r\n<h2 class=\"pb20\">Singular Focus; Multiple Intelligences</h2>\r\n\r\n<p>GurukulE, New Delhi is a recognized unaided Christian minority institution founded in the year 1999, affiliated to the Council for the Indian School Certificate Examination , New Delhi (School Code Mp023). The School prepares its students for the ICSE and the ISC Examination at X and XII Level respectively.</p>\r\n\r\n<p>The school is managed by the Sisters of the Congregation of the Mother of Gurukul. The Congregation is a religious and charitable organization and its members offer their dedicated and selfless service for the intellectual , moral and aesthetic development of the people in the vicinity.</p>\r\n</div>\r\n<!--./col-md-6-->\r\n\r\n<div class=\"col-md-4 col-sm-4\">\r\n<div class=\"about_img\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/about.jpg\" /></div>\r\n</div>\r\n<!--./col-md-6--></div>\r\n<!--./row--></div>\r\n<!--./container-->\r\n\r\n<section class=\"bggray fullwidth\">\r\n<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 whitebox\">\r\n<div class=\"col-lg-3 col-md-3 col-sm-3\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/classrooms.jpg\" /></div>\r\n\r\n<div class=\"col-lg-9 col-md-9 col-sm-9\">\r\n<h2>Teaching Methodology</h2>\r\n\r\n<p class=\"text-justify\">The school is managed by the Sisters of the Congregation of the Mother of Gurukul&nbsp;. The Congregation is a religious and charitable organization and its members offer their dedicated and selfless service for the intellectual , moral and aesthetic development of the people in the vicinity. The school is managed by the Sisters of the Congregation of the Mother of Carmel . The Congregation is a religious and charitable organization and its members offer their dedicated and selfless service for the intellectual , moral and aesthetic development of the people in the vicinity.</p>\r\n</div>\r\n</div>\r\n<!--./col-md-4--><!--./col-md-4--></div>\r\n<!--./row--></div>\r\n<!--./container--></section>\r\n\r\n<div class=\"container spaceb60 spacet60 text-center\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-md-offset-2\">\r\n<h2>Professional Learning Program</h2>\r\n\r\n<p class=\"text-justify\">The School has partnered with Learning Forward India, whereby Educators undertake structured Training sessions and self-directed professional learning.</p>\r\n\r\n<p class=\"text-justify\">The Professional Learning Program (PLP) aligns with the mission of Learning Forward (www.learningforward.org), the world\'s largest body of individual educators, with a vision of excellent teaching and learning every day. The Iconic School, in collaboration with The Learning Forward India Academy promotes building the capacity of leaders to establish and sustain highly effective professional learning. Professional learning means a comprehensive, sustained and intensive approach to improving teachersâ€™ and principalsâ€™ effectiveness in raising student achievement.</p>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-12-15 02:01:28');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (93, 'manual', 0, 'Pre Primary', 'page/pre-primary', 'page', 'pre-primary', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"blue-box1\">\r\n<div class=\"row\">\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"around40\"><img alt=\"\" height=\"75\" src=\"https://demo.smart-school.in/uploads/gallery/media/iconic-f17-1.png\" width=\"75\" />\r\n<h2>Involve</h2>\r\n\r\n<p class=\"blue-text\">Pre-Primary</p>\r\n\r\n<p>GurukulE, New&nbsp;Delhi is a recognized unaided Christian minority institution founded in the year 1999, affiliated to the Council for the Indian School Certificate Examination , New Delhi (School Code Mp023). The School prepares its students for the ICSE and the ISC Examination at X and XII Level respectively. The school is managed by the Sisters of the Congregation of the Mother of Carmel</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-md-8 col-sm-6\"><img class=\"img-responsive\" src=\"https://demo.smart-school.in/uploads/gallery/media//classrooms.jpg\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-12-15 02:02:46');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (94, 'manual', 0, 'Primary', 'page/primary', 'page', 'primary', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"blue-box1\">\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-sm-6\"><img class=\"img-responsive\" src=\"https://demo.smart-school.in/uploads/gallery/media//classrooms.jpg\" /></div>\r\n\r\n<div class=\"col-md-4 col-sm-6\">\r\n<div class=\"around40\"><img alt=\"\" height=\"75\" src=\"https://demo.smart-school.in/uploads/gallery/media/iconic-f17-1.png\" width=\"75\" />\r\n<h2>Instill</h2>\r\n\r\n<p class=\"blue-text\">Pre-Primary</p>\r\n\r\n<p>Mount Carmel School, Baghmugalia, Bhopal is a recognized unaided Christian minority institution founded in the year 1999, affiliated to the Council for the Indian School Certificate Examination , New Delhi (School Code Mp023). The School prepares its students for the ICSE and the ISC Examination at X and XII Level respectively. The school is managed by the Sisters of the Congregation of the Mother of Carmel</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-05-04 16:03:23');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (95, 'manual', 0, 'Sports', 'page/sports', 'page', 'sports', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-5 col-sm-5\"><img class=\"img-responsive\" src=\"https://demo.smart-school.in/uploads/gallery/media//classrooms.jpg\" /></div>\r\n\r\n<div class=\"col-md-7 col-sm-7\">\r\n<h2>Sports</h2>\r\n\r\n<p>Mount Carmel School , Baghmugalia, Bhopal is a recognized unaided Christian minority institution founded in the year 1999, affiliated to the Council for the Indian School Certificate Examination , New Delhi (School Code Mp023). The School prepares its students for the ICSE and the ISC Examination at X and XII Level respectively. The school is managed by the Sisters of the Congregation of the Mother of Carmel</p>\r\n\r\n<p>Mount Carmel School , Baghmugalia, Bhopal is a recognized unaided Christian minority institution founded in the year 1999, affiliated to the Council for the Indian School Certificate Examination , New Delhi (School Code Mp023). The School prepares its students for the ICSE and the ISC Examination at X and XII Level respectively. The school is managed by the Sisters of the Congregation of the Mother of Carmel</p>\r\n</div>\r\n\r\n<div class=\"col-sm-12 col-md-12 text-justify sports pt20\">\r\n<h2>Sports for Icons - <b>\"Right to play & play for all\"</b></h2>\r\n\r\n<p>A healthy mind resides in a healthy body. Games and sports education are not only significant for triumph in the student life but it is a key to achievement in every stride of life. The Mount School believes in \"Right to play & play for all\". The school sports structure is developed to cater to various needs of the child. If sports are inculcated from a very young age, it helps build character and discipline. It gives students the confidence to trust their own individuality.</p>\r\n\r\n<h2>For Kinder Garten â€“ SPORTS SPROUTS</h2>\r\n\r\n<p>In this, natural curiosity and interaction with the environment stimulate the growth of motor activity which helps students to make a strong foundation for the sports. The body movement and coordination of the child is tapped through various locomotor, non-locomotor and manipulative activities. For the little icons, skill based sports like skating and gymnastic are offered.</p>\r\n\r\n<h2>For Primary - ALL-ROUNDER</h2>\r\n\r\n<p>The School provides opportunities to students to have the benefit of maximum sports with enjoyment. Education on fundamentals of every sports activity is imparted wherein the students get an experience of various sports like football, cricket, skating, swimming, athletics, etc. This varied experience enables students to identify a sport of their choice and excel in the same.</p>\r\n\r\n<h2>For Middleâ€“ SPECIALISATION</h2>\r\n\r\n<p>At this stage, sports programme encourages the students to excel in one sport, so that they are able to chip in and execute well in Intramural and Extramural competitions. Sports offered for the specialization include basketball, cricket and football.</p>\r\n\r\n<p>We, at Mount, believe that as long as the icons are involved in sports activities, all their senses, the locomotor system and intellectual capacities are engaged. By playing sports, child becomes stronger, physically as well as mentally. Above all, they learn to overcome obstacles and challenges in real life situations.</p>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-05-04 16:03:45');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (96, 'manual', 0, 'Student Council', 'page/student-council', 'page', 'student-council', '', '', '', '', '<div class=\"container spacet50 spaceb50\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 text-justify\">\r\n<h2>Student Council</h2>\r\n\r\n<p>GurukulE, New Delhi is a recognized unaided Christian minority institution founded in the year 1999, affiliated to the Council for the Indian School Certificate Examination , New Delhi (School Code Mp023). The School prepares its students for the ICSE and the ISC Examination at X and XII Level respectively.</p>\r\n\r\n<ol class=\"list-1\">\r\n	<li>School&nbsp; diary&nbsp; is to be brought to school daily without fail.</li>\r\n	<li>Students should come to school in complete school uniform, on all school days ( including PTM).</li>\r\n	<li>Punctuality &nbsp;a must and late comers will be suitably checked with corrective measures.</li>\r\n	<li>No mobile phone is to be brought to school for what so ever reason. The set will be confiscated and very strict action will be taken against the user. The set will be returned only at the end of the<br />\r\n	&nbsp;&nbsp;session .</li>\r\n	<li>Students are discouraged to come to school by private vans using gas kits or over loaded.</li>\r\n	<li>85 % attendance is a must for every student.</li>\r\n	<li>A pupil who uses unfair means during examinations will be given zero in the subject and repetition of the same shall lead to dismissal.</li>\r\n	<li>Parents / guardians can meet the principal in her office from 9:00 am to 10:00 am. on all school<br />\r\n	working days.</li>\r\n	<li>Parents / guardians should meet teachers only on PTM or with appointment in special cases.</li>\r\n	<li>It is compulsory for all students to participate in sports, games and co-curricular&nbsp; activities of<br />\r\n	the school .</li>\r\n	<li>In case of accidents, school will provide only first aid; other related expenses will be borne by&nbsp;<br />\r\n	Parents / guardians.</li>\r\n</ol>\r\n\r\n<div class=\"col-md-12\">\r\n<h2>Student Council 2018-19</h2>\r\n\r\n<table class=\"table table-striped table-bordered\">\r\n	<tbody>\r\n		<tr>\r\n			<td>School Captain</td>\r\n			<td>Yashi Agrawal</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sports Captain</td>\r\n			<td>Krishna Soni</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Editor-in-Chief</td>\r\n			<td>Tanisha Agrawal</td>\r\n		</tr>\r\n		<tr>\r\n			<th colspan=\"12\">\r\n			<center>\r\n			<h3>Houses</h3>\r\n			</center>\r\n			</th>\r\n		</tr>\r\n		<tr>\r\n			<td>Jade House Captain</td>\r\n			<td>Naman Sinha</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Jade Vice-House Captain</td>\r\n			<td>Kartik Marale</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Topaz House Captain</td>\r\n			<td>Ansh Budhwani</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Topaz Vice-House Captain</td>\r\n			<td>Aparna Kushwaha</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sapphire House Captain</td>\r\n			<td>Alankriti Dube</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sapphire Vice-House Captain</td>\r\n			<td>Prakhar Verma</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ruby House Captain</td>\r\n			<td>Niyati Sinha</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ruby Vice-House Captain</td>\r\n			<td>Hiren Jain</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-12-15 02:02:29');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (97, 'manual', 0, 'Houses & Mentoring', 'page/houses-mentoring', 'page', 'houses-mentoring', '', '', '', '', '<div class=\"container spacet50 spaceb50\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 col-sm-12 text-justify\">\r\n<h3>School House System and Mentoring System</h3>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Grade 1 to 4 â€“ Mentoring System</h3>\r\n\r\n<p>The school is managed by the Sisters of the Congregation of the Mother of Carmel . The Congregation is a religious and charitable organization and its members offer their dedicated and selfless service for the intellectual , moral and aesthetic development of the people in the vicinity</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Grade 5 onwards â€“ House System</h3>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>\r\n</div>\r\n</div>\r\n</div>', '0000-00-00', 0, NULL, 'no', '2019-05-04 16:04:43');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (98, 'manual', 0, 'Career Counselling', 'page/career-counselling', 'page', 'career-counselling', '', '', '', '', '<div class=\"container spaceb60 spacet60\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12 whitebox\">\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\"><img class=\"img-responsive img-rounded\" src=\"https://demo.smart-school.in/uploads/gallery/media/classrooms.jpg\" /></div>\r\n\r\n<div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n<h2>Career Counselling</h2>\r\n\r\n<p class=\"text-justify\">The school is managed by the Sisters of the Congregation of the Mother of Carmel . The Congregation is a religious and charitable organization and its members offer their dedicated and selfless service for the intellectual , moral and aesthetic development of the people in the vicinity. The school is managed by the Sisters of the Congregation of the Mother of Carmel . The Congregation is a religious and charitable organization and its members offer their dedicated and selfless service for the intellectual , moral and aesthetic development of the people in the vicinity.</p>\r\n\r\n<p>Counselling activities encompass the following :</p>\r\n\r\n<ul class=\"square\">\r\n	<li>Helping students to identify their areas of interest and aptitude.</li>\r\n	<li>Guiding students about application processes in India and abroad.</li>\r\n	<li>Providing updated information about various institutions, courses and careers.</li>\r\n	<li>Giving sustained support in the admission process that includes collating and dispatching transcripts, writing out recommendation letters, predicted grades and school profiles and helping with the Personal Statements to be submitted.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<!--./col-md-4--><!--./col-md-4--></div>\r\n<!--./row--></div>', '0000-00-00', 0, NULL, 'no', '2019-05-04 16:05:12');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (99, 'manual', 0, 'new page', 'page/new-page', 'page', 'new-page', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal25.jpg', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>fsfsf</p>\r\n</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-09-30 06:49:58');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (100, 'manual', 0, 'one', 'page/one', 'page', 'one', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>&nbsp;</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-05 10:16:20');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (101, 'manual', 0, 'two', 'page/two', 'page', 'two', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>&nbsp;</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-05 10:16:28');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (102, 'manual', 0, 'three', 'page/three', 'page', 'three', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>&nbsp;</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-05 10:16:35');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (103, 'manual', 0, 'four', 'page/four', 'page', 'four', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>&nbsp;</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-05 10:16:42');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (104, 'manual', 0, 'Academic Information', 'page/academic-information', 'page', 'academic-information', '', '', '', '', '<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">Academic Session Related Information</h1>\r\n<title></title>\r\n<h3 background-color:=\"\" box-sizing:=\"\" fauna=\"\" style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \" text-shadow:=\"\">Assessment Pattern and Schedule</h3>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">1.</strong>&nbsp;The academic session comprises 2 terms having 2 rounds of assessments in each term for classes Mont-1 to VIII<span style=\"padding: 0px; margin: 0px; font-size: 10.5px; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; box-sizing: border-box;\">th</span>.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">2.</strong>&nbsp;Each round consists of one assessment in each of the subject English, Hindi, Maths, EVS/science and social studies.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">3.</strong>&nbsp;No retest shall be conducted for any assessment missed by the child and average is given for the assessment missed by the child</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">4.</strong>&nbsp;At the end of each term, the child is given an overall subject-wise grade in the Report Card based on his/her performance.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">5.</strong>&nbsp;The assessments are conducted on every Tuesday from July onwards</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">6.</strong>&nbsp;Report card- On the loss of Report Card a new Report card will be issued on payment of Rs.100 only for the current session. (In case the report card is lost after First time Result, the comments/ grade will be entered in new Report Card).</p>\r\n\r\n<h3 background-color:=\"\" box-sizing:=\"\" fauna=\"\" style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \" text-shadow:=\"\">Comprehensive and continuous Evaluation (CCE) Simplified</h3>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">In keeping with the guidelines from the CBSE, the implementation of the comprehensive and continuous Evaluation aims at holistic assessment of the student. This encourages the use of internal assessment to evaluate the learner’s growth and reduces the emphasis on external examinations. Hence the achievements of students in academic and non academic areas will be consistently recorded and reflected in the report book. As a result, excellence in diverse areas will be recognized and rewarded. The academic work of every child will hereby be assessed through formative and summative assessments, The unit tests being a compulsory component of each formative assessment.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">There will be two terms in all classes; the first term will be from April- September and the second term from October – March of the subsequent year. Each term will have two formative and one summative assessment which will be indicated in grades.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Formative Assessment</strong>&nbsp;would include class work/ homework, oral exercise, quizzes, projects ( group/ individual), assignment/tests, experiments and conversation/interviews.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">The CCE envisages learning in areas other than academic and ensures that every child acquires life skills, correct attitude and values, participates actively in co-curricular activities and conscious and concerned about his/her health and physical fitness. The assessment of co-scholastic areas will also find a place in the report book, thus providing a holistic profile of the development over a span of learning time of every child. We seek your co-operation and support in order to make this programme a success and an invaluable tool towards making SIS self sufficient and self reliant and successful individuals.</p>', '0000-00-00', 0, NULL, 'no', '2019-12-15 14:02:26');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (105, 'manual', 0, 'Bus Facility', 'page/bus-facility', 'page', 'bus-facility', '', '', '', '', '<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">Bus Facility</h1>\r\n\r\n<p>&nbsp;</p>\r\n<title></title>\r\n<p>&nbsp;\r\n<p background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">The school has its own fleet of buses . students can avail the school transport subject to availability of seats . The routes of the school buses are planned by the school. Parents should consult the Transport in charge for necessary details.</p>\r\n</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">For safety of the students, the following rules are to be followed by every student travelling by the school bus.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">1.</strong>&nbsp;All the students using the school bus are expected to be at the bus stop at least 5 minutes before the arrival of the bus. Buses will not wait for late comers.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">2.</strong>&nbsp;Students should board and get off only at the respective stops assigned to them.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">3.</strong>&nbsp;Children should stay away from main road until the bus arrives.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">4.</strong>&nbsp;The drivers are authorized to stop buses only at the designated stops.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">5.</strong>&nbsp;The bus incharges and student bus monitors are responsible for maintaining discipline in the bus enroute.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">6.</strong>&nbsp;Under no circumstances children will be allowed to board another bus without prior permission from the principal.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">7.</strong>&nbsp;No student will enter the driver’s cabin.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">8.</strong>&nbsp;Parents are to make sure that they pick their children at the notified time from the specified bus stop Failing which the child will be brought back to the school from where the parent is to collect the child.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">9.</strong>&nbsp;In case due to any reason, student using the school transport has to be picked up by parent/ guardian, the following rules will be observed.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">&nbsp;&nbsp;&nbsp;&nbsp;9.1</strong>&nbsp;A written request to be sent in the morning for issuing of gate pass.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">&nbsp;&nbsp;&nbsp;&nbsp;9.2</strong>&nbsp;Gate pass will be issued on producing parent identity card at the reception. The gate pass is to be shown at the gate at the time of departure.</p>', '0000-00-00', 0, NULL, 'no', '2019-12-15 13:59:37');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (106, 'manual', 0, 'Sports Facility', 'page/sports-facility', 'page', 'sports-facility', '', '', '', '', '<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">Facility: Sports Facility</h1>\r\n\r\n<p><br />\r\n<title></title>\r\n</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">We recognize the need and importance of games and sports activities for the development of all round personality of a child.&nbsp;<strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">“SPORTS-A WAY OF LIFE”</strong>&nbsp;is our aim. We provide a variety of games and sports facilities based on the need and interest of the child which helps the child to understand that a sound and a fit body is a prerequisite for scholastic achievement. There are many values such as team work, self control, discipline, courage, confidence, perseverance, determination and sense of belongingness that can be instilled among the children through a wide range of games and sports programmes. We have qualified coaches to impart. Coaching to the children.</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">Various sports facilities offered to the students:-</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">1.</strong>&nbsp;Yoga</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">2.</strong>&nbsp;Badminton</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">3.</strong>&nbsp;Boxing</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">4.</strong>&nbsp;Football</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">5.</strong>&nbsp;Chess</p>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">6.</strong>&nbsp;and many others</p>', '0000-00-00', 0, NULL, 'no', '2019-12-15 14:00:26');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (107, 'manual', 0, 'Other Facilities', 'page/other-facilities', 'page', 'other-facilities', '', '', '', '', '<title></title>\r\n<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">Facility: Other School Facilities</h1>\r\n\r\n<h3 background-color:=\"\" box-sizing:=\"\" fauna=\"\" style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \" text-shadow:=\"\">&nbsp;</h3>\r\n\r\n<h3 background-color:=\"\" box-sizing:=\"\" fauna=\"\" style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \" text-shadow:=\"\">IT Enabled classrooms</h3>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">The school has provided smart classes to incorporate innovative teaching methods. These classes facilitate integrated teaching through PowerPoint presentations, and internet.</p>\r\n\r\n<h3 background-color:=\"\" box-sizing:=\"\" fauna=\"\" style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \" text-shadow:=\"\">Laboratories</h3>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">The school has well equipped Physics, Chemistry, Biology, EVS, Math’s, Montessori and IT laboratories. All laboratories follow the basic rules, regulations and instruction of CBSE. The school also has interactive Board Rooms, and Audio Visual Rooms.</p>\r\n\r\n<h3 background-color:=\"\" box-sizing:=\"\" fauna=\"\" style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \" text-shadow:=\"\">House System</h3>\r\n\r\n<p align=\"justify\" background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\">The student community starting from Class 1 onwords, is divided into four Houses. Each House has a House Mistress and teachers to guide the students in their curricular and co-curricular activities. They help and guide the children to make right choices in extra curricular and co-curricular activities. The progress of the child is carefully monitored and recorded at every stage. The house mistress and teachers act as a vital link in the Saraswati system, taking on the role of friends , advisors and confidantes. The house system inculcates in the students qualities of leadership, co-operation, mutual understanding,tolerance and self – reliance. Various activities such as Dramatics, Elocution, Quiz, Group Singing, Dance etc. are organized to build up team spirit. Besides this, the students are initiated into Community Service and Social Work.</p>\r\n\r\n<p background-color:=\"\" font-size:=\"\" style=\"padding: 0px; margin: 0px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \" trebuchet=\"\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Note :</strong>&nbsp;To inculcate in the students, a sense of belongingness of the House, it is mandatory for each student to wear their House Uniform on Wednesdays and Saturdays.</p>', '0000-00-00', 0, NULL, 'no', '2019-12-15 14:01:32');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (108, 'manual', 0, 'Academic Unit Tests', 'page/academic-unit-tests', 'page', 'academic-unit-tests', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">Unit Test: General Instructions, Rules & Regulations and Schedule</h1>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">1. General Instructions:</h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">a.</strong>&nbsp;There will be two terms in the entire year.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">b.</strong>Each term shall comprise a two UT cycle and the term end examination.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">c.</strong>If a student misses any UT, no retest will be conducted.</p>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">2. Instruction related to examination pattern to be followed for X1</h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">a.</strong>There will be two terminal exams</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">b.</strong>There will be two round of UT, in first term and two round of UT in Second term</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">c.</strong>If a student misses any UT no retest will be conducted and it will be marked as zero.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">d.</strong>Second Term examination will take place in the month of December and pre-boards in the month of January.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">e.</strong>No retest shall be conducted for students who miss Unit Tests. However, an average of marks secured in the Unit Test of subject may be awarded in the final Term in cases that warrant consideration.</p>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">3. Rules and Regulations for Unit Tests and Examinations :</h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">a.</strong>Students suffering from fever, cold, flu or any other kind of aliment which can be transmitted to others, should not come to school during Unit Tests/Examinations.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">b.</strong>Students who miss Unit Tests due to medical reasons should submit a medical certificate for the same , to their class teachers, within 3 working days .</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">c.</strong>Students shall not be permitted to sit for the Unit Tests if they reach the class after 7.40 am. In Unit Test Days.</p>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">4. Consequences of using unfair means during Unit tests/ Examinations</h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">a.</strong>The answer sheet shall be taken away immediately by the invigilator.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">b.</strong>A new sheet shall be given to the student.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">c.</strong>Marks shall not be awarded for the questions attempted in the first sheet.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">d.</strong>Disciplinary action will be taken against the student according to school rules.</p>\r\n</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-15 13:53:54');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (109, 'manual', 0, 'Admission Procedure', 'page/admission-procedure', 'page', 'admission-procedure', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">Admission/ Entry Qualification</h1>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">For Completing the admission formalities , parents are requested to fill the Admission Form, and Use of school Bus Form (where applicable) . These Form are to be carefully completed and submitted to the school office along with initial payment of fees during office hours namely from 8.00 a.m to 1.30 p.m on all working days.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">1.</strong>&nbsp;The parents will then procure the uniform and stationery kit from the school shops.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">2.</strong>&nbsp;The child has to report in complete school uniform along with the books and stationery as prescribed by the school.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">3:</strong>&nbsp;Birth certificate</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">3.1) Mont-1 :</strong>&nbsp;As stated in the admission application form , the date of birth of the child is required to be supported by a certificate Photostat copy of the birth certificate issued by the municipal corporation/ Local bodies as applicable.&nbsp;<strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\"><i style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">An affidavit or any other evidence is not acceptable in support of date of birth for admission.</i></strong></p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">3.2) Class-1 onwards :</strong>&nbsp;original Transfer certificate is required from the previous school for students seeking admission at SIS Gurgaon . A child will not be issued an admit card if he transfer certificate is not submitted.</p>\r\n</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-15 13:54:44');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (110, 'manual', 0, 'Bell Timings', 'page/bell-timings', 'page', 'bell-timings', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76); text-align: center;\">School Bell Timings</h1>\r\n\r\n<table align=\"center\" class=\"tableStyle aligncenter\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; display: block; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76); height: 33px;\" width=\"235\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle1\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"100%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large; color: rgb(255, 204, 153);\">Academic Session Period</span></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table class=\"aligncenter \" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; display: block; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76); height: 58px;\" width=\"361\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 April 2014</td>\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">31 March 2015</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle aligncenter\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; display: block; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"169\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle1\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"100%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large; color: rgb(255, 204, 153);\">Admission Period</span></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table class=\"aligncenter \" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; display: block; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76); height: 58px;\" width=\"381\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 April 2014</td>\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">31 August 2014</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle aligncenter\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; display: block; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76); height: 33px;\" width=\"188\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle1\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"100%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large; color: rgb(255, 204, 153);\">School Bell Timings</span></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bell Timings</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">S.No.</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Time</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Period</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">0</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">7:40 &ndash; 8:00</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Assembly</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">8:00 &ndash; 8:40</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1st Period</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">2</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">8:40 &ndash; 9:20</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">2nd Period</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">3</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">9:20- 10:00</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">3rd Period</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">4</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">10:00 &ndash; 10:40</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">4th Period</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">5</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">10:40 &ndash; 11:00</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Lunch</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">6</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">11:00 &ndash; 11:40</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">5th Period</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">7</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">11:40 &ndash; 12:20</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">6th Period</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">8</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">12:20 &ndash; 1:00</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">7th Period</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">9</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1:00- 1:40</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">8th Period</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-15 13:55:47');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (111, 'manual', 0, 'General Information', 'page/general-information', 'page', 'general-information', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">General Information</h1>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">BIRTHDAYS</strong></h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">The parents are requested not to spend lavishly on food, cake , presents etc. On a child&rsquo;s birthday as it disrupts the school discipline. At the most, parents are requested to send not more than toffees for the class children. Anything more than this will be sent back.</p>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">STUDENT ABSENCE RULES</strong></h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">1.</strong>&nbsp;Any kind of leave requires prior sanctioning.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">2.</strong>&nbsp;If your child is absent, please inform the school promptly, preferably your child&rsquo;s class teacher in writing.</p>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">ID Cards</strong></h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">1.</strong>&nbsp;ID cards proforma to be submitted at the time of admission (in case of new admission).</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">2.</strong>&nbsp;In case of loss of ID Rs. 100 has to be paid.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">3.</strong>&nbsp;Rs. 25 will be charged for the new string.</p>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">School Uniform</strong></h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">The school uniform is available at the school Uniform shop in the school campus Wearing the specified school uniform is compulsory. Wearing of the school uniform is not only a necessity, but also a matter of pride. It brings about oneness amongst the children and responsibility in their attitude. To maintain uniformity of shades design and material, parents are advised to buy the uniform for their ward, from the School Shop only. Parents are requested to avoid buying oversized uniforms, because it does not look smart and becomes inconvenient for the child to move about freely.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">Boys must sport a neat look. Girls with short hair (above the collar) should wear hair band. Hair longer than should be tied back from the face. No jewellery, except for a wrist watch and a simple pair of earrings may be worn. Every child must carry a handkerchief. The onus of sending the child to school in neat, well fitting and decent uniform lies on the parents.</p>\r\n\r\n<h3 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 21px; line-height: 30px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">School Transport System</strong></h3>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">The school has its own fleet of buses. Students can avail the school transport subject to availability of seats. The routes of the school buses are planned by the school. Parents should consult the Transport In charge for necessary details.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">For safety of the students, the following rules are to be followed by every student traveling by the school bus.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">1.</strong>&nbsp;All the students using the school bus are expected to be at the bus stop at least 5 minutes before the arrival of the bus. Buses will not wait for late comers.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">2.</strong>&nbsp;Students should board and get off only at the respective stops assigned to them.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">3.</strong>&nbsp;Children should stay away from main road until the bus arrives.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">4.</strong>&nbsp;The drivers are authorized to stop buses only at the designated stops.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">5.</strong>&nbsp;The bus incharges and student bus monitors are responsible for maintaining discipline in the bus en route.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">6.</strong>&nbsp;Under no circumstances children will be allowed to board another bus without prior permission from the principal.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">7.</strong>&nbsp;No student will enter the driver&rsquo;s cabin.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">8.</strong>&nbsp;Parents are to make sure that they pick their children at the notified time from the specified bus stop Failing which the child will be brought back to the school from where the parent is to collect the child.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">9.</strong>&nbsp;In case due to any reason, student using the school transport has to be picked up by parent/ guardian, the following rules will be observed.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">9.1) :&nbsp;</strong>A written request to be sent in the morning for issuing of gate pass.</p>\r\n\r\n<p align=\"justify\" style=\"padding: 0px; margin: 0px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\"><strong style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">9.2) :&nbsp;</strong>Gate pass will be issued on producing parent identity card at the reception. The gate pass is to be shown at the gate at the time of departure.</p>\r\n</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-15 13:57:05');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (112, 'manual', 0, 'List of Holidays', 'page/list-of-holidays', 'page', 'list-of-holidays', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">Holiday List</h1>\r\n\r\n<table align=\"center\" class=\"tableStyle\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76); height: 33px;\" width=\"237\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle1\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"100%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">Academic Session Period</span></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76); height: 58px;\" width=\"776\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 April 2014</td>\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">31 March 2015</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">&nbsp;</p>\r\n\r\n<table align=\"center\" class=\"tableStyle\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76); height: 33px;\" width=\"173\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle1\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"100%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">Admission Period</span></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76); height: 58px;\" width=\"782\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 April 2014</td>\r\n			<td align=\"center\" class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">31 August 2014</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">&nbsp;</p>\r\n\r\n<table align=\"center\" class=\"tableStyle\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle1\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"100%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">HOLIDAY LIST</span></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"padding: 0px; margin: 0px 0px 10px; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">&nbsp;</p>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">APRIL 2014</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">8 April 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">8 April 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Ram Navmi</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">13 April 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">13 April 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Mahavir Jayanti</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">14 April 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">14 April 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Dr. Ambedkar Jayanti</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">18 April 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">18 April 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Good Friday</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">May-June 2014</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">11 May 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">30 June 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">51 DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Summer Break</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">July &ndash; August 2014</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">29 August 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">29 August 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Eid-Ul-Fitr</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">10 August 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">10 August 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Rakhsha Bandhan</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">15 August 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">15 August 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Independence Day</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">18 August 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">18 August 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Janmashtami</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">October 2014</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">02 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">02 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Gandhi Jayanti</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">03 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">03 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Maha Navmi & Dusshera</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">6 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">6 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Eid-Ul-Zuha(Bakrid)</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">11 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">11 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Karva Chauth</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">23 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">23 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Diwali</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">25 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">25 October 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Bhai Dooj</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">November 2014</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 November 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 November 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Haryana Day</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">04 November 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">04 November 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Muharram</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">06 November 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">06 November 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Guru Nanak Jayanti</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">December 2014</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">25 December 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">25 December 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Christmas Day</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">26 December 2014</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">15 January 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">20 DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Winter Break</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">January 2015</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">26 January 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">26 January 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Republic Day Day</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px 0px 10px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">Febuary 2015</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">17 Febuary 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">17 Febuary 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Maha Shivratri</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<table align=\"center\" class=\"tableStyle\" frame=\"box\" rules=\"all\" style=\"padding: 0px; margin: 0px; border-collapse: collapse; border-spacing: 0px; box-sizing: border-box; border-bottom: 1px solid rgb(5, 16, 21); width: 890px; border-top-color: rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21); color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\" width=\"60%\">\r\n	<tbody style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n		<tr class=\"RowStyle2\" style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td colspan=\"4\" rowspan=\"1\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\"><span style=\"padding: 0px; margin: 0px; box-sizing: border-box; font-size: large;\">March 2015</span></td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">FROM DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">TO DATE</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">NO. OF DAYS</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">HOLIDAY LIST</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">5/6 March 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">5/6 March 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Holi</td>\r\n		</tr>\r\n		<tr style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">6/7 March 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">6/7 March 2015</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">1 DAY</td>\r\n			<td class=\"ContentRow\" style=\"padding: 5px 10px 5px 5px; margin: 0px; box-sizing: border-box; border-top: 1px solid rgb(5, 16, 21); border-right-color: rgb(5, 16, 21); border-bottom-color: rgb(5, 16, 21); border-left-color: rgb(5, 16, 21);\" valign=\"top\" width=\"25%\">Dulhendi</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-15 13:58:03');
INSERT INTO `front_cms_pages` (`id`, `page_type`, `is_homepage`, `title`, `url`, `type`, `slug`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `description`, `publish_date`, `publish`, `sidebar`, `is_active`, `created_at`) VALUES (113, 'manual', 0, 'Rules & Regulations', 'page/rules-regulations', 'page', 'rules-regulations', '', '', '', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h1 style=\"padding: 0px; margin: 0px 0px 10px; clear: both; color: rgb(233, 245, 250); font-variant-numeric: normal; font-variant-east-asian: normal; font-weight: normal; font-stretch: normal; font-size: 40px; line-height: 50px; font-family: \"Fauna One\", Helvetica, Arial, Verdana, sans-serif; box-sizing: border-box; text-shadow: none; background-color: rgba(7, 33, 46, 0.76);\">School Rules & Regulations</h1>\r\n\r\n<ul class=\"checked\" style=\"padding: 0px; margin: 0px 0px 0px 20px; list-style-position: outside; list-style-image: none; box-sizing: border-box; color: rgb(144, 174, 190); font-family: \"Trebuchet MS\", Helvetica, Arial, Verdana, sans-serif; font-size: 14px; background-color: rgba(7, 33, 46, 0.76);\">\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Students should arrive at school before the bell rings. The school gates will close thereafter.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Students are expected to respect the school property. No student should damage any school furniture, write or draw anything on walls or in any way damage things belonging to others. Damage done even by accident should be reported at once to the section educators or to the Principal. Students are liable to pay for the damage caused.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">The school is not responsible for goods lost. It is not advisable to bring valuable articles like expensive watches or fountain pens to school.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Students who come to school in the care of some person should never leave before the person arrives. In case of any delay, they should report to the school office. Those who go home alone should be prompt in returning straight home.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">The school reserves the right to suspend or to take strict disciplinary action against a student whose diligence is constantly unsatisfactory or whose conduct is harmful to other students.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Once a child has come to the school, he/she should not be asked to come home on half day leave or leave from any period. In case emergency, the parents may collect the child from the school personally.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Students should get their assessment reports signed by their parents/guardians within threes days of the receipt of the assessment reports and return them to the section educators.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Students should not miss any teaching/games/ activity period.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">No student shall indulge in any of the following practices namely:\r\n	<ul style=\"padding: 0px; margin: 0px 0px 0px 20px; list-style: outside none disc; box-sizing: border-box;\">\r\n		<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Disfiguring or otherwise damaging any school property.</li>\r\n		<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Rowdy and rude behavior.</li>\r\n		<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Use of violence in any form.</li>\r\n		<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Casteism, communism or practice of un-touchability.</li>\r\n	</ul>\r\n	</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Please note that no request, seeking permission to allow the child to go to friends/ relative&rsquo;s house, shall be entertained.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Students suffering from infectious diseases like Conjunctivitis, Dermatitis Scabies etc. should not be sent to the school.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Students suffering from chronic diseases like Asthma, Epilepsy Rheumatic heart disease etc. are advised to be under the continuous medical supervision of a specialist doctor. History of their illness must be filled up in the diary</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">A student returning to the school after suffering from an infectious or contagious disease should produce a doctor&rsquo;s certificate of fitness permitting him to attend the school. He/she must make up all the work missed by him/her.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Repeated absence without leave application, unexplained absence for more than six consecutive days renders the student&rsquo;s admission canceled.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">In case of any indisposition/injury/mishap, the school shall provide First Aid to the students and parents will be informed. In case of any emergency, students will be referred to the nearest hospital and parents shall bear all expenses thereof.</li>\r\n	<li style=\"padding: 0px; margin: 0px; box-sizing: border-box;\">Parents under no circumstances are allowed to directly enter the classroom during school hours.</li>\r\n</ul>\r\n</body>\r\n</html>', '0000-00-00', 0, NULL, 'no', '2019-12-15 13:58:49');


#
# TABLE STRUCTURE FOR: front_cms_program_photos
#

DROP TABLE IF EXISTS `front_cms_program_photos`;

CREATE TABLE `front_cms_program_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) DEFAULT NULL,
  `media_gallery_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `program_id` (`program_id`),
  CONSTRAINT `front_cms_program_photos_ibfk_1` FOREIGN KEY (`program_id`) REFERENCES `front_cms_programs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

INSERT INTO `front_cms_program_photos` (`id`, `program_id`, `media_gallery_id`, `created_at`) VALUES (39, 1, 85, '2019-12-15 01:52:04');
INSERT INTO `front_cms_program_photos` (`id`, `program_id`, `media_gallery_id`, `created_at`) VALUES (41, 1, 82, '2019-12-15 01:52:24');
INSERT INTO `front_cms_program_photos` (`id`, `program_id`, `media_gallery_id`, `created_at`) VALUES (42, 1, 84, '2019-12-15 01:52:32');


#
# TABLE STRUCTURE FOR: front_cms_programs
#

DROP TABLE IF EXISTS `front_cms_programs`;

CREATE TABLE `front_cms_programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `url` mediumtext DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `event_start` date DEFAULT NULL,
  `event_end` date DEFAULT NULL,
  `event_venue` mediumtext DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `is_active` varchar(10) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `meta_title` mediumtext NOT NULL,
  `meta_description` mediumtext NOT NULL,
  `meta_keyword` mediumtext NOT NULL,
  `feature_image` mediumtext NOT NULL,
  `publish_date` date NOT NULL,
  `publish` varchar(10) DEFAULT '0',
  `sidebar` int(10) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (1, 'banner', NULL, NULL, 'Banner Images', NULL, NULL, NULL, NULL, NULL, 'no', '2019-12-05 10:22:29', '', '', '', '', '0000-00-00', '0', 0);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (5, 'gallery', 'bhajan-sandhya-good', 'read/bhajan-sandhya-good', 'bhajan sandhya good', NULL, NULL, NULL, NULL, '<p><span font-size:=\"\" open=\"\">Fusce semper, nibh eu sollicitudin imperdiet, dolor magna vestibulum mi, vel tincidunt augue ipsum nec erat. Vestibulum congue leo elementum ullamcorper commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</span></p>\r\n\r\n<p><span font-size:=\"\" open=\"\">Fusce semper, nibh eu sollicitudin imperdiet, dolor magna vestibulum mi, vel tincidunt augue ipsum nec erat. Vestibulum congue leo elementum ullamcorper commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</span></p>', 'no', '2019-05-07 16:01:19', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal6.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (6, 'gallery', 'art', 'read/art', 'Art', NULL, NULL, NULL, NULL, '<p>Fusce semper, nibh eu sollicitudin imperdiet, dolor magna vestibulum mi, vel tincidunt augue ipsum nec erat. Vestibulum congue leo elementum ullamcorper commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>', 'no', '2019-04-27 17:30:54', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal15.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (7, 'gallery', 'pre-primary', 'read/pre-primary', 'Pre Primary', NULL, NULL, NULL, NULL, '<p>Fusce semper, nibh eu sollicitudin imperdiet, dolor magna vestibulum mi, vel tincidunt augue ipsum nec erat. Vestibulum congue leo elementum ullamcorper commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>', 'no', '2019-04-27 17:06:01', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal21.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (8, 'gallery', 'ncc-bands', 'read/ncc-bands', 'NCC & Bands', NULL, NULL, NULL, NULL, '<p><span xss=\"removed\">Fusce semper, nibh eu sollicitudin imperdiet, dolor magna vestibulum mi, vel tincidunt augue ipsum nec erat. Vestibulum congue leo elementum ullamcorper commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</span></p>\r\n\r\n<p><span xss=\"removed\">Fusce semper, nibh eu sollicitudin imperdiet, dolor magna vestibulum mi, vel tincidunt augue ipsum nec erat. Vestibulum congue leo elementum ullamcorper commodo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</span></p>', 'no', '2019-04-27 16:06:23', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal34.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (9, 'gallery', 'recreation-centre', 'read/recreation-centre', 'Recreation Centre', NULL, NULL, NULL, NULL, '<p>School&nbsp;Recreation Centre</p>', 'no', '2019-04-27 17:29:56', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal77.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (10, 'gallery', 'facilities', 'read/facilities', 'Facilities', NULL, NULL, NULL, NULL, '<p>School Facilities</p>', 'no', '2019-04-27 17:28:16', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal3.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (11, 'gallery', 'activities', 'read/activities', 'Activities', NULL, NULL, NULL, NULL, '<p><span style=\"color: rgb(68, 68, 68); font-family: Roboto, sans-serif; font-size: 15px;\">Bimply dummy text of the printing and typesetting istryrem Ipsum has been the industry&#39;s standard dummy text ever when an unknown printer.</span></p>', 'no', '2019-04-27 16:13:14', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal25.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (12, 'gallery', 'sports', 'read/sports', 'Sports', NULL, NULL, NULL, NULL, '<p>School Sports</p>', 'no', '2019-05-04 16:29:56', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal38.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (13, 'gallery', 'celebration', 'read/celebration', 'Celebration', NULL, NULL, NULL, NULL, '<p>School Celebration</p>', 'no', '2019-04-27 17:23:41', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal5.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (14, 'gallery', 'campus', 'read/campus', 'Campus', NULL, NULL, NULL, NULL, '<p><img src=\"https://demo.smart-school.in/uploads/gallery/media/gal4.jpg\" /></p>', 'no', '2019-04-27 16:07:07', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal4.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (19, 'events', 'school-band-troupe', 'read/school-band-troupe', 'School Band Troupe', NULL, '2019-04-10', '2019-04-10', '', '<p open=\"\" style=\"line-height: 22px; font-family: \">MCRS hosts several competitions for fostering the talents and skills of students. The main competitions are as follows:</p>\r\n\r\n<h2 open=\"\" style=\"font-size: 20px; color: rgb(139, 22, 24); font-weight: lighter; margin: 5px 0px 10px; font-family: \">Literary Fest</h2>\r\n\r\n<p open=\"\" style=\"line-height: 22px; font-family: \">To develop the literary skills of students the Literary Fest is conducted every academic year. Competitions such as essay writing, debates, group discussions, elocutions and many more are conducted during the Literary Fest. Competitions are conducted for juniors and seniors house wise. &lsquo;Literary Genius Award&rsquo; goes to the participant who has scored the top marks.<strong><em>&nbsp;&lsquo;</em></strong>Best House-Literary&rsquo;- Rolling trophy presented to the house that scores the highest point in Literary Fest.</p>', 'no', '2019-05-01 17:17:20', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal31.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (20, 'events', 'competitions', 'read/competitions', 'Competitions', NULL, '2019-04-10', '2019-04-10', '', '<p><span open=\"\" style=\"font-family: \">To develop the literary skills of students the Literary Fest is conducted every academic year. Competitions such as essay writing, debates, group discussions, elocutions and many more are conducted during the Literary Fest. Competitions are conducted for juniors and seniors house wise. &lsquo;Literary Genius Award&rsquo; goes to the participant who has scored the top marks.</span><strong open=\"\" style=\"font-family: \"><em>&nbsp;&lsquo;</em></strong><span open=\"\" style=\"font-family: \">Best House-Literary&rsquo;- Rolling trophy presented to the house that scores the highest point in Literary Fest.</span></p>\r\n\r\n<p><span open=\"\" style=\"font-family: \">Students get an opportunity to bring out the artist them. Competitions are held on various art forms, such as dance items, music, mime, fancy dress etc. Competitions are all conducted house wise. The &lsquo;Kalathilakam&rsquo; and &lsquo;Kalaprathibha&rsquo; awards are presented to the girl and boy participant respectively, who scores the top points for the Arts Fest.</span><strong open=\"\" style=\"font-family: \"><em>&nbsp;&lsquo;</em></strong><span open=\"\" style=\"font-family: \">Best House -Arts t&rsquo;- Rolling trophy is presented to the house that scores the highest point in Arts Fest.</span></p>', 'no', '2019-04-30 14:39:15', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal43.jpg', '0000-00-00', '0', NULL);
INSERT INTO `front_cms_programs` (`id`, `type`, `slug`, `url`, `title`, `date`, `event_start`, `event_end`, `event_venue`, `description`, `is_active`, `created_at`, `meta_title`, `meta_description`, `meta_keyword`, `feature_image`, `publish_date`, `publish`, `sidebar`) VALUES (21, 'events', 'working-days-time-schedule', 'read/working-days-time-schedule', 'Working Days & Time Schedule', NULL, '2019-04-10', '2019-04-10', '', '<p style=\"margin: 4px 0px; text-align: left;\">The school Administration Office works from Monday to Saturday, from 8:00 am to 4:00pm.</p>\r\n\r\n<p style=\"margin: 4px 0px; text-align: left;\">On days such as Xmas, Onam, Good Friday the school Administration Office will not function.</p>\r\n\r\n<p style=\"margin: 4px 0px; text-align: left;\">The classes are held on all weekdays from Monday to Friday, from 8:30 am to 2:30 pm.</p>\r\n\r\n<p style=\"margin: 4px 0px; text-align: left;\">Supervisory classes are conducted on working days intimated for the senior classes from 3:00 pm to 6:00 pm.</p>\r\n\r\n<p style=\"margin: 4px 0px; text-align: left;\">The summer vacation for the students will begin on the first of April and ends on the last of May, unless otherwise specified.</p>\r\n\r\n<p style=\"margin: 4px 0px; text-align: left;\">Normally 10 days are given as holidays for Onam and Xmas.</p>\r\n\r\n<p style=\"margin: 4px 0px; text-align: left;\">The school will work on Patriotic days so as to celebrate these days.</p>\r\n\r\n<p style=\"margin: 4px 0px; text-align: left;\">School will remain closed for the students on the days the teachers have teachers training programmes.</p>\r\n\r\n<p style=\"margin: 4px 0px; text-align: left;\">The time schedule may vary on the days of functions or competitions.</p>', 'no', '2019-04-30 14:38:37', '', '', '', 'https://demo.smart-school.in/uploads/gallery/media/gal41.jpg', '0000-00-00', '0', NULL);


#
# TABLE STRUCTURE FOR: front_cms_settings
#

DROP TABLE IF EXISTS `front_cms_settings`;

CREATE TABLE `front_cms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme` varchar(50) DEFAULT NULL,
  `is_active_rtl` int(10) DEFAULT 0,
  `is_active_front_cms` int(11) DEFAULT 0,
  `is_active_sidebar` int(1) DEFAULT 0,
  `logo` varchar(200) DEFAULT NULL,
  `contact_us_email` varchar(100) DEFAULT NULL,
  `complain_form_email` varchar(100) DEFAULT NULL,
  `sidebar_options` mediumtext NOT NULL,
  `fb_url` varchar(200) NOT NULL,
  `twitter_url` varchar(200) NOT NULL,
  `youtube_url` varchar(200) NOT NULL,
  `google_plus` varchar(200) NOT NULL,
  `instagram_url` varchar(200) NOT NULL,
  `pinterest_url` varchar(200) NOT NULL,
  `linkedin_url` varchar(200) NOT NULL,
  `google_analytics` mediumtext DEFAULT NULL,
  `footer_text` varchar(500) DEFAULT NULL,
  `fav_icon` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `front_cms_settings` (`id`, `theme`, `is_active_rtl`, `is_active_front_cms`, `is_active_sidebar`, `logo`, `contact_us_email`, `complain_form_email`, `sidebar_options`, `fb_url`, `twitter_url`, `youtube_url`, `google_plus`, `instagram_url`, `pinterest_url`, `linkedin_url`, `google_analytics`, `footer_text`, `fav_icon`, `created_at`) VALUES (1, 'bold_blue', NULL, 1, NULL, './uploads/school_content/logo/front_logo-5df617c8e16705.80810780.png', '', '', '[\"news\"]', 'http://facebook.com', 'http://facebook.com', 'http://facebook.com', 'http://facebook.com', 'http://facebook.com', 'http://facebook.com', 'http://facebook.com', '', 'Copyright 2019 @ SIS', './uploads/school_content/logo/front_fav_icon-5df61b63288ec2.45494317.png', '2019-12-15 06:39:23');


#
# TABLE STRUCTURE FOR: general_calls
#

DROP TABLE IF EXISTS `general_calls`;

CREATE TABLE `general_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(500) NOT NULL,
  `follow_up_date` date NOT NULL,
  `call_dureation` varchar(50) NOT NULL,
  `note` mediumtext NOT NULL,
  `call_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: grades
#

DROP TABLE IF EXISTS `grades`;

CREATE TABLE `grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `point` float(10,1) DEFAULT NULL,
  `mark_from` float(10,2) DEFAULT NULL,
  `mark_upto` float(10,2) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: homework
#

DROP TABLE IF EXISTS `homework`;

CREATE TABLE `homework` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `homework_date` date NOT NULL,
  `submit_date` date NOT NULL,
  `staff_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `create_date` date NOT NULL,
  `document` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `evaluated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: homework_evaluation
#

DROP TABLE IF EXISTS `homework_evaluation`;

CREATE TABLE `homework_evaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `homework_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: hostel
#

DROP TABLE IF EXISTS `hostel`;

CREATE TABLE `hostel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostel_name` varchar(100) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `intake` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: hostel_rooms
#

DROP TABLE IF EXISTS `hostel_rooms`;

CREATE TABLE `hostel_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostel_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `room_no` varchar(200) DEFAULT NULL,
  `no_of_bed` int(11) DEFAULT NULL,
  `cost_per_bed` float(10,2) DEFAULT 0.00,
  `title` varchar(200) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: id_card
#

DROP TABLE IF EXISTS `id_card`;

CREATE TABLE `id_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `school_name` varchar(100) NOT NULL,
  `school_address` varchar(500) NOT NULL,
  `background` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `sign_image` varchar(100) NOT NULL,
  `header_color` varchar(100) NOT NULL,
  `enable_admission_no` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_student_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_class` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_fathers_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_mothers_name` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_address` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_phone` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_dob` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `enable_blood_group` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  `status` tinyint(1) NOT NULL COMMENT '0=disable,1=enable',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `id_card` (`id`, `title`, `school_name`, `school_address`, `background`, `logo`, `sign_image`, `header_color`, `enable_admission_no`, `enable_student_name`, `enable_class`, `enable_fathers_name`, `enable_mothers_name`, `enable_address`, `enable_phone`, `enable_dob`, `enable_blood_group`, `status`) VALUES (1, 'Sample Student Identity Card', 'Mount Carmel School', '110 Kings Street, CA  Phone: 456542 Email: mount@gmail.com', 'samplebackground12.png', 'samplelogo12.png', 'samplesign12.png', '#595959', 1, 1, 1, 1, 0, 1, 1, 1, 1, 1);


#
# TABLE STRUCTURE FOR: income
#

DROP TABLE IF EXISTS `income`;

CREATE TABLE `income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inc_head_id` varchar(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `invoice_no` varchar(200) NOT NULL,
  `date` date DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `note` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'yes',
  `is_deleted` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `documents` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: income_head
#

DROP TABLE IF EXISTS `income_head`;

CREATE TABLE `income_head` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `income_category` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` varchar(255) NOT NULL DEFAULT 'yes',
  `is_deleted` varchar(255) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: item
#

DROP TABLE IF EXISTS `item`;

CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_category_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `item_photo` varchar(225) DEFAULT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `item_store_id` int(11) DEFAULT NULL,
  `item_supplier_id` int(11) DEFAULT NULL,
  `quantity` int(100) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: item_category
#

DROP TABLE IF EXISTS `item_category`;

CREATE TABLE `item_category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `item_category` varchar(255) NOT NULL,
  `is_active` varchar(255) NOT NULL DEFAULT 'yes',
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: item_issue
#

DROP TABLE IF EXISTS `item_issue`;

CREATE TABLE `item_issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_type` varchar(15) DEFAULT NULL,
  `issue_to` varchar(100) DEFAULT NULL,
  `issue_by` varchar(100) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `item_category_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `note` text NOT NULL,
  `is_returned` int(2) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` varchar(10) DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `item_category_id` (`item_category_id`),
  CONSTRAINT `item_issue_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_issue_ibfk_2` FOREIGN KEY (`item_category_id`) REFERENCES `item_category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: item_stock
#

DROP TABLE IF EXISTS `item_stock`;

CREATE TABLE `item_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `symbol` varchar(10) NOT NULL DEFAULT '+',
  `store_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `attachment` varchar(250) DEFAULT NULL,
  `description` text NOT NULL,
  `is_active` varchar(10) DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `store_id` (`store_id`),
  CONSTRAINT `item_stock_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_stock_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `item_supplier` (`id`) ON DELETE CASCADE,
  CONSTRAINT `item_stock_ibfk_3` FOREIGN KEY (`store_id`) REFERENCES `item_store` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: item_store
#

DROP TABLE IF EXISTS `item_store`;

CREATE TABLE `item_store` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `item_store` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: item_supplier
#

DROP TABLE IF EXISTS `item_supplier`;

CREATE TABLE `item_supplier` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `item_supplier` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_person_name` varchar(255) NOT NULL,
  `contact_person_phone` varchar(255) NOT NULL,
  `contact_person_email` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: languages
#

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(50) DEFAULT NULL,
  `is_deleted` varchar(10) NOT NULL DEFAULT 'yes',
  `is_active` varchar(255) DEFAULT 'no',
  `code` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (1, 'Azerbaijan', 'no', 'no', 'az', '2019-03-25 10:27:01', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (2, 'Albanian', 'no', 'no', 'sq', '2019-03-25 10:25:01', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (3, 'Amharic', 'no', 'no', 'am', '2019-03-25 10:25:16', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (4, 'English', 'no', 'no', 'en', '2019-03-25 10:32:05', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (5, 'Arabic', 'no', 'no', 'ar', '2019-03-25 10:26:50', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (7, 'Afrikaans', 'no', 'no', 'af', '2019-03-25 10:24:33', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (8, 'Basque', 'no', 'no', 'eu', '2019-03-25 10:27:41', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (11, 'Bengali', 'no', 'no', 'bn', '2019-03-25 10:27:52', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (13, 'Bosnian', 'no', 'no', 'bs', '2019-03-25 10:28:03', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (14, 'Welsh', 'no', 'no', 'cy', '2019-03-25 10:46:16', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (15, 'Hungarian', 'no', 'no', 'hu', '2019-03-25 10:36:29', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (16, 'Vietnamese', 'no', 'no', 'vi', '2019-03-25 10:46:06', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (17, 'Haitian (Creole)', 'no', 'no', 'ht', '2019-03-25 10:35:54', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (18, 'Galician', 'no', 'no', 'gl', '2019-03-25 10:35:00', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (19, 'Dutch', 'no', 'no', 'nl', '2019-03-25 10:31:52', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (21, 'Greek', 'no', 'no', 'el', '2019-03-25 10:35:28', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (22, 'Georgian', 'no', 'no', 'ka', '2019-03-25 10:35:10', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (23, 'Gujarati', 'no', 'no', 'gu', '2019-03-25 10:35:38', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (24, 'Danish', 'no', 'no', 'da', '2019-03-25 10:31:40', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (25, 'Hebrew', 'no', 'no', 'he', '2019-03-25 10:36:06', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (26, 'Yiddish', 'no', 'no', 'yi', '2019-03-25 10:46:34', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (27, 'Indonesian', 'no', 'no', 'id', '2019-03-25 10:36:51', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (28, 'Irish', 'no', 'no', 'ga', '2019-03-25 10:37:02', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (29, 'Italian', 'no', 'no', 'it', '2019-03-25 10:37:11', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (30, 'Icelandic', 'no', 'no', 'is', '2019-03-25 10:36:40', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (31, 'Spanish', 'no', 'no', 'es', '2019-03-25 10:44:20', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (33, 'Kannada', 'no', 'no', 'kn', '2019-03-25 10:37:41', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (34, 'Catalan', 'no', 'no', 'ca', '2019-03-25 10:28:20', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (36, 'Chinese', 'no', 'no', 'zh', '2019-03-25 10:31:19', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (37, 'Korean', 'no', 'no', 'ko', '2019-03-25 10:37:55', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (38, 'Xhosa', 'no', 'no', 'xh', '2019-03-25 10:46:26', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (39, 'Latin', 'no', 'no', 'la', '2019-03-25 10:38:04', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (40, 'Latvian', 'no', 'no', 'lv', '2019-03-25 10:38:41', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (41, 'Lithuanian', 'no', 'no', 'lt', '2019-03-25 10:38:46', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (43, 'Malagasy', 'no', 'no', 'mg', '2019-03-25 10:39:17', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (44, 'Malay', 'no', 'no', 'ms', '2019-03-25 10:39:26', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (45, 'Malayalam', 'no', 'no', 'ml', '2019-03-25 10:39:36', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (46, 'Maltese', 'no', 'no', 'mt', '2019-03-25 10:39:46', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (47, 'Macedonian', 'no', 'no', 'mk', '2019-03-25 10:39:06', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (48, 'Maori', 'no', 'no', 'mi', '2019-03-25 10:39:55', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (49, 'Marathi', 'no', 'no', 'mr', '2019-03-25 10:40:04', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (51, 'Mongolian', 'no', 'no', 'mn', '2019-03-25 10:40:14', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (52, 'German', 'no', 'no', 'de', '2019-03-25 10:35:20', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (53, 'Nepali', 'no', 'no', 'ne', '2019-03-25 10:40:24', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (54, 'Norwegian', 'no', 'no', 'no', '2019-03-25 10:40:43', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (55, 'Punjabi', 'no', 'no', 'pa', '2019-03-25 10:41:20', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (57, 'Persian', 'no', 'no', 'fa', '2019-03-25 10:40:57', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (59, 'Portuguese', 'no', 'no', 'pt', '2019-03-25 10:41:09', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (60, 'Romanian', 'no', 'no', 'ro', '2019-03-25 10:41:41', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (61, 'Russian', 'no', 'no', 'ru', '2019-03-25 10:43:19', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (62, 'Cebuano', 'no', 'no', '', '2017-04-06 01:08:33', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (64, 'Sinhala', 'no', 'no', 'si', '2019-03-25 10:43:46', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (65, 'Slovakian', 'no', 'no', '', '2017-04-06 01:08:33', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (66, 'Slovenian', 'no', 'no', 'sl', '2019-03-25 10:44:07', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (67, 'Swahili', 'no', 'no', 'sw', '2019-03-25 10:44:40', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (68, 'Sundanese', 'no', 'no', 'su', '2019-03-25 10:44:30', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (70, 'Thai', 'no', 'no', 'th', '2019-03-25 10:45:29', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (71, 'Tagalog', 'no', 'no', 'tl', '2019-03-25 10:45:00', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (72, 'Tamil', 'no', 'no', 'ta', '2019-03-25 10:45:09', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (74, 'Telugu', 'no', 'no', 'te', '2019-03-25 10:45:18', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (75, 'Turkish', 'no', 'no', 'tr', '2019-03-25 10:45:36', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (77, 'Uzbek', 'no', 'no', 'uz', '2019-03-25 10:45:56', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (79, 'Urdu', 'no', 'no', 'ur', '2019-03-25 10:45:47', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (80, 'Finnish', 'no', 'no', 'fi', '2019-03-25 10:34:34', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (81, 'French', 'no', 'no', 'fr', '2019-03-25 10:34:46', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (82, 'Hindi', 'no', 'no', 'hi', '2019-03-25 10:36:19', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (84, 'Czech', 'no', 'no', 'cs', '2019-03-25 10:31:30', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (85, 'Swedish', 'no', 'no', 'sv', '2019-03-25 10:44:50', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (86, 'Scottish', 'no', 'no', 'gd', '2019-03-25 10:43:35', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (87, 'Estonian', 'no', 'no', 'et', '2019-03-25 10:34:20', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (88, 'Esperanto', 'no', 'no', 'eo', '2019-03-25 10:34:12', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (89, 'Javanese', 'no', 'no', 'jv', '2019-03-25 10:37:31', '0000-00-00 00:00:00');
INSERT INTO `languages` (`id`, `language`, `is_deleted`, `is_active`, `code`, `created_at`, `updated_at`) VALUES (90, 'Japanese', 'no', 'no', 'ja', '2019-03-25 10:37:20', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: leave_types
#

DROP TABLE IF EXISTS `leave_types`;

CREATE TABLE `leave_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `is_active` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: libarary_members
#

DROP TABLE IF EXISTS `libarary_members`;

CREATE TABLE `libarary_members` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `library_card_no` varchar(50) DEFAULT NULL,
  `member_type` varchar(50) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: messages
#

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `send_mail` varchar(10) DEFAULT '0',
  `send_sms` varchar(10) DEFAULT '0',
  `is_group` varchar(10) DEFAULT '0',
  `is_individual` varchar(10) DEFAULT '0',
  `is_class` int(10) NOT NULL DEFAULT 0,
  `group_list` text DEFAULT NULL,
  `user_list` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: migrations
#

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: notification_roles
#

DROP TABLE IF EXISTS `notification_roles`;

CREATE TABLE `notification_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_notification_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `send_notification_id` (`send_notification_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `notification_roles_ibfk_1` FOREIGN KEY (`send_notification_id`) REFERENCES `send_notification` (`id`) ON DELETE CASCADE,
  CONSTRAINT `notification_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: notification_setting
#

DROP TABLE IF EXISTS `notification_setting`;

CREATE TABLE `notification_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `is_mail` varchar(10) DEFAULT '0',
  `is_sms` varchar(10) DEFAULT '0',
  `display_notification` int(11) NOT NULL DEFAULT 0,
  `is_notification` int(1) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `notification_setting` (`id`, `type`, `is_mail`, `is_sms`, `display_notification`, `is_notification`, `created_at`) VALUES (1, 'student_admission', '1', '0', 0, 0, '2019-04-10 10:27:50');
INSERT INTO `notification_setting` (`id`, `type`, `is_mail`, `is_sms`, `display_notification`, `is_notification`, `created_at`) VALUES (2, 'exam_result', '1', '0', 1, 0, '2019-04-10 10:27:50');
INSERT INTO `notification_setting` (`id`, `type`, `is_mail`, `is_sms`, `display_notification`, `is_notification`, `created_at`) VALUES (3, 'fee_submission', '1', '0', 1, 0, '2019-04-10 10:27:50');
INSERT INTO `notification_setting` (`id`, `type`, `is_mail`, `is_sms`, `display_notification`, `is_notification`, `created_at`) VALUES (4, 'absent_attendence', '1', '0', 1, 0, '2019-04-10 10:27:50');
INSERT INTO `notification_setting` (`id`, `type`, `is_mail`, `is_sms`, `display_notification`, `is_notification`, `created_at`) VALUES (5, 'login_credential', '1', '0', 0, 0, '2019-04-10 10:27:50');


#
# TABLE STRUCTURE FOR: payment_settings
#

DROP TABLE IF EXISTS `payment_settings`;

CREATE TABLE `payment_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(200) NOT NULL,
  `api_username` varchar(200) DEFAULT NULL,
  `api_secret_key` varchar(200) NOT NULL,
  `salt` varchar(200) NOT NULL,
  `api_publishable_key` varchar(200) NOT NULL,
  `api_password` varchar(200) DEFAULT NULL,
  `api_signature` varchar(200) DEFAULT NULL,
  `api_email` varchar(200) DEFAULT NULL,
  `paypal_demo` varchar(100) NOT NULL,
  `account_no` varchar(200) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `payment_settings` (`id`, `payment_type`, `api_username`, `api_secret_key`, `salt`, `api_publishable_key`, `api_password`, `api_signature`, `api_email`, `paypal_demo`, `account_no`, `is_active`, `created_at`, `updated_at`) VALUES (1, 'paypal', '', '', '', '', '', '', NULL, '', '', 'yes', '2018-07-12 01:26:13', '0000-00-00 00:00:00');
INSERT INTO `payment_settings` (`id`, `payment_type`, `api_username`, `api_secret_key`, `salt`, `api_publishable_key`, `api_password`, `api_signature`, `api_email`, `paypal_demo`, `account_no`, `is_active`, `created_at`, `updated_at`) VALUES (2, 'stripe', NULL, '', '', '', NULL, NULL, NULL, '', '', 'no', '2018-07-12 01:26:26', '0000-00-00 00:00:00');
INSERT INTO `payment_settings` (`id`, `payment_type`, `api_username`, `api_secret_key`, `salt`, `api_publishable_key`, `api_password`, `api_signature`, `api_email`, `paypal_demo`, `account_no`, `is_active`, `created_at`, `updated_at`) VALUES (3, 'payu', NULL, '', '', '', NULL, NULL, NULL, '', '', 'no', '2018-07-12 01:26:35', '0000-00-00 00:00:00');
INSERT INTO `payment_settings` (`id`, `payment_type`, `api_username`, `api_secret_key`, `salt`, `api_publishable_key`, `api_password`, `api_signature`, `api_email`, `paypal_demo`, `account_no`, `is_active`, `created_at`, `updated_at`) VALUES (4, 'ccavenue', NULL, '', '', '', NULL, NULL, NULL, '', '', 'no', '2018-07-12 01:26:45', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: payslip_allowance
#

DROP TABLE IF EXISTS `payslip_allowance`;

CREATE TABLE `payslip_allowance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payslip_id` int(11) NOT NULL,
  `allowance_type` varchar(200) NOT NULL,
  `amount` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `cal_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: permission_category
#

DROP TABLE IF EXISTS `permission_category`;

CREATE TABLE `permission_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perm_group_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `short_code` varchar(100) DEFAULT NULL,
  `enable_view` int(11) DEFAULT 0,
  `enable_add` int(11) DEFAULT 0,
  `enable_edit` int(11) DEFAULT 0,
  `enable_delete` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;

INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (1, 1, 'Student', 'student', 1, 1, 1, 1, '2018-06-22 11:47:11');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (2, 1, 'Import Student', 'import_student', 1, 0, 0, 0, '2018-06-22 11:47:19');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (3, 1, 'Student Categories', 'student_categories', 1, 1, 1, 1, '2018-06-22 11:47:36');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (4, 1, 'Student Houses', 'student_houses', 1, 1, 1, 1, '2018-06-22 11:47:53');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (5, 2, 'Collect Fees', 'collect_fees', 1, 1, 0, 1, '2018-06-22 11:51:03');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (6, 2, 'Fees Carry Forward', 'fees_carry_forward', 1, 0, 0, 0, '2018-06-27 01:48:15');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (7, 2, 'Fees Master', 'fees_master', 1, 1, 1, 1, '2018-06-27 01:48:57');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (8, 2, 'Fees Group', 'fees_group', 1, 1, 1, 1, '2018-06-22 11:51:46');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (9, 3, 'Income', 'income', 1, 1, 1, 1, '2018-06-22 11:53:21');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (10, 3, 'Income Head', 'income_head', 1, 1, 1, 1, '2018-06-22 11:52:44');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (11, 3, 'Search Income', 'search_income', 1, 0, 0, 0, '2018-06-22 11:53:00');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (12, 4, 'Expense', 'expense', 1, 1, 1, 1, '2018-06-22 11:54:06');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (13, 4, 'Expense Head', 'expense_head', 1, 1, 1, 1, '2018-06-22 11:53:47');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (14, 4, 'Search Expense', 'search_expense', 1, 0, 0, 0, '2018-06-22 11:54:13');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (15, 5, 'Student Attendance', 'student_attendance', 1, 1, 1, 0, '2018-06-22 11:54:49');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (16, 5, 'Student Attendance Report', 'student_attendance_report', 1, 0, 0, 0, '2018-06-22 11:54:26');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (17, 6, 'Exam', 'exam', 1, 1, 1, 1, '2018-06-22 11:56:02');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (19, 6, 'Marks Register', 'marks_register', 1, 1, 1, 0, '2018-06-22 11:56:19');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (20, 6, 'Marks Grade', 'marks_grade', 1, 1, 1, 1, '2018-06-22 11:55:25');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (21, 7, 'Class Timetable', 'class_timetable', 1, 1, 1, 0, '2018-06-22 12:01:36');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (22, 7, 'Assign Subject', 'assign_subject', 1, 1, 1, 1, '2018-06-22 12:01:57');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (23, 7, 'Subject', 'subject', 1, 1, 1, 1, '2018-06-22 12:02:17');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (24, 7, 'Class', 'class', 1, 1, 1, 1, '2018-06-22 12:02:35');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (25, 7, 'Section', 'section', 1, 1, 1, 1, '2018-06-22 12:01:10');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (26, 7, 'Promote Student', 'promote_student', 1, 0, 0, 0, '2018-06-22 12:02:47');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (27, 8, 'Upload Content', 'upload_content', 1, 1, 0, 1, '2018-06-22 12:03:19');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (28, 9, 'Books', 'books', 1, 1, 1, 1, '2018-06-22 12:04:04');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (29, 9, 'Issue Return Student', 'issue_return', 1, 0, 0, 0, '2018-06-22 12:03:41');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (30, 9, 'Add Staff Member', 'add_staff_member', 1, 0, 0, 0, '2018-07-02 13:07:00');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (31, 10, 'Issue Item', 'issue_item', 1, 0, 0, 0, '2018-06-22 12:04:51');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (32, 10, 'Item Stock', 'item_stock', 1, 1, 1, 1, '2018-06-22 12:05:17');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (33, 10, 'Item', 'item', 1, 1, 1, 1, '2018-06-22 12:05:40');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (34, 10, 'Store', 'store', 1, 1, 1, 1, '2018-06-22 12:06:02');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (35, 10, 'Supplier', 'supplier', 1, 1, 1, 1, '2018-06-22 12:06:25');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (37, 11, 'Routes', 'routes', 1, 1, 1, 1, '2018-06-22 12:09:17');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (38, 11, 'Vehicle', 'vehicle', 1, 1, 1, 1, '2018-06-22 12:09:36');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (39, 11, 'Assign Vehicle', 'assign_vehicle', 1, 1, 1, 1, '2018-06-27 06:09:20');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (40, 12, 'Hostel', 'hostel', 1, 1, 1, 1, '2018-06-22 12:10:49');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (41, 12, 'Room Type', 'room_type', 1, 1, 1, 1, '2018-06-22 12:10:27');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (42, 12, 'Hostel Rooms', 'hostel_rooms', 1, 1, 1, 1, '2018-06-25 07:53:03');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (43, 13, 'Notice Board', 'notice_board', 1, 1, 1, 1, '2018-06-22 12:11:17');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (44, 13, 'Email / SMS', 'email_sms', 1, 0, 0, 0, '2018-06-22 12:10:54');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (46, 13, 'Email / SMS Log', 'email_sms_log', 1, 0, 0, 0, '2018-06-22 12:11:23');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (47, 1, 'Student Report', 'student_report', 1, 0, 0, 0, '2018-07-03 12:19:36');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (48, 14, 'Transaction Report', 'transaction_report', 1, 0, 0, 0, '2018-07-06 13:13:32');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (49, 14, 'User Log', 'user_log', 1, 0, 0, 0, '2018-07-06 13:13:53');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (53, 15, 'Languages', 'languages', 0, 1, 0, 0, '2018-06-22 12:13:18');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (54, 15, 'General Setting', 'general_setting', 1, 0, 1, 0, '2018-07-05 10:38:35');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (55, 15, 'Session Setting', 'session_setting', 1, 1, 1, 1, '2018-06-22 12:14:15');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (56, 15, 'Notification Setting', 'notification_setting', 1, 0, 1, 0, '2018-07-05 10:38:41');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (57, 15, 'SMS Setting', 'sms_setting', 1, 0, 1, 0, '2018-07-05 10:38:47');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (58, 15, 'Email Setting', 'email_setting', 1, 0, 1, 0, '2018-07-05 10:38:51');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (59, 15, 'Front CMS Setting', 'front_cms_setting', 1, 0, 1, 0, '2018-07-05 10:38:55');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (60, 15, 'Payment Methods', 'payment_methods', 1, 0, 1, 0, '2018-07-05 10:38:59');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (61, 16, 'Menus', 'menus', 1, 1, 0, 1, '2018-07-09 05:20:06');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (62, 16, 'Media Manager', 'media_manager', 1, 1, 0, 1, '2018-07-09 05:20:26');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (63, 16, 'Banner Images', 'banner_images', 1, 1, 0, 1, '2018-06-22 12:16:02');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (64, 16, 'Pages', 'pages', 1, 1, 1, 1, '2018-06-22 12:16:21');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (65, 16, 'Gallery', 'gallery', 1, 1, 1, 1, '2018-06-22 12:17:02');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (66, 16, 'Event', 'event', 1, 1, 1, 1, '2018-06-22 12:17:20');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (67, 16, 'News', 'notice', 1, 1, 1, 1, '2018-07-03 10:09:34');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (68, 2, 'Fees Group Assign', 'fees_group_assign', 1, 0, 0, 0, '2018-06-22 11:50:42');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (69, 2, 'Fees Type', 'fees_type', 1, 1, 1, 1, '2018-06-22 11:49:34');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (70, 2, 'Fees Discount', 'fees_discount', 1, 1, 1, 1, '2018-06-22 11:50:10');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (71, 2, 'Fees Discount Assign', 'fees_discount_assign', 1, 0, 0, 0, '2018-06-22 11:50:17');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (72, 2, 'Fees Statement', 'fees_statement', 1, 0, 0, 0, '2018-06-22 11:48:56');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (73, 2, 'Search Fees Payment', 'search_fees_payment', 1, 0, 0, 0, '2018-06-22 11:50:27');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (74, 2, 'Search Due Fees', 'search_due_fees', 1, 0, 0, 0, '2018-06-22 11:50:35');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (75, 2, 'Balance Fees Report', 'balance_fees_report', 1, 0, 0, 0, '2018-06-22 11:48:50');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (76, 6, 'Exam Schedule', 'exam_schedule', 1, 1, 1, 0, '2018-06-22 11:55:40');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (77, 7, 'Assign Class Teacher', 'assign_class_teacher', 1, 1, 1, 1, '2018-06-22 12:00:52');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (78, 17, 'Admission Enquiry', 'admission_enquiry', 1, 1, 1, 1, '2018-06-22 12:21:24');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (79, 17, 'Follow Up Admission Enquiry', 'follow_up_admission_enquiry', 1, 1, 0, 1, '2018-06-22 12:21:39');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (80, 17, 'Visitor Book', 'visitor_book', 1, 1, 1, 1, '2018-06-22 12:18:58');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (81, 17, 'Phone Call Log', 'phone_call_log', 1, 1, 1, 1, '2018-06-22 12:20:57');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (82, 17, 'Postal Dispatch', 'postal_dispatch', 1, 1, 1, 1, '2018-06-22 12:20:21');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (83, 17, 'Postal Receive', 'postal_receive', 1, 1, 1, 1, '2018-06-22 12:20:04');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (84, 17, 'Complain', 'complaint', 1, 1, 1, 1, '2018-07-03 10:10:55');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (85, 17, 'Setup Font Office', 'setup_font_office', 1, 1, 1, 1, '2018-06-22 12:19:24');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (86, 18, 'Staff', 'staff', 1, 1, 1, 1, '2018-06-22 12:23:31');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (87, 18, 'Disable Staff', 'disable_staff', 1, 0, 0, 0, '2018-06-22 12:23:12');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (88, 18, 'Staff Attendance', 'staff_attendance', 1, 1, 1, 0, '2018-06-22 12:23:10');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (89, 18, 'Staff Attendance Report', 'staff_attendance_report', 1, 0, 0, 0, '2018-06-22 12:22:54');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (90, 18, 'Staff Payroll', 'staff_payroll', 1, 1, 0, 1, '2018-06-22 12:22:51');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (91, 18, 'Payroll Report', 'payroll_report', 1, 0, 0, 0, '2018-06-22 12:22:34');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (93, 19, 'Homework', 'homework', 1, 1, 1, 1, '2018-06-22 12:23:50');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (94, 19, 'Homework Evaluation', 'homework_evaluation', 1, 1, 0, 0, '2018-06-27 04:37:21');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (95, 19, 'Homework Report', 'homework_report', 1, 0, 0, 0, '2018-06-22 12:23:54');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (96, 20, 'Student Certificate', 'student_certificate', 1, 1, 1, 1, '2018-07-06 12:11:07');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (97, 20, 'Generate Certificate', 'generate_certificate', 1, 0, 0, 0, '2018-07-06 12:07:16');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (98, 20, 'Student ID Card', 'student_id_card', 1, 1, 1, 1, '2018-07-06 12:11:28');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (99, 20, 'Generate ID Card', 'generate_id_card', 1, 0, 0, 0, '2018-07-06 12:11:49');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (102, 21, 'Calendar To Do List', 'calendar_to_do_list', 1, 1, 1, 1, '2018-06-22 12:24:41');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (104, 10, 'Item Category', 'item_category', 1, 1, 1, 1, '2018-06-22 12:04:33');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (105, 1, 'Student Parent Login Details', 'student_parent_login_details', 1, 0, 0, 0, '2018-06-22 11:48:01');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (106, 22, 'Quick Session Change', 'quick_session_change', 1, 0, 0, 0, '2018-06-22 12:24:45');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (107, 1, 'Disable Student', 'disable_student', 1, 0, 0, 0, '2018-06-25 07:51:34');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (108, 18, ' Approve Leave Request', 'approve_leave_request', 1, 1, 1, 1, '2018-07-02 11:47:41');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (109, 18, 'Apply Leave', 'apply_leave', 1, 1, 1, 1, '2018-06-26 05:23:32');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (110, 18, 'Leave Types ', 'leave_types', 1, 1, 1, 1, '2018-07-02 11:47:56');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (111, 18, 'Department', 'department', 1, 1, 1, 1, '2018-06-26 05:27:07');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (112, 18, 'Designation', 'designation', 1, 1, 1, 1, '2018-06-26 05:27:07');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (113, 22, 'Fees Collection And Expense Monthly Chart', 'fees_collection_and_expense_monthly_chart', 1, 0, 0, 0, '2018-07-03 08:38:15');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (114, 22, 'Fees Collection And Expense Yearly Chart', 'fees_collection_and_expense_yearly_chart', 1, 0, 0, 0, '2018-07-03 08:38:15');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (115, 22, 'Monthly Fees Collection Widget', 'Monthly fees_collection_widget', 1, 0, 0, 0, '2018-07-03 08:43:35');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (116, 22, 'Monthly Expense Widget', 'monthly_expense_widget', 1, 0, 0, 0, '2018-07-03 08:43:35');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (117, 22, 'Student Count Widget', 'student_count_widget', 1, 0, 0, 0, '2018-07-03 08:43:35');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (118, 22, 'Staff Role Count Widget', 'staff_role_count_widget', 1, 0, 0, 0, '2018-07-03 08:43:35');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (119, 1, 'Guardian Report', 'guardian_report', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (120, 1, 'Student History', 'student_history', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (121, 1, 'Student Login Credential', 'student_login_credential', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (122, 5, 'Attendance By Date', 'attendance_by_date', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (123, 9, 'Add Student', 'add_student', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (124, 11, 'Student Transport Report', 'student_transport_report', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (125, 12, 'Student Hostel Report', 'student_hostel_report', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (126, 15, 'User Status', 'user_status', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (127, 18, 'Can See Other Users Profile', 'can_see_other_users_profile', 1, 0, 0, 0, '2018-07-03 10:12:29');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (128, 1, 'Student Timeline', 'student_timeline', 0, 1, 0, 1, '2018-07-05 09:38:52');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (129, 18, 'Staff Timeline', 'staff_timeline', 0, 1, 0, 1, '2018-07-05 09:38:52');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (130, 15, 'Backup', 'backup', 1, 1, 0, 1, '2018-07-09 05:47:17');
INSERT INTO `permission_category` (`id`, `perm_group_id`, `name`, `short_code`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `created_at`) VALUES (131, 15, 'Restore', 'restore', 1, 0, 0, 0, '2018-07-09 05:47:17');


#
# TABLE STRUCTURE FOR: permission_group
#

DROP TABLE IF EXISTS `permission_group`;

CREATE TABLE `permission_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `short_code` varchar(100) NOT NULL,
  `is_active` int(11) DEFAULT 0,
  `system` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (1, 'Student Information', 'student_information', 1, 1, '2018-06-27 05:09:31');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (2, 'Fees Collection', 'fees_collection', 1, 0, '2018-07-11 04:19:10');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (3, 'Income', 'income', 1, 0, '2018-06-27 02:19:05');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (4, 'Expense', 'expense', 1, 0, '2018-07-04 03:07:33');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (5, 'Student Attendance', 'student_attendance', 1, 0, '2018-07-02 09:18:08');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (6, 'Examination', 'examination', 1, 0, '2018-07-11 04:19:08');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (7, 'Academics', 'academics', 1, 1, '2018-07-02 08:55:43');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (8, 'Download Center', 'download_center', 1, 0, '2018-07-02 09:19:29');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (9, 'Library', 'library', 1, 0, '2018-06-28 12:43:14');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (10, 'Inventory', 'inventory', 1, 0, '2018-06-27 02:18:58');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (11, 'Transport', 'transport', 1, 0, '2018-06-27 09:21:26');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (12, 'Hostel', 'hostel', 1, 0, '2018-07-02 09:19:32');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (13, 'Communicate', 'communicate', 1, 0, '2018-07-02 09:20:00');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (14, 'Reports', 'reports', 1, 1, '2018-06-27 05:10:22');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (15, 'System Settings', 'system_settings', 1, 1, '2018-06-27 05:10:28');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (16, 'Front CMS', 'front_cms', 1, 0, '2018-07-10 06:46:54');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (17, 'Front Office', 'front_office', 1, 0, '2018-06-27 05:15:30');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (18, 'Human Resource', 'human_resource', 1, 1, '2018-06-27 05:11:02');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (19, 'Homework', 'homework', 1, 0, '2018-06-27 02:19:38');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (20, 'Certificate', 'certificate', 1, 0, '2018-06-27 09:21:29');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (21, 'Calendar To Do List', 'calendar_to_do_list', 1, 0, '2018-06-27 05:12:25');
INSERT INTO `permission_group` (`id`, `name`, `short_code`, `is_active`, `system`, `created_at`) VALUES (22, 'Dashboard and Widgets', 'dashboard_and_widgets', 1, 1, '2018-06-27 05:11:17');


#
# TABLE STRUCTURE FOR: read_notification
#

DROP TABLE IF EXISTS `read_notification`;

CREATE TABLE `read_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `notification_id` int(11) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: reference
#

DROP TABLE IF EXISTS `reference`;

CREATE TABLE `reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: roles
#

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `is_active` int(11) DEFAULT 0,
  `is_system` int(1) NOT NULL DEFAULT 0,
  `is_superadmin` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO `roles` (`id`, `name`, `slug`, `is_active`, `is_system`, `is_superadmin`, `created_at`, `updated_at`) VALUES (1, 'Admin', NULL, 0, 1, 0, '2018-06-30 11:39:11', '0000-00-00 00:00:00');
INSERT INTO `roles` (`id`, `name`, `slug`, `is_active`, `is_system`, `is_superadmin`, `created_at`, `updated_at`) VALUES (2, 'Teacher', NULL, 0, 1, 0, '2018-06-30 11:39:14', '0000-00-00 00:00:00');
INSERT INTO `roles` (`id`, `name`, `slug`, `is_active`, `is_system`, `is_superadmin`, `created_at`, `updated_at`) VALUES (3, 'Accountant', NULL, 0, 1, 0, '2018-06-30 11:39:17', '0000-00-00 00:00:00');
INSERT INTO `roles` (`id`, `name`, `slug`, `is_active`, `is_system`, `is_superadmin`, `created_at`, `updated_at`) VALUES (4, 'Librarian', NULL, 0, 1, 0, '2018-06-30 11:39:21', '0000-00-00 00:00:00');
INSERT INTO `roles` (`id`, `name`, `slug`, `is_active`, `is_system`, `is_superadmin`, `created_at`, `updated_at`) VALUES (6, 'Receptionist', NULL, 0, 1, 0, '2018-07-02 01:39:03', '0000-00-00 00:00:00');
INSERT INTO `roles` (`id`, `name`, `slug`, `is_active`, `is_system`, `is_superadmin`, `created_at`, `updated_at`) VALUES (7, 'Super Admin', NULL, 0, 1, 1, '2018-07-11 10:11:29', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: roles_permissions
#

DROP TABLE IF EXISTS `roles_permissions`;

CREATE TABLE `roles_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `perm_cat_id` int(11) DEFAULT NULL,
  `can_view` int(11) DEFAULT NULL,
  `can_add` int(11) DEFAULT NULL,
  `can_edit` int(11) DEFAULT NULL,
  `can_delete` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=637 DEFAULT CHARSET=utf8;

INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (3, 1, 3, 1, 1, 1, 1, '2018-07-06 11:12:08');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (4, 1, 4, 1, 1, 1, 1, '2018-07-06 11:13:01');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (6, 1, 5, 1, 1, 0, 1, '2018-07-02 12:49:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (8, 1, 7, 1, 1, 1, 1, '2018-07-06 11:13:29');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (9, 1, 8, 1, 1, 1, 1, '2018-07-06 11:13:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (10, 1, 17, 1, 1, 1, 1, '2018-07-06 11:18:56');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (11, 1, 78, 1, 1, 1, 1, '2018-07-03 02:19:43');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (13, 1, 69, 1, 1, 1, 1, '2018-07-06 11:14:15');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (14, 1, 70, 1, 1, 1, 1, '2018-07-06 11:14:39');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (23, 1, 12, 1, 1, 1, 1, '2018-07-06 11:15:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (24, 1, 13, 1, 1, 1, 1, '2018-07-06 11:18:28');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (26, 1, 15, 1, 1, 1, 0, '2018-07-02 12:54:21');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (28, 1, 19, 1, 1, 1, 0, '2018-07-02 13:01:10');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (29, 1, 20, 1, 1, 1, 1, '2018-07-06 11:19:50');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (30, 1, 76, 1, 1, 1, 0, '2018-07-02 13:01:10');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (31, 1, 21, 1, 1, 1, 0, '2018-07-02 13:01:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (32, 1, 22, 1, 1, 1, 1, '2018-07-02 13:02:05');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (33, 1, 23, 1, 1, 1, 1, '2018-07-06 11:20:17');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (34, 1, 24, 1, 1, 1, 1, '2018-07-06 11:20:39');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (35, 1, 25, 1, 1, 1, 1, '2018-07-06 11:22:35');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (37, 1, 77, 1, 1, 1, 1, '2018-07-06 11:19:50');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (43, 1, 32, 1, 1, 1, 1, '2018-07-06 11:52:05');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (44, 1, 33, 1, 1, 1, 1, '2018-07-06 11:52:29');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (45, 1, 34, 1, 1, 1, 1, '2018-07-06 11:53:59');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (46, 1, 35, 1, 1, 1, 1, '2018-07-06 11:54:34');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (47, 1, 104, 1, 1, 1, 1, '2018-07-06 11:53:08');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (48, 1, 37, 1, 1, 1, 1, '2018-07-06 11:55:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (49, 1, 38, 1, 1, 1, 1, '2018-07-09 06:45:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (53, 1, 43, 1, 1, 1, 1, '2018-07-10 11:00:31');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (58, 1, 52, 1, 1, 0, 1, '2018-07-09 04:49:43');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (61, 1, 55, 1, 1, 1, 1, '2018-07-02 10:54:16');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (67, 1, 61, 1, 1, 0, 1, '2018-07-09 07:29:19');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (68, 1, 62, 1, 1, 0, 1, '2018-07-09 07:29:19');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (69, 1, 63, 1, 1, 0, 1, '2018-07-09 05:21:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (70, 1, 64, 1, 1, 1, 1, '2018-07-09 04:32:19');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (71, 1, 65, 1, 1, 1, 1, '2018-07-09 04:41:21');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (72, 1, 66, 1, 1, 1, 1, '2018-07-09 04:43:09');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (73, 1, 67, 1, 1, 1, 1, '2018-07-09 04:44:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (74, 1, 79, 1, 1, 0, 1, '2018-07-02 13:34:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (75, 1, 80, 1, 1, 1, 1, '2018-07-06 11:11:23');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (76, 1, 81, 1, 1, 1, 1, '2018-07-06 11:11:23');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (78, 1, 83, 1, 1, 1, 1, '2018-07-06 11:11:23');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (79, 1, 84, 1, 1, 1, 1, '2018-07-06 11:11:23');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (80, 1, 85, 1, 1, 1, 1, '2018-07-12 01:46:00');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (83, 1, 88, 1, 1, 1, 0, '2018-07-03 13:34:20');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (87, 1, 92, 1, 1, 1, 1, '2018-06-26 05:03:43');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (88, 1, 93, 1, 1, 1, 1, '2018-07-09 02:54:20');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (94, 1, 82, 1, 1, 1, 1, '2018-07-06 11:11:23');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (120, 1, 39, 1, 1, 1, 1, '2018-07-06 11:56:28');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (140, 1, 110, 1, 1, 1, 1, '2018-07-06 11:25:08');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (141, 1, 111, 1, 1, 1, 1, '2018-07-06 11:26:28');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (142, 1, 112, 1, 1, 1, 1, '2018-07-06 11:26:28');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (145, 1, 94, 1, 1, 0, 0, '2018-07-09 02:50:40');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (147, 2, 43, 1, 1, 1, 1, '2018-06-30 09:16:24');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (148, 2, 44, 1, 0, 0, 0, '2018-06-27 12:47:09');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (149, 2, 46, 1, 0, 0, 0, '2018-06-28 01:56:41');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (156, 1, 9, 1, 1, 1, 1, '2018-07-06 11:14:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (157, 1, 10, 1, 1, 1, 1, '2018-07-06 11:15:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (159, 1, 40, 1, 1, 1, 1, '2018-07-09 06:39:40');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (160, 1, 41, 1, 1, 1, 1, '2018-07-06 11:57:09');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (161, 1, 42, 1, 1, 1, 1, '2018-07-09 06:43:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (169, 1, 27, 1, 1, 0, 1, '2018-07-02 13:06:58');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (178, 1, 54, 1, 0, 1, 0, '2018-07-05 10:39:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (179, 1, 56, 1, 0, 1, 0, '2018-07-05 10:39:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (180, 1, 57, 1, 0, 1, 0, '2018-07-05 10:39:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (181, 1, 58, 1, 0, 1, 0, '2018-07-05 10:39:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (182, 1, 59, 1, 0, 1, 0, '2018-07-05 10:39:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (183, 1, 60, 1, 0, 1, 0, '2018-07-05 10:39:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (190, 1, 105, 1, 0, 0, 0, '2018-07-02 12:43:25');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (193, 1, 6, 1, 0, 0, 0, '2018-07-02 12:49:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (194, 1, 68, 1, 0, 0, 0, '2018-07-02 12:49:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (196, 1, 72, 1, 0, 0, 0, '2018-07-02 12:49:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (197, 1, 73, 1, 0, 0, 0, '2018-07-02 12:49:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (198, 1, 74, 1, 0, 0, 0, '2018-07-02 12:49:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (199, 1, 75, 1, 0, 0, 0, '2018-07-02 12:49:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (201, 1, 14, 1, 0, 0, 0, '2018-07-02 12:52:03');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (203, 1, 16, 1, 0, 0, 0, '2018-07-02 12:54:21');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (204, 1, 26, 1, 0, 0, 0, '2018-07-02 13:02:05');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (206, 1, 29, 1, 0, 0, 0, '2018-07-02 13:13:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (207, 1, 30, 1, 0, 0, 0, '2018-07-02 13:13:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (208, 1, 31, 1, 0, 0, 0, '2018-07-02 13:15:36');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (215, 1, 50, 1, 0, 0, 0, '2018-07-02 13:34:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (216, 1, 51, 1, 0, 0, 0, '2018-07-02 13:34:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (222, 1, 1, 1, 1, 1, 1, '2018-07-10 11:00:31');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (225, 1, 108, 1, 1, 1, 1, '2018-07-09 03:47:26');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (227, 1, 91, 1, 0, 0, 0, '2018-07-03 03:19:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (229, 1, 89, 1, 0, 0, 0, '2018-07-03 03:30:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (230, 10, 53, 0, 1, 0, 0, '2018-07-03 05:22:55');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (231, 10, 54, 0, 0, 1, 0, '2018-07-03 05:22:55');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (232, 10, 55, 1, 1, 1, 1, '2018-07-03 05:28:42');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (233, 10, 56, 0, 0, 1, 0, '2018-07-03 05:22:55');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (235, 10, 58, 0, 0, 1, 0, '2018-07-03 05:22:55');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (236, 10, 59, 0, 0, 1, 0, '2018-07-03 05:22:55');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (239, 10, 1, 1, 1, 1, 1, '2018-07-03 05:46:43');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (241, 10, 3, 1, 0, 0, 0, '2018-07-03 05:53:56');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (242, 10, 2, 1, 0, 0, 0, '2018-07-03 05:54:39');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (243, 10, 4, 1, 0, 1, 1, '2018-07-03 06:01:24');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (245, 10, 107, 1, 0, 0, 0, '2018-07-03 06:06:41');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (246, 10, 5, 1, 1, 0, 1, '2018-07-03 06:08:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (247, 10, 7, 1, 1, 1, 1, '2018-07-03 06:12:07');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (248, 10, 68, 1, 0, 0, 0, '2018-07-03 06:12:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (249, 10, 69, 1, 1, 1, 1, '2018-07-03 06:19:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (250, 10, 70, 1, 0, 0, 1, '2018-07-03 06:22:40');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (251, 10, 72, 1, 0, 0, 0, '2018-07-03 06:26:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (252, 10, 73, 1, 0, 0, 0, '2018-07-03 06:26:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (253, 10, 74, 1, 0, 0, 0, '2018-07-03 06:28:34');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (254, 10, 75, 1, 0, 0, 0, '2018-07-03 06:28:34');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (255, 10, 9, 1, 1, 1, 1, '2018-07-03 06:32:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (256, 10, 10, 1, 1, 1, 1, '2018-07-03 06:33:09');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (257, 10, 11, 1, 0, 0, 0, '2018-07-03 06:33:09');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (258, 10, 12, 1, 1, 1, 1, '2018-07-03 06:38:40');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (259, 10, 13, 1, 1, 1, 1, '2018-07-03 06:38:40');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (260, 10, 14, 1, 0, 0, 0, '2018-07-03 06:38:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (261, 10, 15, 1, 1, 1, 0, '2018-07-03 06:41:28');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (262, 10, 16, 1, 0, 0, 0, '2018-07-03 06:42:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (263, 10, 17, 1, 1, 1, 1, '2018-07-03 06:44:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (264, 10, 19, 1, 1, 1, 0, '2018-07-03 06:45:45');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (265, 10, 20, 1, 1, 1, 1, '2018-07-03 06:48:51');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (266, 10, 76, 1, 0, 0, 0, '2018-07-03 06:51:21');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (267, 10, 21, 1, 1, 1, 0, '2018-07-03 06:52:45');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (268, 10, 22, 1, 1, 1, 1, '2018-07-03 06:55:00');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (269, 10, 23, 1, 1, 1, 1, '2018-07-03 06:57:16');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (270, 10, 24, 1, 1, 1, 1, '2018-07-03 06:57:49');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (271, 10, 25, 1, 1, 1, 1, '2018-07-03 06:57:49');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (272, 10, 26, 1, 0, 0, 0, '2018-07-03 06:58:25');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (273, 10, 77, 1, 1, 1, 1, '2018-07-03 06:59:57');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (274, 10, 27, 1, 1, 0, 1, '2018-07-03 07:00:36');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (275, 10, 28, 1, 1, 1, 1, '2018-07-03 07:03:09');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (276, 10, 29, 1, 0, 0, 0, '2018-07-03 07:04:03');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (277, 10, 30, 1, 0, 0, 0, '2018-07-03 07:04:03');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (278, 10, 31, 1, 0, 0, 0, '2018-07-03 07:04:03');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (279, 10, 32, 1, 1, 1, 1, '2018-07-03 07:05:42');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (280, 10, 33, 1, 1, 1, 1, '2018-07-03 07:06:32');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (281, 10, 34, 1, 1, 1, 1, '2018-07-03 07:08:03');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (282, 10, 35, 1, 1, 1, 1, '2018-07-03 07:08:41');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (283, 10, 104, 1, 1, 1, 1, '2018-07-03 07:10:43');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (284, 10, 37, 1, 1, 1, 1, '2018-07-03 07:12:42');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (285, 10, 38, 1, 1, 1, 1, '2018-07-03 07:13:56');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (286, 10, 39, 1, 1, 1, 1, '2018-07-03 07:15:39');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (287, 10, 40, 1, 1, 1, 1, '2018-07-03 07:17:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (288, 10, 41, 1, 1, 1, 1, '2018-07-03 07:18:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (289, 10, 42, 1, 1, 1, 1, '2018-07-03 07:19:31');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (290, 10, 43, 1, 1, 1, 1, '2018-07-03 07:21:15');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (291, 10, 44, 1, 0, 0, 0, '2018-07-03 07:22:06');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (292, 10, 46, 1, 0, 0, 0, '2018-07-03 07:22:06');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (293, 10, 50, 1, 0, 0, 0, '2018-07-03 07:22:59');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (294, 10, 51, 1, 0, 0, 0, '2018-07-03 07:22:59');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (295, 10, 60, 0, 0, 1, 0, '2018-07-03 07:25:05');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (296, 10, 61, 1, 1, 1, 1, '2018-07-03 07:26:52');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (297, 10, 62, 1, 1, 1, 1, '2018-07-03 07:28:53');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (298, 10, 63, 1, 1, 0, 0, '2018-07-03 07:29:37');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (299, 10, 64, 1, 1, 1, 1, '2018-07-03 07:30:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (300, 10, 65, 1, 1, 1, 1, '2018-07-03 07:32:51');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (301, 10, 66, 1, 1, 1, 1, '2018-07-03 07:32:51');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (302, 10, 67, 1, 0, 0, 0, '2018-07-03 07:32:51');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (303, 10, 78, 1, 1, 1, 1, '2018-07-04 05:40:04');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (307, 1, 126, 1, 0, 0, 0, '2018-07-03 10:56:13');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (310, 1, 119, 1, 0, 0, 0, '2018-07-03 11:45:00');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (311, 1, 120, 1, 0, 0, 0, '2018-07-03 11:45:00');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (312, 1, 107, 1, 0, 0, 0, '2018-07-03 11:45:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (313, 1, 122, 1, 0, 0, 0, '2018-07-03 11:49:37');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (315, 1, 123, 1, 0, 0, 0, '2018-07-03 11:57:03');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (317, 1, 124, 1, 0, 0, 0, '2018-07-03 11:59:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (320, 1, 47, 1, 0, 0, 0, '2018-07-03 12:31:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (321, 1, 121, 1, 0, 0, 0, '2018-07-03 12:31:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (322, 1, 109, 1, 1, 1, 1, '2018-07-03 12:40:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (369, 1, 102, 1, 1, 1, 1, '2018-07-11 13:31:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (372, 10, 79, 1, 1, 0, 0, '2018-07-04 05:40:04');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (373, 10, 80, 1, 1, 1, 1, '2018-07-04 05:53:09');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (374, 10, 81, 1, 1, 1, 1, '2018-07-04 05:53:50');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (375, 10, 82, 1, 1, 1, 1, '2018-07-04 05:56:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (376, 10, 83, 1, 1, 1, 1, '2018-07-04 05:57:55');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (377, 10, 84, 1, 1, 1, 1, '2018-07-04 06:00:26');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (378, 10, 85, 1, 1, 1, 1, '2018-07-04 06:02:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (379, 10, 86, 1, 1, 1, 1, '2018-07-04 06:16:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (380, 10, 87, 1, 0, 0, 0, '2018-07-04 06:19:49');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (381, 10, 88, 1, 1, 1, 0, '2018-07-04 06:21:20');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (382, 10, 89, 1, 0, 0, 0, '2018-07-04 06:21:51');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (383, 10, 90, 1, 1, 0, 1, '2018-07-04 06:25:01');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (384, 10, 91, 1, 0, 0, 0, '2018-07-04 06:25:01');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (385, 10, 108, 1, 1, 1, 1, '2018-07-04 06:27:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (386, 10, 109, 1, 1, 1, 1, '2018-07-04 06:28:26');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (387, 10, 110, 1, 1, 1, 1, '2018-07-04 06:32:43');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (388, 10, 111, 1, 1, 1, 1, '2018-07-04 06:33:21');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (389, 10, 112, 1, 1, 1, 1, '2018-07-04 06:35:06');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (390, 10, 127, 1, 0, 0, 0, '2018-07-04 06:35:06');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (391, 10, 93, 1, 1, 1, 1, '2018-07-04 06:37:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (392, 10, 94, 1, 1, 0, 0, '2018-07-04 06:38:02');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (394, 10, 95, 1, 0, 0, 0, '2018-07-04 06:38:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (395, 10, 102, 1, 1, 1, 1, '2018-07-04 06:41:02');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (396, 10, 106, 1, 0, 0, 0, '2018-07-04 06:41:39');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (397, 10, 113, 1, 0, 0, 0, '2018-07-04 06:42:37');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (398, 10, 114, 1, 0, 0, 0, '2018-07-04 06:42:37');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (399, 10, 115, 1, 0, 0, 0, '2018-07-04 06:48:45');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (400, 10, 116, 1, 0, 0, 0, '2018-07-04 06:48:45');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (401, 10, 117, 1, 0, 0, 0, '2018-07-04 06:49:43');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (402, 10, 118, 1, 0, 0, 0, '2018-07-04 06:49:43');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (411, 1, 2, 1, 0, 0, 0, '2018-07-04 09:46:10');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (412, 1, 11, 1, 0, 0, 0, '2018-07-04 10:24:05');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (416, 2, 3, 1, 1, 1, 1, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (428, 2, 4, 1, 1, 1, 1, '2018-07-05 03:40:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (432, 1, 128, 0, 1, 0, 1, '2018-07-05 09:39:50');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (434, 1, 125, 1, 0, 0, 0, '2018-07-06 11:29:26');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (435, 1, 96, 1, 1, 1, 1, '2018-07-09 02:33:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (437, 1, 98, 1, 1, 1, 1, '2018-07-09 02:44:17');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (444, 1, 99, 1, 0, 0, 0, '2018-07-06 13:11:22');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (445, 1, 48, 1, 0, 0, 0, '2018-07-06 13:19:35');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (446, 1, 49, 1, 0, 0, 0, '2018-07-06 13:19:35');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (448, 1, 71, 1, 0, 0, 0, '2018-07-08 05:17:06');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (453, 1, 106, 1, 0, 0, 0, '2018-07-09 02:17:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (454, 1, 113, 1, 0, 0, 0, '2018-07-09 02:17:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (455, 1, 114, 1, 0, 0, 0, '2018-07-09 02:17:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (456, 1, 115, 1, 0, 0, 0, '2018-07-09 02:17:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (457, 1, 116, 1, 0, 0, 0, '2018-07-09 02:17:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (458, 1, 117, 1, 0, 0, 0, '2018-07-09 02:17:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (459, 1, 118, 1, 0, 0, 0, '2018-07-09 02:17:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (461, 1, 97, 1, 0, 0, 0, '2018-07-09 02:30:16');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (462, 1, 95, 1, 0, 0, 0, '2018-07-09 02:48:41');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (464, 1, 86, 1, 1, 1, 1, '2018-07-09 07:39:48');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (466, 1, 129, 0, 1, 0, 1, '2018-07-09 03:09:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (467, 1, 87, 1, 0, 0, 0, '2018-07-09 03:11:59');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (468, 1, 90, 1, 1, 0, 1, '2018-07-09 03:22:50');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (471, 1, 53, 0, 1, 0, 0, '2018-07-09 04:50:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (474, 1, 130, 1, 1, 0, 1, '2018-07-09 12:26:36');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (476, 1, 131, 1, 0, 0, 0, '2018-07-09 06:23:32');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (477, 2, 1, 1, 1, 1, 1, '2018-07-11 08:26:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (478, 2, 2, 1, 0, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (479, 2, 47, 1, 0, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (480, 2, 105, 1, 0, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (482, 2, 119, 1, 0, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (483, 2, 120, 1, 0, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (485, 2, 15, 1, 1, 1, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (486, 2, 16, 1, 0, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (487, 2, 122, 1, 0, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (492, 2, 21, 1, 0, 0, 0, '2018-07-12 01:50:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (493, 2, 22, 1, 0, 0, 0, '2018-07-12 01:50:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (494, 2, 23, 1, 0, 0, 0, '2018-07-12 01:50:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (495, 2, 24, 1, 0, 0, 0, '2018-07-12 01:50:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (496, 2, 25, 1, 0, 0, 0, '2018-07-12 01:50:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (498, 2, 77, 1, 0, 0, 0, '2018-07-12 01:50:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (499, 2, 27, 1, 1, 0, 1, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (502, 2, 93, 1, 1, 1, 1, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (503, 2, 94, 1, 1, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (504, 2, 95, 1, 0, 0, 0, '2018-07-10 08:17:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (505, 3, 5, 1, 1, 0, 1, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (506, 3, 6, 1, 0, 0, 0, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (507, 3, 7, 1, 1, 1, 1, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (508, 3, 8, 1, 1, 1, 1, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (509, 3, 68, 1, 0, 0, 0, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (510, 3, 69, 1, 1, 1, 1, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (511, 3, 70, 1, 1, 1, 1, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (512, 3, 71, 1, 0, 0, 0, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (513, 3, 72, 1, 0, 0, 0, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (514, 3, 73, 1, 0, 0, 0, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (515, 3, 74, 1, 0, 0, 0, '2018-07-10 08:37:30');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (517, 3, 75, 1, 0, 0, 0, '2018-07-10 08:40:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (518, 3, 9, 1, 1, 1, 1, '2018-07-10 08:40:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (519, 3, 10, 1, 1, 1, 1, '2018-07-10 08:40:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (520, 3, 11, 1, 0, 0, 0, '2018-07-10 08:40:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (521, 3, 12, 1, 1, 1, 1, '2018-07-10 08:47:00');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (522, 3, 13, 1, 1, 1, 1, '2018-07-10 08:47:00');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (523, 3, 14, 1, 0, 0, 0, '2018-07-10 08:47:00');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (524, 3, 86, 1, 1, 1, 1, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (525, 3, 87, 1, 0, 0, 0, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (526, 3, 88, 1, 1, 1, 0, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (527, 3, 89, 1, 0, 0, 0, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (528, 3, 90, 1, 1, 0, 1, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (529, 3, 91, 1, 0, 0, 0, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (530, 3, 108, 1, 1, 1, 1, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (531, 3, 109, 1, 1, 1, 1, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (532, 3, 110, 1, 1, 1, 1, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (533, 3, 111, 1, 1, 1, 1, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (534, 3, 112, 1, 1, 1, 1, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (535, 3, 127, 1, 0, 0, 0, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (536, 3, 129, 0, 1, 0, 1, '2018-07-10 08:48:44');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (537, 3, 43, 1, 1, 1, 1, '2018-07-10 08:49:49');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (538, 3, 44, 1, 0, 0, 0, '2018-07-10 08:49:49');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (539, 3, 46, 1, 0, 0, 0, '2018-07-10 08:49:49');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (540, 3, 31, 1, 0, 0, 0, '2018-07-10 08:50:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (541, 3, 32, 1, 1, 1, 1, '2018-07-10 08:50:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (542, 3, 33, 1, 1, 1, 1, '2018-07-10 08:50:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (543, 3, 34, 1, 1, 1, 1, '2018-07-10 08:50:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (544, 3, 35, 1, 1, 1, 1, '2018-07-10 08:50:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (545, 3, 104, 1, 1, 1, 1, '2018-07-10 08:50:38');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (546, 3, 37, 1, 1, 1, 1, '2018-07-10 08:52:17');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (547, 3, 38, 1, 1, 1, 1, '2018-07-10 08:52:17');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (548, 3, 39, 1, 1, 1, 1, '2018-07-10 08:52:17');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (549, 3, 124, 1, 0, 0, 0, '2018-07-10 08:52:17');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (553, 6, 78, 1, 1, 1, 1, '2018-07-10 09:02:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (554, 6, 79, 1, 1, 0, 1, '2018-07-10 09:02:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (555, 6, 80, 1, 1, 1, 1, '2018-07-10 09:02:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (556, 6, 81, 1, 1, 1, 1, '2018-07-10 09:02:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (557, 6, 82, 1, 1, 1, 1, '2018-07-10 09:02:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (558, 6, 83, 1, 1, 1, 1, '2018-07-10 09:02:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (559, 6, 84, 1, 1, 1, 1, '2018-07-10 09:02:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (560, 6, 85, 1, 1, 1, 1, '2018-07-10 09:02:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (561, 6, 86, 1, 0, 0, 0, '2018-07-10 09:11:10');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (574, 6, 43, 1, 1, 1, 1, '2018-07-10 09:05:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (575, 6, 44, 1, 0, 0, 0, '2018-07-10 09:05:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (576, 6, 46, 1, 0, 0, 0, '2018-07-10 09:05:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (578, 6, 102, 1, 1, 1, 1, '2018-07-10 09:20:33');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (579, 4, 28, 1, 1, 1, 1, '2018-07-10 09:23:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (580, 4, 29, 1, 0, 0, 0, '2018-07-10 09:23:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (581, 4, 30, 1, 0, 0, 0, '2018-07-10 09:23:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (582, 4, 123, 1, 0, 0, 0, '2018-07-10 09:23:54');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (583, 4, 86, 1, 0, 0, 0, '2018-07-10 09:24:13');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (584, 4, 43, 1, 1, 1, 1, '2018-07-10 09:25:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (585, 4, 44, 1, 0, 0, 0, '2018-07-10 09:25:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (586, 4, 46, 1, 0, 0, 0, '2018-07-10 09:25:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (588, 2, 102, 1, 1, 1, 1, '2018-07-12 01:47:45');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (589, 2, 106, 1, 0, 0, 0, '2018-07-10 09:25:37');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (590, 2, 117, 1, 0, 0, 0, '2018-07-10 09:25:37');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (591, 3, 40, 1, 1, 1, 1, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (592, 3, 41, 1, 1, 1, 1, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (593, 3, 42, 1, 1, 1, 1, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (594, 3, 125, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (595, 3, 48, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (596, 3, 49, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (597, 3, 102, 1, 1, 1, 1, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (598, 3, 106, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (599, 3, 113, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (600, 3, 114, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (601, 3, 115, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (602, 3, 116, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (603, 3, 117, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (604, 3, 118, 1, 0, 0, 0, '2018-07-10 09:28:12');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (609, 6, 117, 1, 0, 0, 0, '2018-07-10 09:30:48');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (611, 2, 86, 1, 0, 0, 0, '2018-07-10 09:38:49');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (612, 1, 44, 1, 0, 0, 0, '2018-07-10 11:00:31');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (613, 1, 46, 1, 0, 0, 0, '2018-07-10 11:00:31');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (616, 1, 127, 1, 0, 0, 0, '2018-07-11 04:22:46');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (617, 2, 17, 1, 1, 1, 1, '2018-07-11 08:25:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (618, 2, 19, 1, 1, 1, 0, '2018-07-11 08:25:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (619, 2, 20, 1, 1, 1, 1, '2018-07-11 08:25:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (620, 2, 76, 1, 1, 1, 0, '2018-07-11 08:25:14');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (621, 2, 107, 1, 0, 0, 0, '2018-07-11 08:26:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (622, 2, 121, 1, 0, 0, 0, '2018-07-11 08:26:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (623, 2, 128, 0, 1, 0, 1, '2018-07-11 08:26:27');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (625, 1, 28, 1, 1, 1, 1, '2018-07-11 10:57:18');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (626, 6, 1, 1, 0, 0, 0, '2018-07-12 01:53:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (627, 6, 21, 1, 0, 0, 0, '2018-07-12 01:53:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (628, 6, 22, 1, 0, 0, 0, '2018-07-12 01:53:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (629, 6, 23, 1, 0, 0, 0, '2018-07-12 01:53:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (630, 6, 24, 1, 0, 0, 0, '2018-07-12 01:53:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (631, 6, 25, 1, 0, 0, 0, '2018-07-12 01:53:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (632, 6, 77, 1, 0, 0, 0, '2018-07-12 01:53:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (633, 6, 106, 1, 0, 0, 0, '2018-07-12 01:53:47');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (634, 4, 102, 1, 1, 1, 1, '2018-07-12 01:54:23');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (635, 4, 106, 1, 0, 0, 0, '2018-07-12 01:54:23');
INSERT INTO `roles_permissions` (`id`, `role_id`, `perm_cat_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`) VALUES (636, 4, 117, 1, 0, 0, 0, '2018-07-12 01:54:23');


#
# TABLE STRUCTURE FOR: room_types
#

DROP TABLE IF EXISTS `room_types`;

CREATE TABLE `room_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type` varchar(200) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sch_settings
#

DROP TABLE IF EXISTS `sch_settings`;

CREATE TABLE `sch_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `dise_code` varchar(50) DEFAULT NULL,
  `date_format` varchar(50) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `currency_symbol` varchar(50) NOT NULL,
  `is_rtl` varchar(10) DEFAULT 'disabled',
  `timezone` varchar(30) DEFAULT 'UTC',
  `session_id` int(11) DEFAULT NULL,
  `start_month` varchar(40) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `theme` varchar(200) NOT NULL DEFAULT 'default.jpg',
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cron_secret_key` varchar(100) NOT NULL,
  `fee_due_days` int(3) DEFAULT 0,
  `class_teacher` varchar(100) NOT NULL,
  `mobile_api_url` text NOT NULL,
  `app_primary_color_code` varchar(20) DEFAULT NULL,
  `app_secondary_color_code` varchar(20) DEFAULT NULL,
  `app_logo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lang_id` (`lang_id`),
  KEY `session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sch_settings` (`id`, `name`, `email`, `phone`, `address`, `lang_id`, `dise_code`, `date_format`, `currency`, `currency_symbol`, `is_rtl`, `timezone`, `session_id`, `start_month`, `image`, `theme`, `is_active`, `created_at`, `updated_at`, `cron_secret_key`, `fee_due_days`, `class_teacher`, `mobile_api_url`, `app_primary_color_code`, `app_secondary_color_code`, `app_logo`) VALUES (1, 'Saraswati International School', 'saraswatiinternational12@gmail.com', '70650 07645', 'SIS, Sec-74, Begumpur Khatola, Gurgaon - 122001', 4, 'SIS01', 'm/d/Y', 'INR', '₹', 'disabled', 'Asia/Kolkata', 14, '4', '1.png', 'default.jpg', 'no', '2019-12-15 06:19:12', '0000-00-00 00:00:00', '', 60, 'no', 'http://sis.gurukule.com/jwsapi/', '#424242', '#eeeeee', '1.png');


#
# TABLE STRUCTURE FOR: school_houses
#

DROP TABLE IF EXISTS `school_houses`;

CREATE TABLE `school_houses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `house_name` varchar(200) NOT NULL,
  `description` varchar(400) NOT NULL,
  `is_active` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sections
#

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(60) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: send_notification
#

DROP TABLE IF EXISTS `send_notification`;

CREATE TABLE `send_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `message` text DEFAULT NULL,
  `visible_student` varchar(10) NOT NULL DEFAULT 'no',
  `visible_staff` varchar(10) NOT NULL DEFAULT 'no',
  `visible_parent` varchar(10) NOT NULL DEFAULT 'no',
  `created_by` varchar(60) DEFAULT NULL,
  `created_id` int(11) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sessions
#

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session` varchar(60) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (7, '2016-17', 'no', '2017-04-20 02:42:19', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (11, '2017-18', 'no', '2017-04-20 02:41:37', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (13, '2018-19', 'no', '2016-08-24 15:26:44', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (14, '2019-20', 'no', '2016-08-24 15:26:55', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (15, '2020-21', 'no', '2016-10-01 01:28:08', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (16, '2021-22', 'no', '2016-10-01 01:28:20', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (18, '2022-23', 'no', '2016-10-01 01:29:02', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (19, '2023-24', 'no', '2016-10-01 01:29:10', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (20, '2024-25', 'no', '2016-10-01 01:29:18', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (21, '2025-26', 'no', '2016-10-01 01:30:10', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (22, '2026-27', 'no', '2016-10-01 01:30:18', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (23, '2027-28', 'no', '2016-10-01 01:30:24', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (24, '2028-29', 'no', '2016-10-01 01:30:30', '0000-00-00 00:00:00');
INSERT INTO `sessions` (`id`, `session`, `is_active`, `created_at`, `updated_at`) VALUES (25, '2029-30', 'no', '2016-10-01 01:30:37', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: sms_config
#

DROP TABLE IF EXISTS `sms_config`;

CREATE TABLE `sms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `api_id` varchar(100) NOT NULL,
  `authkey` varchar(100) NOT NULL,
  `senderid` varchar(100) NOT NULL,
  `contact` text DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'disabled',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: source
#

DROP TABLE IF EXISTS `source`;

CREATE TABLE `source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: staff
#

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(200) NOT NULL,
  `department` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `qualification` varchar(200) NOT NULL,
  `work_exp` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `father_name` varchar(200) NOT NULL,
  `mother_name` varchar(200) NOT NULL,
  `contact_no` varchar(200) NOT NULL,
  `emergency_contact_no` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `dob` date NOT NULL,
  `marital_status` varchar(100) NOT NULL,
  `date_of_joining` date NOT NULL,
  `date_of_leaving` date NOT NULL,
  `local_address` varchar(300) NOT NULL,
  `permanent_address` varchar(200) NOT NULL,
  `note` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `password` varchar(250) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `account_title` varchar(200) NOT NULL,
  `bank_account_no` varchar(200) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `ifsc_code` varchar(200) NOT NULL,
  `bank_branch` varchar(100) NOT NULL,
  `payscale` varchar(200) NOT NULL,
  `basic_salary` varchar(200) NOT NULL,
  `epf_no` varchar(200) NOT NULL,
  `contract_type` varchar(100) NOT NULL,
  `shift` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `twitter` varchar(200) NOT NULL,
  `linkedin` varchar(200) NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `resume` varchar(200) NOT NULL,
  `joining_letter` varchar(200) NOT NULL,
  `resignation_letter` varchar(200) NOT NULL,
  `other_document_name` varchar(200) NOT NULL,
  `other_document_file` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `verification_code` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `staff` (`id`, `employee_id`, `department`, `designation`, `qualification`, `work_exp`, `name`, `surname`, `father_name`, `mother_name`, `contact_no`, `emergency_contact_no`, `email`, `dob`, `marital_status`, `date_of_joining`, `date_of_leaving`, `local_address`, `permanent_address`, `note`, `image`, `password`, `gender`, `account_title`, `bank_account_no`, `bank_name`, `ifsc_code`, `bank_branch`, `payscale`, `basic_salary`, `epf_no`, `contract_type`, `shift`, `location`, `facebook`, `twitter`, `linkedin`, `instagram`, `resume`, `joining_letter`, `resignation_letter`, `other_document_name`, `other_document_file`, `user_id`, `is_active`, `verification_code`) VALUES (1, '', '', '', '', '', 'Super Admin', '', '', '', '', '', 'admin@ss.com', '0000-00-00', '', '0000-00-00', '0000-00-00', '', '', '', '', '$2y$10$A7OmjJkkQz1.S32ojzn5PemoeIN/u4CmTiowYxTwf6cka2wVvY5K6', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '');


#
# TABLE STRUCTURE FOR: staff_attendance
#

DROP TABLE IF EXISTS `staff_attendance`;

CREATE TABLE `staff_attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `staff_id` int(11) NOT NULL,
  `staff_attendance_type_id` int(11) NOT NULL,
  `remark` varchar(200) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: staff_attendance_type
#

DROP TABLE IF EXISTS `staff_attendance_type`;

CREATE TABLE `staff_attendance_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `key_value` varchar(200) NOT NULL,
  `is_active` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `staff_attendance_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (1, 'Present', '<b class=\"text text-success\">P</b>', 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `staff_attendance_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (2, 'Late', '<b class=\"text text-warning\">L</b>', 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `staff_attendance_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (3, 'Absent', '<b class=\"text text-danger\">A</b>', 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `staff_attendance_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (4, 'Half Day', '<b class=\"text text-warning\">F</b>', 'yes', '2018-05-06 21:56:16', '0000-00-00 00:00:00');
INSERT INTO `staff_attendance_type` (`id`, `type`, `key_value`, `is_active`, `created_at`, `updated_at`) VALUES (5, 'Holiday', 'H', 'yes', '0000-00-00 00:00:00', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: staff_designation
#

DROP TABLE IF EXISTS `staff_designation`;

CREATE TABLE `staff_designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(200) NOT NULL,
  `is_active` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: staff_leave_details
#

DROP TABLE IF EXISTS `staff_leave_details`;

CREATE TABLE `staff_leave_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `alloted_leave` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: staff_leave_request
#

DROP TABLE IF EXISTS `staff_leave_request`;

CREATE TABLE `staff_leave_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `leave_from` date NOT NULL,
  `leave_to` date NOT NULL,
  `leave_days` int(11) NOT NULL,
  `employee_remark` varchar(200) NOT NULL,
  `admin_remark` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL,
  `applied_by` varchar(200) NOT NULL,
  `document_file` varchar(200) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: staff_payroll
#

DROP TABLE IF EXISTS `staff_payroll`;

CREATE TABLE `staff_payroll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `basic_salary` int(11) NOT NULL,
  `pay_scale` varchar(200) NOT NULL,
  `grade` varchar(50) NOT NULL,
  `is_active` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: staff_payslip
#

DROP TABLE IF EXISTS `staff_payslip`;

CREATE TABLE `staff_payslip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `basic` int(11) NOT NULL,
  `total_allowance` int(11) NOT NULL,
  `total_deduction` int(11) NOT NULL,
  `leave_deduction` int(11) NOT NULL,
  `tax` varchar(200) NOT NULL,
  `net_salary` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `month` varchar(200) NOT NULL,
  `year` varchar(200) NOT NULL,
  `payment_mode` varchar(200) NOT NULL,
  `payment_date` date NOT NULL,
  `remark` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: staff_roles
#

DROP TABLE IF EXISTS `staff_roles`;

CREATE TABLE `staff_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `staff_roles` (`id`, `role_id`, `staff_id`, `is_active`, `created_at`, `updated_at`) VALUES (1, 7, 1, 0, '2019-12-15 02:40:30', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: staff_timeline
#

DROP TABLE IF EXISTS `staff_timeline`;

CREATE TABLE `staff_timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `timeline_date` date NOT NULL,
  `description` varchar(300) NOT NULL,
  `document` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_attendences
#

DROP TABLE IF EXISTS `student_attendences`;

CREATE TABLE `student_attendences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_session_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `attendence_type_id` int(11) DEFAULT NULL,
  `remark` varchar(200) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `student_session_id` (`student_session_id`),
  KEY `attendence_type_id` (`attendence_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_doc
#

DROP TABLE IF EXISTS `student_doc`;

CREATE TABLE `student_doc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `doc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_fees
#

DROP TABLE IF EXISTS `student_fees`;

CREATE TABLE `student_fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_session_id` int(11) DEFAULT NULL,
  `feemaster_id` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `amount_discount` float(10,2) NOT NULL,
  `amount_fine` float(10,2) NOT NULL DEFAULT 0.00,
  `description` text DEFAULT NULL,
  `date` date DEFAULT '0000-00-00',
  `payment_mode` varchar(50) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_fees_deposite
#

DROP TABLE IF EXISTS `student_fees_deposite`;

CREATE TABLE `student_fees_deposite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_fees_master_id` int(11) DEFAULT NULL,
  `fee_groups_feetype_id` int(11) DEFAULT NULL,
  `amount_detail` text DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `student_fees_master_id` (`student_fees_master_id`),
  KEY `fee_groups_feetype_id` (`fee_groups_feetype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_fees_discounts
#

DROP TABLE IF EXISTS `student_fees_discounts`;

CREATE TABLE `student_fees_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_session_id` int(11) DEFAULT NULL,
  `fees_discount_id` int(11) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'assigned',
  `payment_id` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `student_session_id` (`student_session_id`),
  KEY `fees_discount_id` (`fees_discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_fees_master
#

DROP TABLE IF EXISTS `student_fees_master`;

CREATE TABLE `student_fees_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_system` int(1) NOT NULL DEFAULT 0,
  `student_session_id` int(11) DEFAULT NULL,
  `fee_session_group_id` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT 0.00,
  `is_active` varchar(10) NOT NULL DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `student_session_id` (`student_session_id`),
  KEY `fee_session_group_id` (`fee_session_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_session
#

DROP TABLE IF EXISTS `student_session`;

CREATE TABLE `student_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `route_id` int(11) NOT NULL,
  `hostel_room_id` int(11) NOT NULL,
  `vehroute_id` int(10) DEFAULT NULL,
  `transport_fees` float(10,2) NOT NULL DEFAULT 0.00,
  `fees_discount` float(10,2) NOT NULL DEFAULT 0.00,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_sibling
#

DROP TABLE IF EXISTS `student_sibling`;

CREATE TABLE `student_sibling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `sibling_student_id` int(11) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_timeline
#

DROP TABLE IF EXISTS `student_timeline`;

CREATE TABLE `student_timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `timeline_date` date NOT NULL,
  `description` varchar(200) NOT NULL,
  `document` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: student_transport_fees
#

DROP TABLE IF EXISTS `student_transport_fees`;

CREATE TABLE `student_transport_fees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_session_id` int(11) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `amount_discount` float(10,2) NOT NULL,
  `amount_fine` float(10,2) NOT NULL DEFAULT 0.00,
  `description` text DEFAULT NULL,
  `date` date DEFAULT '0000-00-00',
  `is_active` varchar(255) DEFAULT 'no',
  `payment_mode` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: students
#

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `admission_no` varchar(100) DEFAULT NULL,
  `roll_no` varchar(100) DEFAULT NULL,
  `admission_date` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `rte` varchar(20) NOT NULL DEFAULT 'No',
  `image` varchar(100) DEFAULT NULL,
  `mobileno` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `pincode` varchar(100) DEFAULT NULL,
  `religion` varchar(100) DEFAULT NULL,
  `cast` varchar(50) NOT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `current_address` text DEFAULT NULL,
  `permanent_address` text DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `route_id` int(11) NOT NULL,
  `school_house_id` int(11) NOT NULL,
  `blood_group` varchar(200) NOT NULL,
  `vehroute_id` int(11) NOT NULL,
  `hostel_room_id` int(11) NOT NULL,
  `adhar_no` varchar(100) DEFAULT NULL,
  `samagra_id` varchar(100) DEFAULT NULL,
  `bank_account_no` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(100) DEFAULT NULL,
  `guardian_is` varchar(100) NOT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `father_phone` varchar(100) DEFAULT NULL,
  `father_occupation` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `mother_phone` varchar(100) DEFAULT NULL,
  `mother_occupation` varchar(100) DEFAULT NULL,
  `guardian_name` varchar(100) DEFAULT NULL,
  `guardian_relation` varchar(100) DEFAULT NULL,
  `guardian_phone` varchar(100) DEFAULT NULL,
  `guardian_occupation` varchar(150) NOT NULL,
  `guardian_address` text DEFAULT NULL,
  `guardian_email` varchar(100) NOT NULL,
  `father_pic` varchar(200) NOT NULL,
  `mother_pic` varchar(200) NOT NULL,
  `guardian_pic` varchar(200) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `previous_school` text DEFAULT NULL,
  `height` varchar(100) NOT NULL,
  `weight` varchar(100) NOT NULL,
  `measurement_date` date NOT NULL,
  `app_key` text DEFAULT NULL,
  `parent_app_key` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable_at` date NOT NULL,
  `note` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: subjects
#

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: teacher_subjects
#

DROP TABLE IF EXISTS `teacher_subjects`;

CREATE TABLE `teacher_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) DEFAULT NULL,
  `class_section_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `class_section_id` (`class_section_id`),
  KEY `session_id` (`session_id`),
  KEY `subject_id` (`subject_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: timetables
#

DROP TABLE IF EXISTS `timetables`;

CREATE TABLE `timetables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_subject_id` int(20) DEFAULT NULL,
  `day_name` varchar(50) DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL,
  `room_no` varchar(50) DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: transport_route
#

DROP TABLE IF EXISTS `transport_route`;

CREATE TABLE `transport_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_title` varchar(100) DEFAULT NULL,
  `no_of_vehicle` int(11) DEFAULT NULL,
  `fare` float(10,2) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `is_active` varchar(255) DEFAULT 'no',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: userlog
#

DROP TABLE IF EXISTS `userlog`;

CREATE TABLE `userlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `ipaddress` varchar(100) DEFAULT NULL,
  `user_agent` varchar(500) DEFAULT NULL,
  `login_datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `userlog` (`id`, `user`, `role`, `ipaddress`, `user_agent`, `login_datetime`) VALUES (1, 'admin@ss.com', 'Super Admin', '43.239.206.101', 'Chrome 78.0.3904.108, Mac OS X', '2019-12-15 06:17:34');
INSERT INTO `userlog` (`id`, `user`, `role`, `ipaddress`, `user_agent`, `login_datetime`) VALUES (2, 'admin@ss.com', 'Super Admin', '43.239.206.101', 'Chrome 78.0.3904.108, Mac OS X', '2019-12-15 07:38:36');
INSERT INTO `userlog` (`id`, `user`, `role`, `ipaddress`, `user_agent`, `login_datetime`) VALUES (3, 'admin@ss.com', 'Super Admin', '150.242.65.215', 'Chrome 79.0.3945.79, Mac OS X', '2019-12-15 07:38:49');
INSERT INTO `userlog` (`id`, `user`, `role`, `ipaddress`, `user_agent`, `login_datetime`) VALUES (4, 'admin@ss.com', 'Super Admin', '43.239.206.101', 'Chrome 78.0.3904.108, Mac OS X', '2019-12-15 11:16:34');
INSERT INTO `userlog` (`id`, `user`, `role`, `ipaddress`, `user_agent`, `login_datetime`) VALUES (5, 'admin@ss.com', 'Super Admin', '150.242.65.215', 'Chrome 79.0.3945.79, Mac OS X', '2019-12-15 13:29:59');


#
# TABLE STRUCTURE FOR: users
#

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `childs` text NOT NULL,
  `role` varchar(30) NOT NULL,
  `verification_code` varchar(200) NOT NULL,
  `is_active` varchar(255) DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: users_authentication
#

DROP TABLE IF EXISTS `users_authentication`;

CREATE TABLE `users_authentication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expired_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: vehicle_routes
#

DROP TABLE IF EXISTS `vehicle_routes`;

CREATE TABLE `vehicle_routes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `route_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: vehicles
#

DROP TABLE IF EXISTS `vehicles`;

CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_no` varchar(20) DEFAULT NULL,
  `vehicle_model` varchar(100) NOT NULL DEFAULT 'None',
  `manufacture_year` varchar(4) DEFAULT NULL,
  `driver_name` varchar(50) DEFAULT NULL,
  `driver_licence` varchar(50) NOT NULL DEFAULT 'None',
  `driver_contact` varchar(20) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: visitors_book
#

DROP TABLE IF EXISTS `visitors_book`;

CREATE TABLE `visitors_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(100) DEFAULT NULL,
  `purpose` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contact` varchar(12) NOT NULL,
  `id_proof` varchar(50) NOT NULL,
  `no_of_pepple` int(11) NOT NULL,
  `date` date NOT NULL,
  `in_time` varchar(20) NOT NULL,
  `out_time` varchar(20) NOT NULL,
  `note` mediumtext NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: visitors_purpose
#

DROP TABLE IF EXISTS `visitors_purpose`;

CREATE TABLE `visitors_purpose` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visitors_purpose` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

