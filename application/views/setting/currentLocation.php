<style type="text/css">


    .modal-dialog2 {margin: 1% auto;}
.color_box {
  float: left;
 width: 10px;
    height: 10px;
  margin: 5px;
  border: 1px solid rgba(0, 0, 0, .2);
}
</style>


<!-- CSS Files -->

<link href="<?php echo base_url(); ?>backend/Content/cssFile.css" rel="stylesheet" />

<div class="content-wrapper" style="min-height: 946px;">
    <!-- <section class="content-header">
        <h1><i class="fa fa-map-marker"></i> Current Location</h1>
    </section> -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            
            <!--./col-md-3-->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-map-marker"></i> Current Location</h3>
                        
                        <!-- <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <a href="#schsetting" role="button" class="btn btn-primary btn-sm checkbox-toggle pull-right edit_setting" data-toggle="tooltip" title="<?php echo $this->lang->line('edit_setting'); ?>" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><i class="fa fa-pencil"></i> <?php echo $this->lang->line('edit'); ?></a>
                            </div>
                        </div> -->
                    </div>
                    <div class="box-body">
                        <div class="content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12 ml-auto mr-auto">
                                        <div class="card">
                                            <div class="card-body">
                                                <!-- <h4 class="card-title"> Current Location </h4> -->
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <section class="">
                                                            <div class="wrapper-demo">
                                                                <div id="dd" class="wrapper-dropdown-3 cus-dropdown" style="z-index:10000;" tabindex="1">
                                                                    <span>Select Device</span>
                                                                    <ul class="dropdown" id="ddlValues"></ul>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                                <br />
                                                <br />
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div id="mapDiv" style="width:100%;height:400px; ">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--./col-md-9-->
        </div>
    </section>
</div>


        

        
        <script>
            var DeviceIDSelected = 0;
            var markersArray = [];
            var HostURL = "<?php echo $settinglist[0]['tracking_url'];?>";
            $(document).ready(function () {
                GetDataURL();
                GetDevices();
            });
            
            //setInterval(GetDevices, 10000);
            setInterval(function () {
                if (DeviceIDSelected != 0) {
                    var DeviceID = DeviceIDSelected;
                    ShowCurLocation(DeviceID);
                }
            }, 10000);
            
            function GetDataURL()
            {
                var ReturnURL = "";
                
                // $.ajax({
                //     type: "GET",
                //     url: "mapConfig.xml",
                //     dataType: "xml",
                //     async:false,
                //     error: function (e) {
                //         ReturnURL = "";
                //         console.log("XML reading Failed: ", e);
                //     },
            
                //     success: function (response) {
            
                //         $(response).find("Configuration").each(function () {
                //             var HostIP = $(this).find('HostIP').text();
                //             var Port = $(this).find('Port').text();
                //             var Https = $(this).find('https').text();
            
                //             HostURL = (Https == 'Y' ? 'HTTPS://' : 'HTTP://') + HostIP + (Port != "" ? (':' + Port) : "");
            
                //             alert(HostURL);
                //         });
                //     }
                // });

                // $.ajax({
                //     type: "GET",
                //     url: "getXml",
                //     dataType: "xml",
                //     async:false,
                //     error: function (e) {
                //         ReturnURL = "";
                //         console.log("XML reading Failed: ", e);
                //     },
            
                //     success: function (response) {
            
                //         $(response).find("Configuration").each(function () {
                //             var HostIP = $(this).find('HostIP').text();
                //             var Port = $(this).find('Port').text();
                //             var Https = $(this).find('https').text();
            
                //             HostURL = (Https == 'Y' ? 'HTTPS://' : 'HTTP://') + HostIP + (Port != "" ? (':' + Port) : "");
            
                //             // alert(HostURL);
                //         });
                //     }
                // });
            
                return ReturnURL;
            }
            
            function GetDevices() {
               
                if (HostURL != "") {
                    var CallingURL = HostURL + '/api/devices';
                    $.ajax({
                        //url: '<%= Page.ResolveUrl("~/index.aspx/GetDeviceLocation")%>',
                        //url: 'http://localhost:8082/api/devices',
                        url: CallingURL,
                        type: 'get',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        headers: {
                            'Authorization': 'Basic ' + btoa('admin:admin')
                            //'Authorization' : 'Bearer ' + 'Token value'
                        },
                        async: false,
                        success: function (response) {
                            // $('#cboDevices').append($('<option>').val('0').text('---- Select Device  ----'))
            
                            var ddlItems = "<li><a href='#'>-------- Select --------</a></li>";
            
                            $.each(response, function (key, item) {
                                var iconClass = '';
                                if (item.status == "online")
                                    iconClass = 'onlineicon onlineicon-accessibility';
                                else if (item.status == "offline")
                                    iconClass = 'offlineicon offlineicon-accessibility';
                                else
                                    iconClass = 'unknownicon unknownicon-accessibility';
            
                                ddlItems += "<li><a href='#' onclick='ShowCurLocation(" + item.id + ")'><i class='" + iconClass + "'></i>" + (item.uniqueId + " (" + item.name + ")") + "</a></li>";
                                //var ItemToInsert = item.uniqueId + "  |  " + item.name + "  |   " + item.status;
                                //$('#cboDevices').append($("<option  style='background-color:" + (item.status == "online" ? "green" : "red") + ";'>").val(item.id).text(ItemToInsert))
                            });
            
                            $('#ddlValues').html(ddlItems);
                        },
                        complete: function (jqXHR) {
                            if (jqXHR.status == '401') {
                                alert('Not Authorized');
                            }
                        },
                        failure: function (response) {
                            ReturnStr = 'x';
                        },
                        error: function (response) {
                            ReturnStr = 'x';
                        }
                    });
                }
            }
            var mapObj;
            var map;
            function SetMap() {
                var mapObj = {
                    //center: new google.maps.LatLng(0, 0),
                    //zoom: 4,
                    center: new google.maps.LatLng(28.644800, 77.216721), // map center
                    zoom: 16, //zoom level, 0 = earth view to higher value
                    panControl: true, //enable pan Control
                    zoomControl: true, //enable zoom control
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL //zoom control size
                    },
                    scaleControl: true, // enable scale control
                    mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
                };
                map = new google.maps.Map(document.getElementById('mapDiv'), mapObj);
            }
            
            function ShowCurLocation(DeviceID) {
                var deviceid = DeviceID;
                DeviceIDSelected = DeviceID;
                clearAllMarkers();
                if (HostURL != "") {
                    var CallingURL = HostURL + '/api/positions';
                    $.ajax({
                        //url: '<%= Page.ResolveUrl("~/index.aspx/GetDeviceLocation")%>',
                        //url: 'http://localhost:8082/api/positions?deviceId=' + deviceid + '&from=2020-01-01T00:30:00Z&to=2020-01-15T00:30:00Z',
                        //url: 'http://localhost:8082/api/positions',
                        url: CallingURL,
                        type: 'get',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        headers: {
                            'Authorization': 'Basic ' + btoa('admin:admin')
                            //'Authorization' : 'Bearer ' + 'Token value'
                        },
                        async: false,
                        success: function (response) {
            
                            for (var i = 0; i < response.length; i++) {
                                if (response[i].deviceId == DeviceID) {
                                    ShowMapLocation(response[i]);
                                }
                            }
            
            
                        },
                        complete: function (jqXHR) {
                            if (jqXHR.status == '401') {
                                alert('Not Authorized');
                            }
                        },
                        failure: function (response) {
                            ReturnStr = 'x';
                        },
                        error: function (response) {
                            ReturnStr = 'x';
                        }
                    });
                }
            }
            
            function ShowMapLocation(response)
            {
                var Latitiude = response.latitude;
                var Longitude = response.longitude;
                var DevicePosition = { lat: Latitiude, lng: Longitude };
            
                var contentString = "<ul>";
                contentString += "<li> Server Time: " + response.serverTime + "</li>";
                contentString += "<li> Device Time: " + response.deviceTime + "</li>";
                //contentString += "<li> Latitiude: " + response[0].Latitiude + "</li>";
                //contentString += "<li> Longitude: " + response[0].Longitude + "</li>";
                contentString += "<li> Address: " + response.address + "</li>";
                contentString += "<li> Accuracy: " + response.accuracy + "</li>";
                contentString += "</ul>";
            
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
            
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(Latitiude, Longitude),
                    center: new google.maps.LatLng(Latitiude, Longitude),
                    zoom: 9,
                    map: map,
                    draggable: true,
                    //animation: google.maps.Animation.DROP,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControl: true, //enable zoom control
                    icon: base_url+"uploads/bus.png",
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    }//zoom control size
                });
                markersArray.push(marker);
                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });
            
                marker.setMap(map);
                map.setCenter(marker.getPosition());
                map.map.fitBounds(MYMAP.bounds);
            }
            
            function clearAllMarkers() {
                if (markersArray.length > 0) {
                    for (var i = 0; i < markersArray.length; i++) {
                        markersArray[i].setMap(null);
                    }
                    markersArray.length = 0;
                }
            }
            
            
            function DropDown(el) {
                this.dd = el;
                this.placeholder = this.dd.children('span');
                this.opts = this.dd.find('ul.dropdown > li');
                this.val = '';
                this.index = -1;
                this.initEvents();
            }
            DropDown.prototype = {
                initEvents: function () {
                    var obj = this;
            
                    obj.dd.on('click', function (event) {
                        $(this).toggleClass('active');
                        return false;
                    });
            
                    obj.opts.on('click', function () {
                        var opt = $(this);
                        obj.val = opt.text();
                        obj.index = opt.index();
                        obj.placeholder.text(obj.val);
                    });
                },
                getValue: function () {
                    return this.val;
                },
                getIndex: function () {
                    return this.index;
                }
            }
            
            $(function () {
            
                var dd = new DropDown($('#dd'));
            
                $(document).click(function () {
                    // all dropdowns
                    $('.wrapper-dropdown-3').removeClass('active');
                });
            
            });
            
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDn4xqzOsphLQamjper8jHCdMvODZEsVy8&callback=SetMap"></script>