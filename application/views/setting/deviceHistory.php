<style type="text/css">


    .modal-dialog2 {margin: 1% auto;}
.color_box {
  float: left;
 width: 10px;
    height: 10px;
  margin: 5px;
  border: 1px solid rgba(0, 0, 0, .2);
}
</style>


<!-- CSS Files -->

<link href="<?php echo base_url(); ?>backend/Content/cssFile.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>backend/Content/bootstrap-datepicker.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>backend/scripts/modernizr.custom.79639.js"></script>

<div class="content-wrapper" style="min-height: 946px;">
    <!-- <section class="content-header">
        <h1><i class="fa fa-map-marker"></i> Current Location</h1>
    </section> -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            
            <!--./col-md-3-->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-map-marker"></i> Device History</h3>
                        
                        <!-- <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <a href="#schsetting" role="button" class="btn btn-primary btn-sm checkbox-toggle pull-right edit_setting" data-toggle="tooltip" title="<?php echo $this->lang->line('edit_setting'); ?>" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><i class="fa fa-pencil"></i> <?php echo $this->lang->line('edit'); ?></a>
                            </div>
                        </div> -->
                    </div>
                    <div class="box-body">
                        
                        <ul class="flexUL" type="none">
                            <li>
                                <div class="">
                                    <div id="dd" class="wrapper-dropdown-3" style="z-index:10000;" tabindex="1">
                                        <span>Select Device</span>
                                        <ul class="dropdown" id="ddlValues"></ul>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="">
                                    From
                                </div>
                                <div class="">
                                    <input type="text" class="form-control txtfield" id="txtFrom" placeholder="Select Date (yyyy/mm/dd)" />
                                </div>
                            </li>
                            <li>
                                <div>
                                    To
                                </div>
                                <div>
                                    <input type="text" class="form-control txtfield" id="txtTo" placeholder="Select Date (yyyy/mm/dd)" />
                                </div>    
                            </li>
                            <li>
                                <div class=""><input type="button" class="btn btn-primary cus-button" value="Show" id="btnShowHist" onclick="ShowHistory()"/></div>
                            </li>
                        </ul>
                        
                        
                       <div class="row" style="margin-top:10px;">
                           <div id="mapDiv" style="width:100%;height:400px; ">

                           </div>
                       </div>
                    </div>
                </div>
            </div>
            <!--./col-md-9-->
        </div>
    </section>
</div>

<script src="<?php echo base_url(); ?>backend/scripts/bootstrap-datepicker.min.js"></script>
        

        
<script>
    var DeviceIDSelected = 0;
    var HostURL = "<?php echo $settinglist[0]['tracking_url'];?>";
    var markersArray = [];
    $(document).ready(function () {
        $('#txtFrom').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        $('#txtTo').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });
        GetDataURL();
        GetDevices();
    });

    function GetDataURL() {
        var ReturnURL = "";
        
        $.ajax({
            type: "GET",
            url: "mapConfig.xml",
            dataType: "xml",
            async: false,
            error: function (e) {
                ReturnURL = "";
                console.log("XML reading Failed: ", e);
            },

            success: function (response) {
                $(response).find("Configuration").each(function () {
                    var HostIP = $(this).find('HostIP').text();
                    var Port = $(this).find('Port').text();
                    var Https = $(this).find('https').text();

                    HostURL = (Https == 'Y' ? 'HTTPS://' : 'HTTP://') + HostIP + (Port != "" ? (':' + Port) : "");


                });
            }
        });

        return ReturnURL;
    }

    function GetDevices() {
        if (HostURL != "") {
            var CallingURL = HostURL + '/api/devices';
            $.ajax({
                //url: '<%= Page.ResolveUrl("~/index.aspx/GetDeviceLocation")%>',
                //url: 'http://3.85.91.27/api/devices',
                url: CallingURL,
                type: 'get',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                headers: {
                    'Authorization': 'Basic ' + btoa('admin:admin')
                    //'Authorization' : 'Bearer ' + 'Token value'
                },
                async: false,
                success: function (response) {
                    var ddlItems = "<li><a href='#'>-------- Select --------</a></li>";

                    $.each(response, function (key, item) {
                        var iconClass = '';
                        if (item.status == "online")
                            iconClass = 'onlineicon onlineicon-accessibility';
                        else if (item.status == "offline")
                            iconClass = 'offlineicon offlineicon-accessibility';
                        else
                            iconClass = 'unknownicon unknownicon-accessibility';

                        ddlItems += "<li><a href='#' onclick='SelectDevice(" + item.id + ")'><i class='" + iconClass + "'></i>" + (item.uniqueId + " (" + item.name + ")") + "</a></li>";
                        //var ItemToInsert = item.uniqueId + "  |  " + item.name + "  |   " + item.status;
                        //$('#cboDevices').append($("<option  style='background-color:" + (item.status == "online" ? "green" : "red") + ";'>").val(item.id).text(ItemToInsert))
                    });

                    $('#ddlValues').html(ddlItems);
                },
                complete: function (jqXHR) {
                    if (jqXHR.status == '401') {
                        alert('Not Authorized');
                    }
                },
                failure: function (response) {
                    ReturnStr = 'x';
                },
                error: function (response) {
                    ReturnStr = 'x';
                }
            });
        }
    }

    var mapObj;
    var map;
    function SetMap() {
        var mapObj = {
            //center: new google.maps.LatLng(0, 0),
            //zoom: 4,
            center: new google.maps.LatLng(28.644800, 77.216721), // map center
            zoom: 12, //zoom level, 0 = earth view to higher value               
            mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
        };
        map = new google.maps.Map(document.getElementById('mapDiv'), mapObj);
    }
    
    function SelectDevice(SelDeviceId)
    {
        DeviceIDSelected = SelDeviceId;
    }
    

    function clearAllMarkers() {
        if (markersArray.length > 0) {
            for (var i = 0; i < markersArray.length; i++) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
        }
    }


    function ShowHistory() {
        var deviceid = DeviceIDSelected;//$('#cboDevices').val();
        var FromDate = $('#txtFrom').val() + 'T00:00:00Z';//2019-01-03T00:30:00Z
        var ToDate = $('#txtTo').val() + 'T00:00:00Z';

        if (HostURL != "") {
            var CallingURL = HostURL + '/api/reports/route?deviceId=' + deviceid + '&from=' + FromDate + '&to=' + ToDate;
            $.ajax({
                url: CallingURL,//'http://3.85.91.27/api/reports/route?deviceId=' + deviceid + '&from=' + FromDate + '&to=' + ToDate,//&type=allEvents
                type: 'get',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                headers: {
                    'Authorization': 'Basic ' + btoa('admin:admin')
                    //'Authorization' : 'Bearer ' + 'Token value'
                },
                async: false,
                success: function (response) {
                    SetMapRoutes(response);
                },
                complete: function (jqXHR) {
                    if (jqXHR.status == '401') {
                        alert('Not Authorized');
                    }
                },
                failure: function (response) {
                    ReturnStr = 'x';
                },
                error: function (response) {
                    ReturnStr = 'x';
                }
            });
        }
    }

    function SetMapRoutes(response)
    {
        var infoWindow = new google.maps.InfoWindow();
        var lat_lng = new Array();
        var latlngbounds = new google.maps.LatLngBounds();

        for (i = 0; i < response.length; i++) {
            var iconPath = '';
            if (i == 0)
                iconPath += base_url+"uploads/start.png";
            else if (i == (response.length - 1))
                iconPath += base_url+"uploads/end.png";
            else
                iconPath += base_url+"uploads/pin.png";
            var data = response[i]
            
            var PosLatlng = new google.maps.LatLng(data.latitude, data.longitude);
            lat_lng.push(PosLatlng);
            var marker = new google.maps.Marker({
                position: PosLatlng,
                map: map,
                icon: iconPath,
                title: 'Lat,long: ' + data.latitude + ',' + data.longitude
            });
            markersArray.push(marker);
            latlngbounds.extend(marker.position);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.address);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);

        //***********Routing starts****************//
        //Initialize the Path Array
        var path = new google.maps.MVCArray();

        //Initialize the Direction Service
        var service = new google.maps.DirectionsService();

       //Set the Path Stroke Color
        var poly = new google.maps.Polyline({ map: map, strokeWeight: 4, strokeColor: '#4986E7' });

        //Loop and Draw Path Route between the Points on MAP
        for (var i = 0; i < lat_lng.length; i++) {
            if ((i + 1) < lat_lng.length) {
                var src = lat_lng[i];
                var des = lat_lng[i + 1];
                path.push(src);
                poly.setPath(path);
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function (result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                            path.push(result.routes[0].overview_path[i]);
                        }
                    }
                });
            }

            
        }
                 
    }

   
    //function generatePath(i, lat_lng) {
    //    // colors
    //    var colorVariable = ["green", "blue", "red"];
    //    //Intialize the Direction Service
    //    var service = new google.maps.DirectionsService();
    //    //Intialize the Path Array
    //    var path = new google.maps.MVCArray();
    //    var color;  // the % makes sure the array cycles, so after rose, it's white again
    //    if (i == 0)
    //        color = colorVariable[0];
    //    else if (i == (lat_lng - 1))
    //        color = colorVariable[2];
    //    else
    //        color = colorVariable[1];

    //    var poly = new google.maps.Polyline({ map: map, strokeColor: color });
    //    polylines.push(poly);
    //    var src = lat_lng[i];
    //    var des = lat_lng[i + 1];
    //    path.push(src);
    //    poly.setPath(path);
    //    // route service
    //    service.route({
    //        origin: src,
    //        destination: des,
    //        travelMode: google.maps.DirectionsTravelMode.DRIVING
    //    }, function (result, status) {
    //        if (status == google.maps.DirectionsStatus.OK) {
    //            for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
    //                path.push(result.routes[0].overview_path[i]);
    //            }
    //        }
    //    });
    
    //}
    function DropDown(el) {
        this.dd = el;
        this.placeholder = this.dd.children('span');
        this.opts = this.dd.find('ul.dropdown > li');
        this.val = '';
        this.index = -1;
        this.initEvents();
    }
    DropDown.prototype = {
        initEvents: function () {
            var obj = this;

            obj.dd.on('click', function (event) {
                $(this).toggleClass('active');
                return false;
            });

            obj.opts.on('click', function () {
                var opt = $(this);
                obj.val = opt.text();
                obj.index = opt.index();
                obj.placeholder.text(obj.val);
            });
        },
        getValue: function () {
            return this.val;
        },
        getIndex: function () {
            return this.index;
        }
    }

    $(function () {

        var dd = new DropDown($('#dd'));

        $(document).click(function () {
            // all dropdowns
            $('.wrapper-dropdown-3').removeClass('active');
        });

    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDn4xqzOsphLQamjper8jHCdMvODZEsVy8&callback=SetMap"></script>