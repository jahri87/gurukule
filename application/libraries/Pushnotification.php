<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pushnotification
{

    public $CI;

    public $API_ACCESS_KEY = "AAAA4364QoA:APA91bErEZqVfkSeV0dgzp10NcWpqRJUu4I4LrvBzp8Nm_qOQ3tWG4wvc0tdsRaJXj3-j8wFzdSal-cVGphWYVXbQjIvByH1HdVAOmoO9Gw5LR12h7v426mBu3EkWmtEL5kQRgxE5q_8";
    public $fcmUrl         = "https://fcm.googleapis.com/fcm/send";

    public function __construct()
    {
        $this->CI = &get_instance();

    }

    public function send($tokens, $msg, $action = "")
    {
    
        // print_r($tokens);
        // die;
        // $notificationData = [
        //     'title'  => $msg['title'],
        //     'body'   => $msg['body'],
        //     'action' => $action,
        //     'sound'  => 'mySound',
        // ];

        // $fcmNotification = [

        //     'to'          => $tokens, //single token
        //     'collapseKey' => "{$tokens}",
        //     'data'        => $notificationData,

        // ];
        // $headers = [
        //     'Authorization: key=' . $this->API_ACCESS_KEY,
        //     'Content-Type: application/json',
        // ];

        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $this->fcmUrl);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        // $result = curl_exec($ch);
        // curl_close($ch);

        // return true;
        
        $payload = array(
              'to' => $tokens,
	      'collapseKey' => "{$tokens}",
              'sound' => 'default',
	      'action' => $action,
              'title'  => $msg['title'],
              'body'   => $msg['body'],
             );
    
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://exp.host/--/api/v2/push/send",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($payload),
              CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Accept-Encoding: gzip, deflate",
                "Content-Type: application/json",
                "cache-control: no-cache",
                "host: exp.host"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {
              //echo "cURL Error #:" . $err;
            } else {
              //echo $response;
            }
            
            return true;
            

    }

}
